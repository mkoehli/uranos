# Publications using URANOS (30)

Citations: **724** (based on [CrossRef.org](https://www.crossref.org/))

*Figure. Left: Publications and their total citations (bar width). Right: Cumulative publications over the years. (Spiral package credits: [G. Skok, 2022](https://doi.org/10.3390/app12136609))*
![Publications and citations per year](pubplot.png)

## Details 
- `2025` Francke, Brogi, Duarte Rocha, Förster, Heistermann, Köhli, Rasche, Reich, .., Schrön  
**"Virtual Joint Field Campaign: a framework of synthetic landscapes to assess multiscale measurement methods of water storage"**  
— *Geosci. Model Dev.*, doi:[10.5194/gmd-18-819-2025](https://doi.org/10.5194/gmd-18-819-2025), Citations: **0**  
- `2025` Wang, Liu, Köhli, Marach, Wang  
**"Monitoring Soil Water Content and Measurement Depth of Cosmic-Ray Neutron Sensing in the Tibetan Plateau"**  
— *Journal of Hydrometeorology*, doi:[10.1175/jhm-d-23-0103.1](https://doi.org/10.1175/jhm-d-23-0103.1), Citations: **0**  
- `2024` Hubert  
**"Analyses of continuous measurements of cosmic ray induced-neutrons spectra at the Concordia Antarctic Station from 2016 to 2024"**  
— *Astroparticle Physics*, doi:[10.1016/j.astropartphys.2024.102949](https://doi.org/10.1016/j.astropartphys.2024.102949), Citations: **3**  
- `2024` Woodley, Kim, Sproles, Eberly, Tuttle  
**"Evaluating Cosmic Ray Neutron Sensor Estimates of Snow Water Equivalent in a Prairie Environment Using UAV Lidar"**  
— *Water Resources Research*, doi:[10.1029/2024wr037164](https://doi.org/10.1029/2024wr037164), Citations: **0**  
- `2024` Schrön, Rasche, Weimar, Köhli, Herbst, Boehrer, Hertle, Kögler, Zacharias  
**"Buoy‐Based Detection of Low‐Energy Cosmic‐Ray Neutrons to Monitor the Influence of Atmospheric, Geomagnetic, and Heliospheric Effects"**  
— *Earth and Space Science*, doi:[10.1029/2023ea003483](https://doi.org/10.1029/2023ea003483), Citations: **0**  
- `2023` Rasche, Weimar, Schrön, Köhli, Morgner, Güntner, Blume  
**"A change in perspective: downhole cosmic-ray neutron sensing for the estimation of soil moisture"**  
— *Hydrol. Earth Syst. Sci.*, doi:[10.5194/hess-27-3059-2023](https://doi.org/10.5194/hess-27-3059-2023), Citations: **1**  
- `2023` Brogi, Pisinaras, Köhli, Dombrowski, Hendricks Franssen, Babakos, Chatzi, Panagopoulos, Bogena  
**"Monitoring Irrigation in Small Orchards with Cosmic-Ray Neutron Sensors"**  
— *Sensors*, doi:[10.3390/s23052378](https://doi.org/10.3390/s23052378), Citations: **4**  
- `2023` Schrön, Köhli, Zacharias  
**"Signal contribution of distant areas to cosmic-ray neutron sensors – implications for footprint and sensitivity"**  
— *Hydrol. Earth Syst. Sci.*, doi:[10.5194/hess-27-723-2023](https://doi.org/10.5194/hess-27-723-2023), Citations: **5**  
- `2023` Köhli, Schrön, Zacharias, Schmidt  
**"URANOS v1.0 – the Ultra Rapid Adaptable Neutron-Only Simulation for Environmental Research"**  
— *Geosci. Model Dev.*, doi:[10.5194/gmd-16-449-2023](https://doi.org/10.5194/gmd-16-449-2023), Citations: **7**  
- `2022` Brogi, Bogena, Köhli, Huisman, Hendricks Franssen, Dombrowski  
**"Feasibility of irrigation monitoring with cosmic-ray neutron sensors"**  
— *Geosci. Instrum. Method. Data Syst.*, doi:[10.5194/gi-11-451-2022](https://doi.org/10.5194/gi-11-451-2022), Citations: **4**  
- `2022` Köhli, Schmoldt  
**"Feasibility of UXO detection via pulsed neutron–neutron logging"**  
— *Applied Radiation and Isotopes*, doi:[10.1016/j.apradiso.2022.110403](https://doi.org/10.1016/j.apradiso.2022.110403), Citations: **5**  
- `2022` Kasner, Zacharias, Schrön  
**"On soil bulk density and its influence to soil moisture estimation with cosmic-ray neutrons"**  
— *[preprint]*, doi:[10.5194/hess-2022-123](https://doi.org/10.5194/hess-2022-123), Citations: **0**  
- `2022` Francke, Heistermann, Köhli, Budach, Schrön, Oswald  
**"Assessing the feasibility of a directional cosmic-ray neutron sensing sensor  for estimating soil moisture"**  
— *Geosci. Instrum. Method. Data Syst.*, doi:[10.5194/gi-11-75-2022](https://doi.org/10.5194/gi-11-75-2022), Citations: **9**  
- `2021` Schrön, Oswald, Zacharias, Kasner, Dietrich, Attinger  
**"Neutrons on Rails: Transregional Monitoring of Soil Moisture and Snow Water Equivalent"**  
— *Geophysical Research Letters*, doi:[10.1029/2021gl093924](https://doi.org/10.1029/2021gl093924), Citations: **19**  
- `2021` Rasche, Köhli, Schrön, Blume, Güntner  
**"Towards disentangling heterogeneous soil moisture patterns in cosmic-ray neutron sensor footprints"**  
— *Hydrol. Earth Syst. Sci.*, doi:[10.5194/hess-25-6547-2021](https://doi.org/10.5194/hess-25-6547-2021), Citations: **15**  
- `2021` Köhli, Schmoldt  
**"Potential von Impulse-Neutron-Neutron-Logging zur Kampfmitteldetektion"**  
— *altlasten spektrum*, doi:[10.37307/j.1864-8371.2021.06.03](https://doi.org/10.37307/j.1864-8371.2021.06.03), Citations: **0**  
- `2021` Jakobi, Huisman, Köhli, Rasche, Vereecken, Bogena  
**"The Footprint Characteristics of Cosmic Ray Thermal Neutrons"**  
— *Geophysical Research Letters*, doi:[10.1029/2021gl094281](https://doi.org/10.1029/2021gl094281), Citations: **17**  
- `2021` Badiee, Wallbank, Fentanes, Trill, Scarlet, Zhu, Cielniak, Cooper, ...., Pearson  
**"Using Additional Moderator to Control the Footprint of a COSMOS Rover for Soil Moisture Measurement"**  
— *Water Resources Research*, doi:[10.1029/2020wr028478](https://doi.org/10.1029/2020wr028478), Citations: **7**  
- `2021` Köhli, Weimar, Schrön, Baatz, Schmidt  
**"Soil Moisture and Air Humidity Dependence of the Above-Ground Cosmic-Ray Neutron Intensity"**  
— *Front. Water*, doi:[10.3389/frwa.2020.544847](https://doi.org/10.3389/frwa.2020.544847), Citations: **37**  
- `2020` Weimar, Köhli, Budach, Schmidt  
**"Large-Scale Boron-Lined Neutron Detection Systems as a 3He Alternative for Cosmic Ray Neutron Sensing"**  
— *Front. Water*, doi:[10.3389/frwa.2020.00016](https://doi.org/10.3389/frwa.2020.00016), Citations: **34**  
- `2019` Schattan, Köhli, Schrön, Baroni, Oswald  
**"Sensing Area‐Average Snow Water Equivalent with Cosmic‐Ray Neutrons: The Influence of Fractional Snow Cover"**  
— *Water Resources Research*, doi:[10.1029/2019wr025647](https://doi.org/10.1029/2019wr025647), Citations: **34**  
- `2019` Li, Schrön, Köhli, Bogena, Weimar, Jiménez Bello, Han, Martínez Gimeno, .., Hendricks Franssen  
**"Can Drip Irrigation be Scheduled with Cosmic‐Ray Neutron Sensing?"**  
— *Vadose Zone Journal*, doi:[10.2136/vzj2019.05.0053](https://doi.org/10.2136/vzj2019.05.0053), Citations: **24**  
- `2018` Köhli, Desch, Gruber, Kaminski, Schmidt, Wagner  
**"Novel neutron detectors based on the time projection method"**  
— *Physica B: Condensed Matter*, doi:[10.1016/j.physb.2018.03.026](https://doi.org/10.1016/j.physb.2018.03.026), Citations: **8**  
- `2018` Schrön, Rosolem, Köhli, Piussi, Schröter, Iwema, Kögler, Oswald, ..., Zacharias  
**"Cosmic‐ray Neutron Rover Surveys of Field Soil Moisture and the Influence of Roads"**  
— *Water Resources Research*, doi:[10.1029/2017wr021719](https://doi.org/10.1029/2017wr021719), Citations: **58**  
- `2018` Köhli, Schrön, Schmidt  
**"Response functions for detectors in cosmic ray neutron sensing"**  
— *Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment*, doi:[10.1016/j.nima.2018.06.052](https://doi.org/10.1016/j.nima.2018.06.052), Citations: **32**  
- `2018` Schrön, Zacharias, Womack, Köhli, Desilets, Oswald, Bumberger, Mollenhauer, ...., Dietrich  
**"Intercomparison of cosmic-ray neutron sensors and water balance monitoring in an urban environment"**  
— *Geosci. Instrum. Method. Data Syst.*, doi:[10.5194/gi-7-83-2018](https://doi.org/10.5194/gi-7-83-2018), Citations: **46**  
- `2017` Schrön, Köhli, Scheiffele, Iwema, Bogena, Lv, Martini, Baroni, ........, Zacharias  
**"Improving calibration and validation of cosmic-ray neutron sensors in the light of spatial sensitivity"**  
— *Hydrol. Earth Syst. Sci.*, doi:[10.5194/hess-21-5009-2017](https://doi.org/10.5194/hess-21-5009-2017), Citations: **100**  
- `2016` Schrön, Zacharias, Köhli, Weimar, Dietrich  
**"Monitoring Environmental Water with Ground Albedo Neutrons from Cosmic Rays"**  
— *Proceedings of The 34th International Cosmic Ray Conference  — PoS(ICRC2015)*, doi:[10.22323/1.236.0231](https://doi.org/10.22323/1.236.0231), Citations: **13**  
- `2016` Köhli, Allmendinger, Häußler, Schröder, Klein, Meven, Schmidt  
**"Efficiency and spatial resolution of the CASCADE thermal neutron detector"**  
— *Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment*, doi:[10.1016/j.nima.2016.05.014](https://doi.org/10.1016/j.nima.2016.05.014), Citations: **36**  
- `2015` Köhli\*, Schrön\*, Zreda, Schmidt, Dietrich, Zacharias *(\*shared 1st authorship)*  
**"Footprint characteristics revised for field‐scale soil moisture monitoring with cosmic‐ray neutrons"**  
— *Water Resources Research*, doi:[10.1002/2015wr017169](https://doi.org/10.1002/2015wr017169), Citations: **206**  

