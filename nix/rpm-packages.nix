{ lib
, releaseTools
, vmTools
, rootPackagesNewest
, fetchurl
, util-linux
, src
, checkinstall
}:

let
  extraPackages = [
    "make" "cmake" "gcc-c++" "gcc" "binutils"
    "libX11-devel" "libXpm-devel" "libXft-devel" "libXext-devel"
    "python" "openssl-devel" "qt5-qtbase-devel"
  ];
in
  lib.genAttrs
  [
    "centos7x86_64"
    "fedora27x86_64"
  ]
  (dist:
    releaseTools.rpmBuild {
      diskImage =# vmTools.diskImageFuns.centos7x86_64 { inherit extraPackages; };
      let
          version = "7.5.1804";
        in vmTools.diskImageFuns."${dist}" rec {
#          name = "centos-${version}-x86_64";
#          fullName = "CentOS ${version} (x86_64)";
#          urlPrefix = "mirror://centos/${version}/os/x86_64";
#          packagesList = fetchurl rec {
#            url = "${urlPrefix}/repodata/${sha256}-primary.xml.gz";
#            sha256 = "6afcd31233c69da026f363eece2a99e5a79842313fef23bc76b96b69e90dd49d";
#          };
          inherit extraPackages;
#          preInstall = ''
#            mknod -m 666 /dev/random c 1 8
#            mknod -m 666 /dev/urandom c 1 9
#          '';
        };
      diskImageFormat = "qcow2";
      memSize = 4096;
      inherit src;
      name = "uranos-rpm";
      unpackPhase = ''
        cp -r $src/* .
      '';

      buildPhase = ''
        ln -s ${rootPackagesNewest dist} root
        export LD_LIBRARY_PATH=${rootPackagesNewest dist}/lib
        cp src/ForLinux/mainwindow.ui src/
        ${if dist == "centos7x86_64" then
        ''
          echo replacing files:
          ls -la src/ForLinux/QT5_9_7
          cp src/ForLinux/QT5_9_7/* src/
        '' else ""}

        echo LD_LIBRARY_PATH = $LD_LIBRARY_PATH
        qmake-qt5 .
        make

        export LOGNAME=root
        export PAGER=cat
        mkdir -p /usr/bin /usr/share
        checkinstall --nodoc -y -D \
          --fstrans=no \
          --requires="${lib.concatStringsSep "," extraPackages}" \
          --provides="" \
          --pkgversion="$(echo ${src.version} | tr _ -)" \
          --maintainer="Markus Koehli <koehli@physi.uni-heidelberg.de>" \
          --pkgname=uranos \
          -- make install

        mkdir -p $out/rpms
        find . -name "*.rpm" -exec cp {} $out/rpms \;
      '';
          
    }
  )
