{
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
  inputs.nix-appimage.url = github:ralismark/nix-appimage;

  outputs = { self, nixpkgs, nix-appimage, ... }@args:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      config = {
        permittedInsecurePackages = [ "checkinstall-1.6.2" ];
      };
      overlays = [
        (import nix/root-packages.nix)
      ];
    };
  in {
    packages.x86_64-linux = rec {
      src = pkgs.stdenvNoCC.mkDerivation rec {
        name = "uranos-source";
        version = "1.10";
        srcs = [ ./data ./resources ./src ./ui ./UranosGUI.pro ./uranos.spec ];
        sourceRoot = ".";
        phases = [ "unpackPhase" ];
        unpackPhase = ''
          mkdir $out
          srcs=( $srcs )
          echo ''${srcs[1]}
          cp -r ''${srcs[0]} $out/data
          cp -r ''${srcs[1]} $out/resources
          cp -r ''${srcs[2]} $out/src
          cp -r ''${srcs[3]} $out/ui
          cp -r ''${srcs[4]} $out/UranosGUI.pro
          cp -r ''${srcs[5]} $out/uranos.spec
        '';
      };

      uranos = pkgs.libsForQt5.callPackage nix/package.nix { inherit src; };
      default = uranos;
      appimage = import nix/appimage.nix (args // { inherit src; });
       
      debPackages = pkgs.callPackage nix/deb-packages.nix { inherit src; };
      rpmPackages = pkgs.callPackage nix/rpm-packages.nix { inherit src; };

      releaseFiles = pkgs.runCommand "uranos-release-files" {} ''
        mkdir $out
        ln -s ${appimage} $out/UranosGUI-${src.version}-x86_64.AppImage
      '';
    };
  };
}
