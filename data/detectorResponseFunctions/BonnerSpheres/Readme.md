Detector Response Functions are energy-dependent detection probability characterizations of CRNS systems. The files provided here are tab-separated ASCII files with the columns energy [MeV] and detection probability [not normalized]

The Bonner Sphere Reference Functions are taken from 10.1016/0168-9002(91)90210-H. The Spheres contain a 3.2 cm diameter Centronics Helium-3 counter. 
