Detector Response Functions are energy-dependent detection probability characterizations of CRNS systems. The files provided here are tab-separated ASCII files with the columns energy [MeV] and detection probability

In case the maximum detection probability in the file exceeds 1.0, the response function will be normalized to a maximum value of 1. In case no value exceeds 1.0 the response function probabilities will be taken as provided.
