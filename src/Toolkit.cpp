/***************************************************************************
**                                                                        **
**  URANOS - Ultra RApid Neutron-Only Simulation                          **
**  designed for Environmental Research                                   **
**  Copyright (C) 2015-2024 Markus Koehli,                                **
**  Physikalisches Institut, Heidelberg University, Germany               **
**                                                                        **
****************************************************************************/


#include "Toolkit.h"

#include "TRandom3.h"

//--------------------------------------------------------------------------
// ++++++++++++++++++++++ Additional functions used in URANOS ++++++++++++++


//--------------------------------------------------------------------------
// ++++++++++++++++++++++ Fit functions ++++++++++++++++++++++++++++++++++++



//Lorentzian Peak Function with offset and slope
Double_t  lorentzianPeak(Double_t* x, Double_t* par)
{
    return (0.5*par[0]*par[1]/TMath::Pi()) / TMath::Max(1.e-10,(x[0]-par[2])*(x[0]-par[2])+ .25*par[1]*par[1]) +par[3];
}

Double_t  gaussoffset(Double_t* x, Double_t* par)
{
    //two gaussians
    return (par[0]*( TMath::Exp(-0.5*TMath::Power(((x[0]-par[1])/par[2]),2)) ) +par[3]);
}

Double_t errf( Double_t *x, Double_t *par)
{
  return par[0]*TMath::Erf((x[0]-par[1])/TMath::Sqrt(2)/par[2])+ par[3];
}


// configures the ROOT output for plots
void  rootlogon()
{
    bool largeStyle = false;
    bool atlasStyle = true;

    bool tightMargins = false;
    bool largeMargins = true;

    TStyle *plain = new TStyle("Plain", "Plain Style");
    plain->SetCanvasBorderMode(0);
    plain->SetPadBorderMode(0);
    plain->SetPadColor(0);
    plain->SetCanvasColor(0);
    plain->SetTitleColor(1);
    //plain->SetTitleSize(0.001);
    plain->SetStatColor(0);
    plain->SetPalette(1) ;
    plain->SetAxisColor(1, "X");

    plain->SetGridWidth(0.1);
    plain->SetGridColor(13);
    plain->SetGridStyle(3);

    gROOT->SetStyle( "Plain" );

    gStyle->SetPaperSize(20, 26);//A4
    gStyle->SetPalette(1) ;

    gStyle->SetLineWidth(1.);
    //gStyle->SetHistLineWidth(0.5);

    //plain->SetTextSize(1.1);
    plain->SetLineStyleString(2,"[12 12]");

    plain->SetPadTickX(1);
    plain->SetPadTickY(1);

    gStyle->SetLabelSize(0.025,"xy");
    gStyle->SetTitleSize(0.025,"xy");
    //gStyle->SetTitleOffset(1.,"x");
    //gStyle->SetTitleOffset(1.,"y");
    //gStyle->SetPadTopMargin(0.1);
    //gStyle->SetPadRightMargin(0.1);
    //gStyle->SetPadBottomMargin(0.16);
    //gStyle->SetPadLeftMargin(0.12);
    //gStyle->SetOptFit(0111);
    gStyle->SetOptFit(0000);

    TGaxis::SetMaxDigits(3);
    gStyle->SetOptStat("");
    //gStyle->SetOptStat(1111);
    //gStyle->SetOptStat("e");

    if (largeStyle)
    {
        //gStyle->SetLineWidth(3.8);
        //gStyle->SetHistLineWidth(3.8);
        gStyle->SetTextSize(2.);

        gStyle->SetLabelSize(0.04,"xy");
        gStyle->SetTitleSize(0.042,"xy");
        //gStyle->SetTitleOffset(1.4,"x");
        //gStyle->SetTitleOffset(1.4,"y");
        //gStyle->SetTitleOffset(1.7,"y");
    }

    // ATLAS Style
    if (atlasStyle)
    {
        Int_t font=42;
        Double_t tsize=0.025;
        Double_t zsize=0.025;
        plain->SetTextFont(font);
        plain->SetTextSize(tsize);
        plain->SetLabelFont(font,"x");
        plain->SetTitleFont(font,"x");
        plain->SetLabelFont(font,"y");
        plain->SetTitleFont(font,"y");
        plain->SetLabelFont(font,"z");
        plain->SetTitleFont(font,"z");
        plain->SetLabelSize(tsize,"x");
        plain->SetTitleSize(tsize,"x");
        plain->SetLabelSize(tsize,"y");
        plain->SetTitleSize(tsize,"y");
        plain->SetLabelSize(tsize,"z");
        plain->SetTitleSize(tsize,"z");

        plain->SetLegendFont(font);

        //this is really a fucker
        plain->SetFuncWidth(1.5);

        if(tightMargins)
        {
            plain->SetPaperSize(20,26);
            plain->SetPadTopMargin(0.05);
            plain->SetPadRightMargin(0.05);
            plain->SetPadBottomMargin(0.13);
            plain->SetPadLeftMargin(0.12);
        }
        else
        {
            plain->SetPaperSize(20,26);
            plain->SetPadTopMargin(0.05);
            plain->SetPadRightMargin(0.07);
            plain->SetPadBottomMargin(0.10);
            plain->SetPadLeftMargin(0.07);
        }
        if (largeMargins)
        {
            plain->SetPaperSize(20,26);
            plain->SetPadTopMargin(0.06);
            plain->SetPadRightMargin(0.11);
            plain->SetPadBottomMargin(0.11);
            plain->SetPadLeftMargin(0.11);
        }
        plain->SetOptTitle(0);

        Int_t icol=0;
        plain->SetFrameBorderMode(icol);
        plain->SetCanvasBorderMode(icol);
        plain->SetPadBorderMode(icol);
        plain->SetPadColor(icol);
        plain->SetCanvasColor(icol);
        plain->SetStatColor(icol);
    }
    plain->SetBarWidth(3);

    //plain->SetPadTopMargin(0.0);	plain->SetPadRightMargin(0.0);	plain->SetPadBottomMargin(0.0);	plain->SetPadLeftMargin(0.00);
}



//convert colors to RGB
 std::vector<float> getRGBfromHCL(double h, double c0, double l0)
 {
    using namespace std;
    vector<float> rgbValues;
    float r,g,b;

    float c = c0*l0;
    float m = l0-c;

    float h0 = h/60.;
    int h0i = floor(h0+0.5);

    float x;

    if (h0 < 2) 		x = c*(1-fabs(h0 - 1));
    else
    {
        if	(h0 < 4)	x = c*(1-fabs(h0-2 - 1));
        else		x = c*(1-fabs(h0-4 - 1));
    }

    if (h0<1)
    {
        r = c; g = x; b = 0;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }
    if (h0<2)
    {
        r = x; g = c; b = 0;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }
    if (h0<3)
    {
        r = 0; g = c; b = x;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }
    if (h0<4)
    {
        r = 0; g = x; b = c;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }
    if (h0<5)
    {
        r = x; g = 0; b = c;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }
    if (h0<6)
    {
        r = c; g = 0; b = x;
        rgbValues.push_back(r+m); rgbValues.push_back(g+m); rgbValues.push_back(b+m);
        return rgbValues;
    }

    rgbValues.push_back(0); rgbValues.push_back(0); rgbValues.push_back(0);
    return rgbValues;
 }

// linear function used to generate color codes
 float getLinearC(double factor, double cmin, double cmax)
 {
     return (cmax-(factor*(cmax-cmin)));
 }

 // power function used to generate color codes
 float getScaledColorValue(double factor, double pow, double cmin, double cmax)
 {
     return (cmax-(TMath::Power(factor,pow)*(cmax-cmin)));
 }

 // set up a linear color table with a number of NCont colors and a number of NRGBs scaled color values in between
 // the scale values are set by the a_i e (0,1)
 // values of C and L, bothe e (0,1) are set to default values
 // typically only the color h e (0,360) has to be varied
void  set_plot_styleSingleGradient(float h, Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    float cmin = 1;
    float cmax = 0.3;
    float lmax = 0.9;
    float lmin = 0.3;
    float pow = 1.5;

    vector<float> stop0 = getRGBfromHCL(h,getScaledColorValue(a0,pow,cmin,cmax),getScaledColorValue(a0,pow,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(h,getScaledColorValue(a1,pow,cmin,cmax),getScaledColorValue(a1,pow,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(h,getScaledColorValue(a2,pow,cmin,cmax),getScaledColorValue(a2,pow,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(h,getScaledColorValue(a3,pow,cmin,cmax),getScaledColorValue(a3,pow,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(h,getScaledColorValue(a4,pow,cmin,cmax),getScaledColorValue(a4,pow,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { stop0.at(0), stop1.at(0), stop2.at(0), stop3.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop0.at(1), stop1.at(1), stop2.at(1), stop3.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop0.at(2), stop1.at(2), stop2.at(2), stop3.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

 // set up a two-color table with a number of NCont colors and a number of NRGBs scaled color values in between
 // the scale values are set by the a_i e (0,1)
 // values of C and L, bothe e (0,1) are set to default values
 // typically only the colors h1 and h2 e (0,360) have to be varied
void  set_plot_styleHeatGradient(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    float cmin = 1;
    float cmax = 0.6;
    float lmax = 0.9;
    float lmin = 0.5;
    float pow1 = 0.4;
    float pow2 = 2.2;
    float h1 = 00;
    float h2 = 60;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { stop0.at(0), stop1.at(0), stop2.at(0), stop3.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop0.at(1), stop1.at(1), stop2.at(1), stop3.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop0.at(2), stop1.at(2), stop2.at(2), stop3.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

// different gradient, see definition 'set_plot_styleHeatGradient'
void  set_plot_styleHeatGradient2(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    float cmin = 1;
    float cmax = 0.6;
    float lmax = 0.9;
    float lmin = 0.5;
    float pow1 = 0.4;
    float pow2 = 2.2;
    float h1 = 00;
    float h2 = 60;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { stop4.at(0), stop3.at(0), stop2.at(0), stop1.at(0), stop0.at(0) };
    Double_t green[NRGBs] = { stop4.at(1), stop3.at(1), stop2.at(1), stop1.at(1), stop0.at(1) };
    Double_t blue[NRGBs]  = { stop4.at(2), stop3.at(2), stop2.at(2), stop1.at(2), stop0.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

// different gradient, see definition 'set_plot_styleHeatGradient'
void  set_plot_styleHeatGradientModified(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    //a0 = 2*a0; a1 = 2*a1;a2 = 2*a2;
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    float cmin = 1;
    float cmax = 0.7;
    float lmax = 0.95;
    float lmin = 0.5;
    float pow1 = 0.8;
    float pow2 = 2.6;
    float h1 = 05;
    float h2 = 60;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { stop4.at(0), stop2.at(0), stop0.at(0), stop2.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop4.at(1), stop2.at(1), stop0.at(1), stop2.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop4.at(2), stop2.at(2), stop0.at(2), stop2.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

// different gradient, see definition 'set_plot_styleHeatGradient'
void  set_plot_styleRainbowGradient(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    const Int_t NRGBs = 9;
    const Int_t NCont = 200;

    float cmin = 0.91;
    float cmax = 0.9;
    float lmax = 0.95;
    float lmin = 0.85;
    float pow1 = 0.4; //pow1 = 1.5;
    float pow2 = 2.2; //pow2 = 1.5;
    float h1 = 0;
    float h2 = 250;

    Double_t a01 = (a0+a1)/2.; Double_t a12 = (a1+a2)/2.; Double_t a23 = (a2+a3)/2.; Double_t a34 = (a3+a4)/2.;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop01 = getRGBfromHCL(getLinearC(a01,h1,h2),getScaledColorValue(a01,pow1,cmin,cmax),getScaledColorValue(a01,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop12 = getRGBfromHCL(getLinearC(a12,h1,h2),getScaledColorValue(a12,pow1,cmin,cmax),getScaledColorValue(a12,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop23 = getRGBfromHCL(getLinearC(a23,h1,h2),getScaledColorValue(a23,pow1,cmin,cmax),getScaledColorValue(a23,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop34 = getRGBfromHCL(getLinearC(a34,h1,h2),getScaledColorValue(a34,pow1,cmin,cmax),getScaledColorValue(a34,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a01, a1, a12, a2, a23, a3, a34, a4 };
    Double_t red[NRGBs]   = { stop0.at(0), stop01.at(0), stop1.at(0), stop12.at(0), stop2.at(0), stop23.at(0), stop3.at(0), stop34.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop0.at(1), stop01.at(1), stop1.at(1), stop12.at(1), stop2.at(1), stop23.at(1), stop3.at(1), stop34.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop0.at(2), stop01.at(2), stop1.at(2), stop12.at(2), stop2.at(2), stop23.at(2), stop3.at(2), stop34.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

// different gradient, see definition 'set_plot_styleHeatGradient'
void  set_plot_styleAllGradient(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    using namespace std;
    const Int_t NRGBs = 9;
    const Int_t NCont = 200;

    float cmin = 0.991;
    float cmax = 0.8;
    float lmax = 0.95;
    float lmin = 0.9;
    float pow1 = 1.5; //pow1 = 1.5;
    float pow2 = 1.5; //pow2 = 1.5;
    float h1 = 0;
    float h2 = 359;

    Double_t a01 = (a0+a1)/2.; Double_t a12 = (a1+a2)/2.; Double_t a23 = (a2+a3)/2.; Double_t a34 = (a3+a4)/2.;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop01 = getRGBfromHCL(getLinearC(a01,h1,h2),getScaledColorValue(a01,pow1,cmin,cmax),getScaledColorValue(a01,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop12 = getRGBfromHCL(getLinearC(a12,h1,h2),getScaledColorValue(a12,pow1,cmin,cmax),getScaledColorValue(a12,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop23 = getRGBfromHCL(getLinearC(a23,h1,h2),getScaledColorValue(a23,pow1,cmin,cmax),getScaledColorValue(a23,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop34 = getRGBfromHCL(getLinearC(a34,h1,h2),getScaledColorValue(a34,pow1,cmin,cmax),getScaledColorValue(a34,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a01, a1, a12, a2, a23, a3, a34, a4 };
    Double_t red[NRGBs]   = { stop0.at(0), stop01.at(0), stop1.at(0), stop12.at(0), stop2.at(0), stop23.at(0), stop3.at(0), stop34.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop0.at(1), stop01.at(1), stop1.at(1), stop12.at(1), stop2.at(1), stop23.at(1), stop3.at(1), stop34.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop0.at(2), stop01.at(2), stop1.at(2), stop12.at(2), stop2.at(2), stop23.at(2), stop3.at(2), stop34.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}


//formats the colorbar to a non-inhumanic scheme
void  set_plot_style(Double_t a0, Double_t a1, Double_t a2, Double_t a3, Double_t a4)
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}


// different gradient, see definition 'set_plot_styleHeatGradient'
void  set_plot_styleCool()
{
    using namespace std;
    float a0 = 0, a1 = 0.56, a2= 0.56, a3 = 0.56, a4 = 1;
    const Int_t NRGBs = 5;
    const Int_t NCont = 200;

    float cmin = 1;
    float cmax = 0.3;
    float lmax = 0.99;
    float lmin = 0.6;
    float pow1 = 0.4;
    float pow2 = 2.2;
    float h1 = 230;
    float h2 = 360;

    vector<float> stop0 = getRGBfromHCL(getLinearC(a0,h1,h2),getScaledColorValue(a0,pow1,cmin,cmax),getScaledColorValue(a0,pow2,lmin,lmax));
    vector<float> stop1 = getRGBfromHCL(getLinearC(a1,h1,h2),getScaledColorValue(a1,pow1,cmin,cmax),getScaledColorValue(a1,pow2,lmin,lmax));
    vector<float> stop2 = getRGBfromHCL(getLinearC(a2,h1,h2),getScaledColorValue(a2,pow1,cmin,cmax),getScaledColorValue(a2,pow2,lmin,lmax));
    vector<float> stop3 = getRGBfromHCL(getLinearC(a3,h1,h2),getScaledColorValue(a3,pow1,cmin,cmax),getScaledColorValue(a3,pow2,lmin,lmax));
    vector<float> stop4 = getRGBfromHCL(getLinearC(a4,h1,h2),getScaledColorValue(a4,pow1,cmin,cmax),getScaledColorValue(a4,pow2,lmin,lmax));

    Double_t stops[NRGBs] = { a0, a1, a2, a3, a4 };
    Double_t red[NRGBs]   = { stop0.at(0), stop1.at(0), stop2.at(0), stop3.at(0), stop4.at(0) };
    Double_t green[NRGBs] = { stop0.at(1), stop1.at(1), stop2.at(1), stop3.at(1), stop4.at(1) };
    Double_t blue[NRGBs]  = { stop0.at(2), stop1.at(2), stop2.at(2), stop3.at(2), stop4.at(2) };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

// modes 0: no luminance scaling, 1: dark at max scale 2: dark at min scale
int getScaledColor(float min, float max, float scale, int mode)
{
    using namespace std;
    vector<float> rgbValues;
    if (mode==0) rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.4,0.91,0.9),getScaledColorValue(scale,2.2,0.8,0.9));
    else if (mode==1) rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.2,0.99,0.5),getScaledColorValue(scale,1.5,0.1,0.99));
    else rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.2,0.99,0.1),getScaledColorValue(scale,0.9,0.99,0.2));

    return TColor::GetColor(rgbValues.at(0),rgbValues.at(1),rgbValues.at(2));
}

// modes 0: no luminance scaling, 1: dark at max scale 2: dark at min scale
std::vector<float> getScaledColorRGB(float min, float max, float scale, int mode)
{
    using namespace std;
    vector<float> rgbValues;
    if (mode==0) rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.4,0.91,0.9),getScaledColorValue(scale,2.2,0.8,0.9));
    else if (mode==1) rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.2,0.99,0.5),getScaledColorValue(scale,1.5,0.1,0.99));
    else rgbValues = getRGBfromHCL(getLinearC(scale,min,max),getScaledColorValue(scale,0.2,0.99,0.1),getScaledColorValue(scale,0.9,0.99,0.2));

    return rgbValues;
}


// fashions up the canvas
void  CanvasFashion(TCanvas* c)
{
    c->SetBorderMode(0);
    c->SetFillColor(0);
}

// fashions up TGraphs
// set axis labels and label plot size to large or small using 'setSize'
void TGraphFashion(TGraph* graph, TString xLabel, TString yLabel, Bool_t setSize)
{
    graph->SetFillColor(19);
    graph->SetMarkerStyle(21);
    graph->SetMarkerSize(1);
    graph->GetXaxis()->SetTitle(xLabel);
    graph->GetYaxis()->SetTitle(yLabel);
    graph->GetXaxis()->SetNoExponent(kTRUE);
    graph->GetYaxis()->SetNoExponent(kTRUE);
    if (setSize)
    {
    graph->GetXaxis()->SetLabelSize(0.027);
    graph->GetXaxis()->SetTitleSize(0.03);
    graph->GetYaxis()->SetLabelSize(0.027);
    graph->GetYaxis()->SetTitleSize(0.03);
    }
}

// fashions up TGraphErrors
// set axis labels and label plot size to large or small using 'setSize'
void TGraphErrorFashion(TGraphErrors* graph, TString xLabel, TString yLabel, Bool_t setSize)
{
    graph->SetFillColor(19);
    graph->SetMarkerStyle(21);
    graph->SetMarkerSize(1.0);
    graph->GetXaxis()->SetTitle(xLabel);
    graph->GetYaxis()->SetTitle(yLabel);
    graph->GetXaxis()->SetNoExponent(kTRUE);
    graph->GetYaxis()->SetNoExponent(kTRUE);
    if (setSize)
    {
    graph->GetXaxis()->SetLabelSize(0.04);
    graph->GetXaxis()->SetTitleSize(0.03);
    graph->GetYaxis()->SetLabelSize(0.04);
    graph->GetYaxis()->SetTitleSize(0.03);
    }
}

// fashions up TGraph2Ds
// set axis labels and label plot size to large or small using 'setSize'
void TGraph2DFashion(TGraph2D* graph, TString xLabel, TString yLabel, TString zLabel, Bool_t setSize)
{
    graph->SetFillColor(19);
    graph->SetMarkerStyle(21);
    graph->SetMarkerSize(1);
    graph->GetXaxis()->SetTitle(xLabel);
    graph->GetYaxis()->SetTitle(yLabel);
    graph->GetZaxis()->SetTitle(zLabel);
    graph->GetXaxis()->SetNoExponent(kTRUE);
    graph->GetZaxis()->SetNoExponent(kTRUE);
    graph->GetYaxis()->SetNoExponent(kTRUE);
    if (setSize)
    {
    graph->GetXaxis()->SetLabelSize(0.027);
    graph->GetXaxis()->SetTitleSize(0.03);
    graph->GetYaxis()->SetLabelSize(0.027);
    graph->GetYaxis()->SetTitleSize(0.03);
    graph->GetZaxis()->SetLabelSize(0.027);
    graph->GetZaxis()->SetTitleSize(0.03);
    }
}

// fashions up TMultiGraphs
// set axis labels and label plot size to large or small using 'setSize'
void TMultiGraphFashion(TMultiGraph* graph, TString xLabel, TString yLabel, Bool_t setSize)
{
    graph->GetXaxis()->SetTitle(xLabel);
    graph->GetYaxis()->SetTitle(yLabel);
    graph->GetXaxis()->SetNoExponent(kTRUE);
    graph->GetYaxis()->SetNoExponent(kTRUE);
    if (setSize)
    {
    graph->GetXaxis()->SetLabelSize(0.027);
    graph->GetXaxis()->SetTitleSize(0.03);
    graph->GetYaxis()->SetLabelSize(0.027);
    graph->GetYaxis()->SetTitleSize(0.03);
    }
}

// fashions up TF1 functions
// set axis labels and label plot size to large or small using 'setSize'
void  TF1Fashion(TF1* hist, TString xLabel, TString yLabel, Bool_t setSize)
{
    if (setSize)
    {
    hist->GetXaxis()->SetLabelSize(0.027);
    hist->GetXaxis()->SetTitleSize(0.03);
    hist->GetYaxis()->SetLabelSize(0.027);
    hist->GetYaxis()->SetTitleSize(0.03);
    }
    hist->GetYaxis()->SetTitle(xLabel);
    hist->GetXaxis()->SetTitle(yLabel);
    hist->GetXaxis()->SetNoExponent(kTRUE);
    hist->GetYaxis()->SetNoExponent(kTRUE);
    //hist->SetBarOffset(-0.30);
    //hist->SetBarWidth(0.5);
}

// fashions up TH1 histograms
// set axis labels and label plot size to large or small using 'setSize'
void  TH1Fashion(TH1* hist, TString xLabel, TString yLabel, Bool_t setSize)
{
    if (setSize)
    {
    hist->GetXaxis()->SetLabelSize(0.027);
    hist->GetXaxis()->SetTitleSize(0.03);
    hist->GetYaxis()->SetLabelSize(0.027);
    hist->GetYaxis()->SetTitleSize(0.03);
    }
    hist->GetYaxis()->SetTitle(xLabel);
    hist->GetXaxis()->SetTitle(yLabel);
    hist->GetXaxis()->SetNoExponent(kTRUE);
    hist->GetYaxis()->SetNoExponent(kTRUE);
    #ifdef ROOTOLDVERSION
    hist->SetBarOffset(-0.30);
    #endif
    //hist->SetBarWidth(0.5);
}

// fashions up TProfile histograms
// set axis labels and label plot size to large or small using 'setSize'
void  TProfileFashion(TProfile* hist, TString xLabel, TString yLabel, Bool_t setSize)
{
    if (setSize)
    {
    hist->GetXaxis()->SetLabelSize(0.027);
    hist->GetXaxis()->SetTitleSize(0.03);
    hist->GetYaxis()->SetLabelSize(0.027);
    hist->GetYaxis()->SetTitleSize(0.03);
    }
    hist->GetYaxis()->SetTitle(xLabel);
    hist->GetXaxis()->SetTitle(yLabel);
    hist->GetXaxis()->SetNoExponent(kTRUE);
    hist->GetYaxis()->SetNoExponent(kTRUE);
    #ifdef ROOTOLDVERSION
    hist->SetBarOffset(-0.30);
    #endif
    //hist->SetBarWidth(0.5);
}


// fashions up TH2 histograms for the pixelwise plots
// set axis labels and label plot size to large or small using 'setSize'
void  TH2Fashion(TH2* hist, Bool_t setSize)
{
    //hist->GetXaxis()->SetRange(0,255);
    //hist->GetYaxis()->SetRange(0,255);
    if (setSize)
    {
        hist->GetXaxis()->SetLabelSize(0.02);
        hist->GetXaxis()->SetTitleSize(0.03);
        hist->GetYaxis()->SetLabelSize(0.02);
        hist->GetYaxis()->SetTitleSize(0.03);
        hist->GetZaxis()->SetLabelSize(0.015);
    }
    if (setSize)
    {
        hist->GetXaxis()->SetLabelSize(0.035);
        hist->GetXaxis()->SetTitleSize(0.037);
        hist->GetYaxis()->SetLabelSize(0.035);
        hist->GetYaxis()->SetTitleSize(0.037);
        hist->GetYaxis()->SetTitleOffset(1.);
    }
    hist->GetXaxis()->SetTitle("Pixel");
    hist->GetYaxis()->SetTitle("Pixel");
    hist->GetXaxis()->SetNoExponent(kTRUE);
    hist->GetYaxis()->SetNoExponent(kTRUE);
    //gStyle->SetOptStat(0);
}

// fashions up TH1 histograms
// set axis labels and label plot size to large or small using 'setSize'
void  TH2Fashion(TH2* hist, TString xLabel, TString yLabel, Bool_t setSize)
{
    //hist->GetXaxis()->SetRange(0,255);
    //hist->GetYaxis()->SetRange(0,255);
    if (setSize)
    {
        hist->GetXaxis()->SetLabelSize(0.02);
        hist->GetXaxis()->SetTitleSize(0.03);
        hist->GetYaxis()->SetLabelSize(0.02);
        hist->GetYaxis()->SetTitleSize(0.03);
        hist->GetZaxis()->SetLabelSize(0.015);
    }
    else
    {
        hist->GetXaxis()->SetLabelSize(0.035);
        hist->GetXaxis()->SetTitleSize(0.045);
        hist->GetYaxis()->SetLabelSize(0.035);
        hist->GetYaxis()->SetTitleSize(0.045);
        hist->GetZaxis()->SetLabelSize(0.03);
    }
    if (false)
    {
        hist->GetXaxis()->SetLabelSize(0.035);
        hist->GetXaxis()->SetTitleSize(0.037);
        hist->GetYaxis()->SetLabelSize(0.035);
        hist->GetYaxis()->SetTitleSize(0.037);
        hist->GetYaxis()->SetTitleOffset(1.);
    }
    hist->GetXaxis()->SetTitle(xLabel);
    hist->GetYaxis()->SetTitle(yLabel);
    hist->GetXaxis()->SetNoExponent(kTRUE);
    hist->GetYaxis()->SetNoExponent(kTRUE);
    //gStyle->SetOptStat(0);
}

// fashions up stacked histograms
// set axis labels and label plot size to large or small using 'setSize'
void  THStackFashion(THStack* stack, TString xLabel, TString yLabel, Bool_t setSize)
{
    //hist->GetXaxis()->SetRange(0,255);
    //hist->GetYaxis()->SetRange(0,255);
    if (setSize)
    {
        stack->GetXaxis()->SetLabelSize(0.03);//originally "2"
        stack->GetXaxis()->SetTitleSize(0.03);
        stack->GetYaxis()->SetLabelSize(0.03);//originally "2"
        stack->GetYaxis()->SetTitleSize(0.03);
    }
    else
    {
        stack->GetXaxis()->SetLabelSize(0.035);
        stack->GetXaxis()->SetTitleSize(0.045);
        stack->GetYaxis()->SetLabelSize(0.035);
        stack->GetYaxis()->SetTitleSize(0.045);
    }
    stack->GetXaxis()->SetTitle(xLabel);
    stack->GetYaxis()->SetTitle(yLabel);
    stack->GetXaxis()->SetNoExponent(kTRUE);
    stack->GetYaxis()->SetNoExponent(kTRUE);
    //gStyle->SetOptStat(0);
}



//--------------------------------------------------------------------------
// ++++++++++++++++++++++ Output/Input +++++++++++++++++++++++++++++++++++++


// plots a TMatrix with z values from zmin to zmax to the specified folder and filename
TH2F* printHisto(Double_t zmin, Double_t zmax, Int_t size, TMatrixF data, TString OutputFolder, TString dataname, Bool_t logPlot)
{
    using namespace std;
    //cout<<"Generating histograms";

    Float_t pixel;
    TH2F* pixelmatrix = new TH2F("p"+dataname, dataname ,size,0.,size-1,size,0.,size-1);

    for(Int_t  l = 0; l < size; l++)
    {
        for(Int_t  m = 0; m < size; m++)
        {
            pixel = data(l,m);

            pixelmatrix->Fill(l,m, pixel);
        }
    }
    //cout<<"...done"<<endl;

    cout<<"Mean x: "<<pixelmatrix->GetMean(1)<<"+/-"<<pixelmatrix->GetMeanError(1)<<" y: "<<pixelmatrix->GetMean(2)<<"+/-"<<pixelmatrix->GetMeanError(2)<<endl;

    TCanvas* c = new TCanvas("c"+dataname,dataname,1280,1280);
    CanvasFashion(c);
    if (logPlot) c->SetLogz();

    TH2Fashion(pixelmatrix,1);
    pixelmatrix->GetZaxis()->SetRangeUser(zmin,zmax);

    set_plot_style(0.00, 0.20, 0.50, 0.7, 1.00);
    pixelmatrix->Draw("COLZ");

    c->SaveAs(OutputFolder+"/"+dataname+".png");
    c->Print(OutputFolder+"/"+dataname+".root");

    return pixelmatrix;
}


// plots a TH1 histogram with y values from zmin to zmax to the specified folder and filename
void  printTH1F(Double_t zmin, Double_t zmax, TH1F* histo, TString OutputFolder, TString dataname)
{
    TString separator;
    if (OutputFolder.Length() > 1) separator = "/";
    else separator = "";

    TCanvas* c = new TCanvas("c"+dataname,dataname,1280,1280);
    CanvasFashion(c);

    TH1Fashion(histo,"Bins","counts", 1);
    if ((zmin*zmax)!=1) histo->GetYaxis()->SetRangeUser(zmin,zmax);

    histo->Draw("");

    c->SaveAs(OutputFolder+separator+dataname+".png");
    //
    histo->Print(OutputFolder+separator+dataname+".root");
}

// plots a TH2 histogram with z values from zmin to zmax to the specified folder and filename
void  printTH2F(Double_t zmin, Double_t zmax, TH2F* histo, TString OutputFolder, TString dataname)
{
    TString separator;
    if (OutputFolder.Length() > 1) separator = "/";
    else separator = "";

    TCanvas* c = new TCanvas("c"+dataname,dataname,1280,1280);
    CanvasFashion(c);

    //TH2Fashion(histo);
    //if ((zmin!=0)&&(zmax!=0)) histo->GetZaxis()->SetRangeUser(zmin,zmax);

    if ((zmin!=0)&&(zmax!=0)) c->SetLogz();

    set_plot_style(0.00, 0.20, 0.50, 0.7, 1.00);
    histo->Draw("COLZ");

    c->SaveAs(OutputFolder+separator+dataname+".png");
    histo->Print(OutputFolder+separator+dataname+".root");
}

// exports a TH2 histogram as a tab separated ASCII matrix
void  storeTH2ToFile(TString OutputFolder, TString filename, TH2F* th2)
{
    using namespace std;
    ofstream *stream_out;

    TString separator;
    if (OutputFolder.Length() > 1) separator = "/";
    else separator = "";

    stream_out = new ofstream(OutputFolder+separator+filename,ofstream::out);

    Int_t nX = th2->GetNbinsX();
    Int_t nY = th2->GetNbinsY();

    for(Int_t  i = 0; i < nY; i++)
    {
        for(Int_t  j = 0; j < nX; j++)
        {
            *stream_out<<castDoubleToString(th2->GetBinContent(j,i))<<"\t";
        }
        *stream_out<<endl;
    }
    stream_out->close();
}

// exports an ASCII string to a file
void  storeToFile(TString OutputFolder, TString filename, TString what_has_to_be_written, bool new_line)
{
    using namespace std;
    ofstream *stream_out;

    TString separator;
    if (OutputFolder.Length() > 1) separator = "/";
    else separator = "";

    stream_out = new ofstream(OutputFolder+separator+filename,ofstream::app);

    *stream_out<<what_has_to_be_written;

    if (new_line) *stream_out<<endl;

    stream_out->close();
}

// embeds the drawing fonts into the vector graphics, which is not done by default by ROOT
// uses a powershell script invoking ghostscript
// should be generalized without hard coded file paths
 void embedFonts(TString path)
 {
    using namespace std;
     ofstream *stream_out;
     stream_out = new ofstream("fontEmbedscript.ps1",ofstream::out);

     *stream_out<<"$msbuild = \"C:\\Program Files\\gs\\gs9.18\\bin\\gswin64c.exe\""<<endl;
     *stream_out<<"$arguments = \"-q -dNOPAUSE -dBATCH -dPDFSETTINGS=/printer  -sDEVICE=pdfwrite  -sOutputFile="<<path<<".bak "<<path<<"\""<<endl;
     *stream_out<<"start-process $msbuild $arguments -Wait"<<endl;
     *stream_out<<"remove-item "<<path<<endl;
     *stream_out<<"rename-item "<<path<<".bak -newname "<<path<<endl;
     stream_out->close();

     system("powershell.exe -ExecutionPolicy Bypass -File fontEmbedscript.ps1");

     // $arguments = "-q -dNOPAUSE -dBATCH -dPDFSETTINGS=/prepress  -sDEVICE=pdfwrite  -sOutputFile=G:\CASCADE\Analyze\Heidi\Efficiency\calc\heidiEfficiencyNew3b.pdf G:\CASCADE\Analyze\Heidi\Efficiency\calc\heidiEfficiencyNew3.pdf"
     //start-process $msbuild $arguments
 }


// 16.01.2015
// exports a TH1 histogram to a tab separated ASCII matrix
void  storeTH1ToFile(TString OutputFolder, TString filename,TH1* histo)
{
     using namespace std;
    ofstream *stream_out;
    stream_out = new ofstream(OutputFolder+"/"+filename,ofstream::out);

    Int_t entries = histo->GetNbinsX();

    for(Int_t  l = 1; l < entries+1; l++)
    {
        *stream_out<<castDoubleToString(histo->GetBinCenter(l))<<"\t";
        *stream_out<<castDoubleToString(histo->GetBinContent(l))<<"\t"<<"0.0"<<"\t"<<castDoubleToString(sqrt(histo->GetBinContent(l)))<<endl;
    }

    stream_out->close();
}

// reads an image from the specified folder location and transforms it to a TMatrix
TMatrixF readMatrixPNG(TString folder, TString filename)
{
    using namespace std;
    QRgb pixelValue;
    string imageStr = std::string((folder+filename).Data())+".png";
    QImage matrixImage;
    matrixImage.load(QString::fromStdString(imageStr));
    int matrixWidthHere = matrixImage.width();
    int matrixHeightHere = matrixImage.height();

    TMatrixF matr(matrixWidthHere,matrixHeightHere);
    for (int i = 0; i<matrixWidthHere; ++i)
    {
        for (int j = 0; j<matrixHeightHere; ++j)
        {
            pixelValue = matrixImage.pixel(i,matrixHeightHere-1-j);
            matr(i,j) = qGray(pixelValue);   //cout<<matr(i,j);
        }
    }
    return matr;
}

// reads an n x n ASCII matrix from the specified folder
// requires the size n to be known
TMatrixF  readmatrix(TString folder, TString filename, TString filetype, Int_t counter, Int_t size)
{
    using namespace std;
    TMatrixF matrix(size,size);
    TString line, dname, separator;
    Float_t temp;
    Int_t i = 0;

    if (folder.Length() > 1) separator = "/";
    else separator = "";
    //fill every value from the file into the according matrix
    if (counter<10)	dname = folder+separator+filename+"0"+castIntToString(counter)+"."+filetype;
    else dname = folder+separator+filename+castIntToString(counter)+"."+filetype;

    if (counter == -1) dname = folder+separator+filename+"."+filetype;

    ifstream input_stream(dname,ios::in);
    while ( line.ReadLine(input_stream) )
    {
        istrstream stream(line.Data());

        for(Int_t  j = 0; j < size; j++)
        {            
            stream >> temp;
            matrix(i,j) = temp;            
        }
        i++;
        if(i == size) break;
    }
    input_stream.close();

    return matrix;
}

// counts the files of a specified filetype in the specified folder
Int_t  countFiles(TString folder, TString filename, TString filetype)
{
    using namespace std;
    cout<<"Total number of files: ";
    Int_t n = 0;
    ifstream f;

    TString separator;
    if (folder.Length() > 1) separator = "/";
    else separator = "";

    f.open(folder+separator+filename+"1"+"."+filetype);

    while (f.good())
    {
        f.close();
        n++;
        //if (n<10) f.open(folder+"/"+filename+"0"+castIntToString(n)+".dat");
        f.open(folder+separator+filename+castIntToString(n)+"."+filetype);
    }
    n--;
    cout<<n<<endl;
    f.close();
    return n;
}

// deprecated
int countEventsInEllipse(TH2F* histo, double radiusX, double x, double radiusY, double y)
{
    int eventcounter = 0;

    //int nX = 127, nY = 127;
    int x0 = x-2*radiusX;
    int y0 = y-2*radiusY;
    int nX = x+2*radiusX;
    int nY = y+2*radiusY;

    if (x0<1) x0 = 1;
    if (y0<1) y0 = 1;
    if (nX>127) nX = 127;
    if (nY>127) nY = 127;

    for(Int_t  i = x0; i <= nX; i++) {
        for(Int_t  j = y0; j <= nY; j++) {
            if ((TMath::Power(((i-x)/radiusX),2)+TMath::Power(((j-y)/radiusY),2))<=1)
            {
                eventcounter+=histo->GetBinContent(i,j);
            }
        }
    }
    return eventcounter;
}


//--------------------------------------------------------------------------
// ++++++++++++++++++++++ Cast functions +++++++++++++++++++++++++++++++++++

// should be converted to template functions

std::string  castDoubleToString(double number)
{
    using namespace std;
    TString s;
    s += number;
    return (string)s;
}

std::string  castDoubleToString(double number,  Int_t characters)
{
    using namespace std;
    TString s;
    s += number;
    s.Resize(characters);
    return (string)s;
}

std::string  castFloatToString(Float_t number)
{
    using namespace std;
    TString s;
    s += number;
    return (string)s;
}

std::string  castFloatToString(Float_t number, Int_t characters)
{
    using namespace std;
    TString s;
    s += number;
    s.Resize(characters);
    return (string)s;
}


std::string  castIntToString(Int_t &number)
{
    using namespace std;
    TString s;
    s += number;
    return (string)s;
}

std::string  castLongToString(Long_t &number)
{
    using namespace std;
    TString s;
    s += number;
    return (string)s;
}

// converts a hex value to base-10 integers
unsigned int heXheX(const char *value)
{
    struct CHexMap
    {
        char chr;
        int value;
    };

    const unsigned int HexMapL = 16;
    CHexMap HexMap[HexMapL] =
    {
        {'0', 0}, {'1', 1},
        {'2', 2}, {'3', 3},
        {'4', 4}, {'5', 5},
        {'6', 6}, {'7', 7},
        {'8', 8}, {'9', 9},
        {'A', 10}, {'B', 11},
        {'C', 12}, {'D', 13},
        {'E', 14}, {'F', 15}
    };

    const char* s = value;
    unsigned int result = 0;

    if (*s == '0' && *(s + 1) == 'X') s += 2;
    bool firsttime = true;
    while (*s != '\0')
    {
        bool found = false;
        for (int i = 0; i < HexMapL; i++)
        {
            if (toupper(*s) == HexMap[i].chr)
            {
                if (!firsttime) result <<= 4;
                result |= HexMap[i].value;
                found = true;
                break;
            }
        }
        if (!found) break;
        s++;
        firsttime = false;
    }

    return result;
}

const std::string intToHex( int i)
{
    std::ostringstream oss;
    oss << std::hex << i;
    return oss.str();
}

// fills zero values in a TMatrix by neighbor extrapolation
// deprecated
void extrapolateZeroValues(TMatrixF &matrix)
{
    Float_t corrSum, corrCounter;

    Int_t ylow = matrix.GetColLwb();
    Int_t ydim = matrix.GetColUpb()+1;
    Int_t xlow = matrix.GetRowLwb();
    Int_t xdim = matrix.GetRowUpb()+1;


    for(Int_t  y = ylow; y < ydim; y++)
    {
        for(Int_t  x = xlow; x < xdim; x++)
        {
            if (matrix(x,y)==0)
            {
                corrCounter = 0;
                corrSum = 0;

                if (x>xlow){
                    if (y>ylow){
                        if (matrix(x-1,y-1)!=0) {corrSum+=matrix(x-1,y-1); corrCounter++;}
                    }
                    if (matrix(x-1,y)!=0) {corrSum+=matrix(x-1,y); corrCounter++;}
                    if (y<ydim-1){
                        if (matrix(x-1,y+1)!=0) {corrSum+=matrix(x-1,y+1); corrCounter++;}
                    }
                }
                if (x<xdim-1){
                    if (y>ylow){
                        if (matrix(x+1,y-1)!=0) {corrSum+=matrix(x+1,y-1); corrCounter++;}
                    }
                    if (matrix(x+1,y)!=0) {corrSum+=matrix(x+1,y); corrCounter++;}
                    if (y<ydim-1){
                        if (matrix(x+1,y+1)!=0) {corrSum+=matrix(x+1,y+1); corrCounter++;}
                    }
                }
                if (y>ylow){
                    if (matrix(x,y-1)!=0) {corrSum+=matrix(x,y-1); corrCounter++;}
                }
                if (y<ydim-1){
                    if (matrix(x,y+1)!=0) {corrSum+=matrix(x,y+1); corrCounter++;}
                }

                matrix(x,y) = TMath::Nint(0.001+corrSum/corrCounter);
            }
        }
    }
}

// scales down an n x n by 1/factor
TMatrixF reduceMatrix(TMatrixF &matrix, Float_t factor)
{
    Int_t ylow = matrix.GetColLwb();
    Int_t ydim = matrix.GetColUpb()+1;
    Int_t xlow = matrix.GetRowLwb();
    Int_t xdim = matrix.GetRowUpb()+1;

    Float_t temp;
    Float_t max=0;

    TMatrixF redMatrix(xdim,ydim); redMatrix=0;

    for(Int_t  y = ylow; y < ydim; y++)
    {
        for(Int_t  x = xlow; x < xdim; x++)
        {
            if (matrix(x,y)>max) max = matrix(x,y);
        }
    }

    if (max!=0)
    {
        for(Int_t  y = ylow; y < ydim; y++)
        {
            for(Int_t  x = xlow; x < xdim; x++)
            {
                temp = matrix(x,y)-factor*max;
                if (temp>0) redMatrix(x,y) = temp;
            }
        }
    }

    return redMatrix;
}

// calculates the gradient matrix of a TMatrix
TMatrixF getGradientMatrix(TMatrixF &matrix)
{
    Int_t ylow = matrix.GetColLwb();
    Int_t ydim = matrix.GetColUpb()+1;
    Int_t xlow = matrix.GetRowLwb();
    Int_t xdim = matrix.GetRowUpb()+1;

    TMatrixF gradMatrix(xdim,ydim); gradMatrix=0;

    for(Int_t  y = ylow+1; y < ydim-1; y++)
    {
        for(Int_t  x = xlow+1; x < xdim-1; x++)
        {
            gradMatrix(x,y) = sqrt((TMath::Power(matrix(x-1,y)-matrix(x+1,y),2)+TMath::Power(matrix(x,y-1)-matrix(x,y+1),2))/9.);
        }
    }

    return gradMatrix;
}

// calculates the gradient matrix of a TMatrix and stores it to *newMatrix
void getGradientMatrixFromTH2(TH2F* matrix, TH2F* newMatrix)
{
    Int_t ylow = 1;
    Int_t ydim = matrix->GetNbinsY();
    Int_t xlow = 1;
    Int_t xdim = matrix->GetNbinsX();

    for(int x=0;x<=xdim;x++)
    {
        for(int y=0;y<=ydim;y++)
        {
            if ((x<4)||(y<4)||(x>xdim-4)||(y>ydim-4))newMatrix->SetBinContent(x,y,0);
        }
    }

    float gradientX = 0;
    float gradientY = 0;
    float gradientD1 = 0;
    float gradientD2 = 0;

    //TF1* gradientLine = new TF1("gradientLine","pol1",0,10);
    TF1* gradientLine = new TF1("gradientLine","[0]+[1]*x",-1,5);

    int px, p0,p1,p2,p3,p4,p5;
    TGraphErrors* gradientGraph = new TGraphErrors(6);

    for(Int_t  y = ylow+3; y < ydim-3; y++) {

        for(Int_t  x = xlow+3; x < xdim-3; x++) {

            px = matrix->GetBinContent(x,y);

            if(px==0)
            {
                gradientX = 0;
                gradientY = 0;
            }
            else
            {
                p0 = matrix->GetBinContent(x-3,y);
                p1 = matrix->GetBinContent(x-2,y);
                p2 = matrix->GetBinContent(x-1,y);
                p3 = matrix->GetBinContent(x+1,y);
                p4 = matrix->GetBinContent(x+2,y);
                p5 = matrix->GetBinContent(x+3,y);

                if( (p0+p1+p2+p3) > 0)
                {
                    gradientGraph->SetPoint(0,0,p0);  gradientGraph->SetPointError(0,0,TMath::Sqrt(p0));
                    gradientGraph->SetPoint(1,1,p1);  gradientGraph->SetPointError(1,0,TMath::Sqrt(p1));
                    gradientGraph->SetPoint(2,2,p2);  gradientGraph->SetPointError(2,0,TMath::Sqrt(p2));
                    gradientGraph->SetPoint(3,3,p3);  gradientGraph->SetPointError(3,0,TMath::Sqrt(p3));
                    gradientGraph->SetPoint(4,4,p4);  gradientGraph->SetPointError(4,0,TMath::Sqrt(p4));
                    gradientGraph->SetPoint(5,5,p5);  gradientGraph->SetPointError(5,0,TMath::Sqrt(p5));

                    gradientLine->SetParameters(1,0);
                    gradientGraph->Fit("gradientLine","RQ");
                    gradientX = gradientLine->GetParameter(1);
                }
                else gradientX = 0;

                p0 = matrix->GetBinContent(x,y-3);
                p1 = matrix->GetBinContent(x,y-2);
                p2 = matrix->GetBinContent(x,y-1);
                p3 = matrix->GetBinContent(x,y+1);
                p4 = matrix->GetBinContent(x,y+2);
                p5 = matrix->GetBinContent(x,y+3);

                if( (p0+p1+p2+p3) > 0)
                {
                    gradientGraph->SetPoint(0,0,p0);  gradientGraph->SetPointError(0,0,TMath::Sqrt(p0));
                    gradientGraph->SetPoint(1,1,p1);  gradientGraph->SetPointError(1,0,TMath::Sqrt(p1));
                    gradientGraph->SetPoint(2,2,p2);  gradientGraph->SetPointError(2,0,TMath::Sqrt(p2));
                    gradientGraph->SetPoint(3,3,p3);  gradientGraph->SetPointError(3,0,TMath::Sqrt(p3));
                    gradientGraph->SetPoint(4,4,p4);  gradientGraph->SetPointError(4,0,TMath::Sqrt(p4));
                    gradientGraph->SetPoint(5,5,p5);  gradientGraph->SetPointError(5,0,TMath::Sqrt(p5));

                    gradientLine->SetParameters(1,0);
                    gradientGraph->Fit("gradientLine","RQ");
                    gradientY = gradientLine->GetParameter(1);
                }
                else gradientY = 0;

                p0 = matrix->GetBinContent(x-2,y-2);
                p1 = matrix->GetBinContent(x-1,y-1);
                p2 = matrix->GetBinContent(x+1,y+1);
                p3 = matrix->GetBinContent(x+2,y+2);

                if( (p0+p1+p2+p3) < 0)
                {
                    gradientGraph->SetPoint(0,0,p0);  gradientGraph->SetPointError(0,0,TMath::Sqrt(p0));
                    gradientGraph->SetPoint(1,1,p1);  gradientGraph->SetPointError(1,0,TMath::Sqrt(p1));
                    gradientGraph->SetPoint(2,2,p2);  gradientGraph->SetPointError(2,0,TMath::Sqrt(p2));
                    gradientGraph->SetPoint(3,3,p3);  gradientGraph->SetPointError(3,0,TMath::Sqrt(p3));

                    gradientLine->SetParameters(1,0);
                    gradientGraph->Fit("gradientLine","RQ");
                    gradientD1 = gradientLine->GetParameter(1);
                }
                else gradientD1 = 0;

                p0 = matrix->GetBinContent(x+2,y-2);
                p1 = matrix->GetBinContent(x+1,y-1);
                p2 = matrix->GetBinContent(x-1,y+1);
                p3 = matrix->GetBinContent(x-2,y+2);

                if( (p0+p1+p2+p3) < 0)
                {
                    gradientGraph->SetPoint(0,0,p0);  gradientGraph->SetPointError(0,0,TMath::Sqrt(p0));
                    gradientGraph->SetPoint(1,1,p1);  gradientGraph->SetPointError(1,0,TMath::Sqrt(p1));
                    gradientGraph->SetPoint(2,2,p2);  gradientGraph->SetPointError(2,0,TMath::Sqrt(p2));
                    gradientGraph->SetPoint(3,3,p3);  gradientGraph->SetPointError(3,0,TMath::Sqrt(p3));

                    gradientLine->SetParameters(1,0);
                    gradientGraph->Fit("gradientLine","RQ");
                    gradientD2 = gradientLine->GetParameter(1);
                }
                else gradientD2 = 0;
            }
            //newMatrix->SetBinContent(x,y, sqrt((TMath::Power(matrix->GetBinContent(x-1,y)-matrix->GetBinContent(x+1,y),2)+TMath::Power(matrix->GetBinContent(x,y-1)-matrix->GetBinContent(x,y+1),2))/9.) );

            if (gradientX>50) gradientX = 0;
            if (gradientY>50) gradientY = 0;
            if (gradientD1>50) gradientD1 = 0;
            if (gradientD2>50) gradientD2 = 0;
            if (gradientX<-50) gradientX = 0;
            if (gradientY<-50) gradientY = 0;
            if (gradientD1<-50) gradientD1 = 0;
            if (gradientD2<-50) gradientD2 = 0;

            newMatrix->SetBinContent(x,y, sqrt(TMath::Power(gradientX,2)+TMath::Power(gradientY,2)+TMath::Power(gradientD1,2)+TMath::Power(gradientD2,2)) );
        }
    }
    delete gradientLine;
    delete gradientGraph;
}


// converts an ASCII matrix to a TH2 histogram
TH2F* getTH2fromMatrix(TMatrixF &matrix, TString thName, TString thTitle)
{
    Int_t ylow = matrix.GetColLwb();
    Int_t ydim = matrix.GetColUpb()+1;
    Int_t xlow = matrix.GetRowLwb();
    Int_t xdim = matrix.GetRowUpb()+1;

    TH2F* matrixTH = new TH2F(thName, thTitle , xdim-xlow, xlow-0.5 , xdim-1+0.5, ydim-ylow, ylow-0.5 , ydim-1+0.5);

    for(Int_t  y = ylow; y < ydim; y++)
    {
        for(Int_t  x = xlow; x < xdim; x++)
        {
            matrixTH->Fill(x,y,matrix(x,y));
        }
    }

    return matrixTH;
}

// deprecated
TH1F* convoluteGaussian(TH1F* hist, Double_t gaussRelW)
{
    Float_t mDiv = TMath::Abs(hist->GetBinCenter(2) - hist->GetBinCenter(1));
    Int_t nMaxBins =  hist->GetNbinsX();

    Double_t binSum;

    TH1F* convolvedHist = new TH1F(hist->GetName(), hist->GetTitle(), nMaxBins , hist->GetBinCenter(0), hist->GetBinCenter(hist->GetNbinsX()-1));

    for(Int_t  n = 1; n < nMaxBins; n++)
    {
        //if (hist->GetBinContent(n)==0) continue;
        binSum = 0;

        for(Int_t  m = -100; m < 101; m++)
        {
            if ((n-m > 0)&&(n-m < nMaxBins))
            {
                binSum += TMath::Gaus(m*mDiv, 0, gaussRelW/2.35482*hist->GetBinCenter(n), true) * hist->GetBinContent(n-m);
            }
        }
        convolvedHist->SetBinContent(n, binSum);
    }
    return convolvedHist;
}

// removes all histograms from the program from the memory.
void histoClearUp(std::vector< TObject* >* vectorTH)
{
    using namespace std;
    TString name;
    vector<TString> names;

    for(int k=0; k<vectorTH->size();k++)
    {
        if (vectorTH->at(k)->GetName()) {names.push_back(vectorTH->at(k)->GetName()); }
    }

    int length = names.size();

    for(int k=0; k < length;k++)
    {        
        if (gDirectory->FindObject(names.at(k))) {delete gDirectory->FindObject(names.at(k));}
    }
    vectorTH->clear();
    //cout<<"deleting histos..."<<endl;
}


// empties all histograms.
void histoErase(std::vector< TObject* >* vectorTH)
{
    using namespace std;
    TString name;
    vector<TString> names;

    for(int k=0; k<vectorTH->size();k++)
    {
        if (vectorTH->at(k)->GetName())
        {
            if (vectorTH->at(k)->InheritsFrom("TH1"))
            {
                TH1* thtemp = static_cast<TH1*> (vectorTH->at(k));

                for(int n = 0; n < thtemp->GetNbinsX(); n++)
                {
                    thtemp->SetBinContent(n,0);
                }
                thtemp->Reset("M");
            }
            if (vectorTH->at(k)->InheritsFrom("TH2"))
            {
                TH2* th2temp = static_cast<TH2*> (vectorTH->at(k));
                for (int l = 0; l < th2temp->GetNbinsX(); l++)
                {
                    th2temp->SetBinContent(l, 0);
                    for (int m = 0; m < th2temp->GetNbinsY(); m++)
                    {
                        th2temp->SetBinContent(l, m, 0);
                    }
                }
                th2temp->Reset("M");
            }
        }
    }
}

/**
 * Clear up the liveView data histograms.
 *
 * @param vectorTH
 */
void histoLiveClearUp(std::vector< TH1* >* vectorTH)
{
    using namespace std;
    for (int k = 0; k < vectorTH->size(); k++)
    {
        for (int l = 0; l < vectorTH->at(k)->GetNbinsX(); l++)
        {
            vectorTH->at(k)->SetBinContent(l, 0);
            for (int m = 0; m < vectorTH->at(k)->GetNbinsY(); m++)
            {
                vectorTH->at(k)->SetBinContent(l, m, 0);
            }
            //vectorTH->at(k)->ResetStats();
        }
        vectorTH->at(k)->Reset("M");
    }
}


/**
 *  calculates the effective probability for an interaction using
 *  material density 'density' in g/cm3, atomic weight 'atWeight' in u
 *  single interaction cross section 'cs' in barns, and
 *  the wavelength lambda in Angstroms, which is only relevant for absorption, otherwise set to 0
 *
 *
 * @param density
 * @param atWeight
 * @param cs
 * @param lambda
 * @return wwProb
 */
double getWWProb(double density, double atWeight, double cs, double lambda)
{
    double wwProb = 0;

    if (lambda == 0)	wwProb = 0.06022045 * density / atWeight * cs;
    else				wwProb = 0.06022045 * density / atWeight * cs * lambda / 1.8;

    return wwProb;
}

/**
 * calculates the wavelength lambda in Angstroms for a given energy in MeV
 *
 * @param energy
 * @return the wavelength lambda.
 */
double getLfromE(double energy)
{
    return sqrt(81.81 / energy / 1e9);
}


/**
 * calculates the energy in MeV for a given wavelength lambda in Angstroms
 *
 * @param lambda -the wavelength.
 * @return the energy .
 */
double getEfromL(double lambda)
{
    return 81.81 / TMath::Power(lambda, 2) / 1e9;
}


/**
 * Create a Spline Function for an energy (log) response function.
 *
 * @return spline energy.
 */
TSpline3* getSplinedDetectorEnergyModel()
{
    const int nData = 40;

    float xVal[nData] = { 1.02E-08, 1.82E-08, 3.24E-08, 5.75E-08, 1.02E-07,   1.82E-07,   3.24E-07,   5.75E-07,   1.02E-06,   1.82E-06,   3.24E-06,   5.75E-06,   1.02E-05,   1.82E-05,   3.24E-05,   5.75E-05,   0.000102329,   0.00018197,   0.000323594,   0.00057544,   0.001023293,   0.001819701,   0.003235937,   0.0057544,   0.01023293,   0.018197009,   0.032359365,   0.057543993,   0.102329299,   0.18197009,   0.323593646,   0.57543993,   1.023293018,   1.819700837,   3.235936642,   5.7543993,   10.23293018,   18.19700813,  32.35936737,   57.5439949 };
    float yVal[nData] = { 6.9625 ,7.351 ,7.744 ,8.3515 ,10.429 ,13.8135 ,16.605 ,19.336 ,20.826 ,21.8715 ,22.334 ,22.633 ,22.4995 ,22.208 ,21.594 ,20.939 ,20.1395 ,19.3815 ,18.54 ,17.579 ,16.933 ,16.04 ,15.0365 ,14.2945 ,13.6655 ,12.743 ,11.9555 ,11.3265 ,10.237 ,9.27 ,8.075 ,6.5935 ,5.193 ,3.765 ,2.5505 ,1.6125 ,1.031 ,0.422 ,0.4885 ,0.3255 };

    for (int i = 0; i < nData; i++)
    {
        xVal[i] = TMath::Log10(xVal[i]); //probably easier to spline?
        yVal[i] = yVal[i] * 4.44 / 100.; //scale to 1 maximum value
    }

    TGraph* graphEnergy = new TGraph(nData, xVal, yVal);

    TSpline3* splineEnergy = new TSpline3("splineEnergyModel", graphEnergy);

    delete graphEnergy;

    return splineEnergy;
}

/**
 * Get the number of lines in a File.
 *
 * @param dname - file name
 * @return lengthFile - length of the file..
 */
int getNumberOfFileEntries(TString dname)
{
    using namespace std;
    int lengthFile = 0;
    TString curLine;

    ifstream input_streamCheck(dname, ios::in);

    while (curLine.ReadLine(input_streamCheck)) { lengthFile++; }

    input_streamCheck.close();

    return lengthFile;
}

// Legendre Polynomials
// inline for computational speed
inline double P0(double x) { return 1.0; }

// n = 1
inline double P1(double x) { return x; }
// n = 2
inline double P2(double x) { return ((3. * x * x) - 1.) * 0.5; }

inline double P7(double x) { return ((429. * x * x * x * x * x * x * x - 693. * x * x * x * x * x + 315. * x * x * x - 35. * x) * 0.0625); }

inline double P8(double x) { return ((6435. * x * x * x * x * x * x * x * x - 12012. * x * x * x * x * x * x + 6930. * x * x * x * x - 1260. * x * x + 35.) * 0.0078125); }

inline double P9(double x) { return ((12155. * x * x * x * x * x * x * x * x * x - 25740. * x * x * x * x * x * x * x + 18018. * x * x * x * x * x - 4620. * x * x * x + 315. * x) * 0.0078125); }

inline double P10(double x) { return ((46189. * x * x * x * x * x * x * x * x * x * x - 109395. * x * x * x * x * x * x * x * x + 90090. * x * x * x * x * x * x - 30030. * x * x * x * x + 3465. * x * x - 63.) * 0.00390625); }

inline double P11(double x) { return ((88179. * x * x * x * x * x * x * x * x * x * x * x - 230945. * x * x * x * x * x * x * x * x * x + 218790. * x * x * x * x * x * x * x - 90090. * x * x * x * x * x + 15015. * x * x * x - 693. * x) * 0.00390625); }

inline double P12(double x) { return ((676039. * x * x * x * x * x * x * x * x * x * x * x * x - 1939938. * x * x * x * x * x * x * x * x * x * x + 2078505. * x * x * x * x * x * x * x * x - 1021020. * x * x * x * x * x * x + 225225. * x * x * x * x - 18018. * x * x + 231.) * 0.0009765625); }

inline double P13(double x) { return ((1300075. * x * x * x * x * x * x * x * x * x * x * x * x * x - 4056234. * x * x * x * x * x * x * x * x * x * x * x + 4849845. * x * x * x * x * x * x * x * x * x - 2771340. * x * x * x * x * x * x * x + 765765. * x * x * x * x * x - 90090. * x * x * x + 3003. * x) * 0.0009765625); }

inline double P14(double x) { return ((5014575. * x * x * x * x * x * x * x * x * x * x * x * x * x * x - 16900975. * x * x * x * x * x * x * x * x * x * x * x * x + 22309287. * x * x * x * x * x * x * x * x * x * x - 14549535. * x * x * x * x * x * x * x * x + 4849845. * x * x * x * x * x * x - 765765. * x * x * x * x + 45045. * x * x - 429.) * 0.00048828125); }

inline double P15(double x) { return ((9694845. * x * x * x * x * x * x * x * x * x * x * x * x * x * x * x - 35102025. * x * x * x * x * x * x * x * x * x * x * x * x * x + 50702925. * x * x * x * x * x * x * x * x * x * x * x - 37182145. * x * x * x * x * x * x * x * x * x + 14549535. * x * x * x * x * x * x * x - 2909907. * x * x * x * x * x + 255255. * x * x * x - 6435. * x) * 0.00048828125); }

inline double P16(double x) { return ((300540195. * x * x * x * x * x * x * x * x * x * x * x * x * x * x * x * x - 1163381400. * x * x * x * x * x * x * x * x * x * x * x * x * x * x + 1825305300. * x * x * x * x * x * x * x * x * x * x * x * x - 1487285800. * x * x * x * x * x * x * x * x * x * x + 669278610. * x * x * x * x * x * x * x * x - 162954792. * x * x * x * x * x * x + 19399380. * x * x * x * x - 875160. * x * x + 6435.) * 0.000030517578125); }

inline double P17(double x)  { return ((583401555.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-2404321560.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x +4071834900.*x*x*x*x*x*x*x*x*x*x*x*x*x-3650610600.*x*x*x*x*x*x*x*x*x*x*x+1859107250.*x*x*x*x*x*x*x*x*x-535422888.*x*x*x*x*x*x*x + 81477396.*x*x*x*x*x-5542680.*x*x*x+109395.*x) *0.000030517578125) ; }

inline double P18(double x)  { return ((2268783825.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-9917826435.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x +18032411700.*x*x*x*x*x*x*x*x*x*x*x*x*x*x-17644617900.*x*x*x*x*x*x*x*x*x*x*x*x+10039179150.*x*x*x*x*x*x*x*x*x*x-3346393050.*x*x*x*x*x*x*x*x + 624660036.*x*x*x*x*x*x-58198140.*x*x*x*x+2078505.*x*x-12155.) *0.000015258789) ; }

inline double P19(double x)  { return ((4418157975.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-20419054425.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x +39671305740.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-42075627300.*x*x*x*x*x*x*x*x*x*x*x*x*x+26466926850.*x*x*x*x*x*x*x*x*x*x*x-10039179150.*x*x*x*x*x*x*x*x*x + 2230928700.*x*x*x*x*x*x*x-267711444.*x*x*x*x*x+14549535.*x*x*x-230945.*x) *0.000015258789) ; }

inline double P20(double x)  { return ((34461632205.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-167890003050.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x +347123925225.*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x*x-396713057400.*x*x*x*x*x*x*x*x*x*x*x*x*x*x+273491577450.*x*x*x*x*x*x*x*x*x*x*x*x-116454478140.*x*x*x*x*x*x*x*x*x*x + 30117537450.*x*x*x*x*x*x*x*x-4461857400.*x*x*x*x*x*x+334639305.*x*x*x*x-9699690.*x*x+46189.) *0.00000381469727) ; }


/**
 * Defining an legrende polynomial function
 * @param n - degree
 * @param x - x value
 * @return evaluated result of the polynomial
 */
inline double legendre_Pl(int n, double x)
{
    switch (n)
    {
    case 0: return P0(x);
        break;
    case 1: return P1(x);
        break;
    case 2: return P2(x);
        break;
    case 3:  return 0.5 * (5. * x * x * x - 3. * x);
        break;
    case 4: return 0.125 * (35. * x * x * x * x - 30. * x * x + 3.);
        break;
    case 5: return 0.125 * (63. * x * x * x * x * x - 70. * x * x * x + 15. * x);
        break;
    case 6: return 0.0625 * (231. * x * x * x * x * x * x - 315. * x * x * x * x + 105. * x * x + 5.);
        break;
    case 7: return P7(x);
        break;
    case 8: return P8(x);
        break;
    case 9: return P9(x);
        break;
    case 10: return P10(x);
        break;
    case 11: return P11(x);
        break;
    case 12: return P12(x);
        break;
    case 13: return P13(x);
        break;
    case 14: return P14(x);
        break;
    case 15: return P15(x);
        break;
    case 16: return P16(x);
        break;
    case 17: return P17(x);
        break;
    case 18: return P18(x);
        break;
    case 19: return P19(x);
        break;
    case 20: return P20(x);
        break;
    }

    if (x == 1.0)
    {
        return 1.0;
    }

    if (x == -1.0)
    {
        return ((n % 2 == 0) ? 1.0 : -1.0);
    }

    if ((x == 0.0) && (n % 2))
    {
        return 0.0;
    }

    double pnm1(P20(x)) ;
    double pnm2(P19(x)) ;
    double pn(pnm1);

    for (int l = 21 ; l <= n ; l++)
    {
        pn = (((2.0 * (double)l) - 1.0) * x * pnm1 -
            (((double)l - 1.0) * pnm2)) / (double)l;
        pnm2 = pnm1;
        pnm1 = pn;
    }

    return pn;
}



/**
 * replaces the x-axis with a logarithmic x-axis of 100 bins
 * root has no logarithmic binning, so it requires generating a bin array
 * @param h
 *
 */
void logXaxis(TH2* h)
{
    TAxis* axis = h->GetXaxis();
    const Int_t nbins = 100;
    Double_t xmin = axis->GetXmin();
    Double_t xmax = axis->GetXmax();
    Double_t logXmin = TMath::Log10(xmin);
    Double_t logXmax= TMath::Log10(xmax);
    Double_t binwidth = (logXmax - logXmin) / nbins;
    Double_t xBins[nbins + 1];
    xBins[0] = xmin;
    for (Int_t i = 1; i <= nbins; i++)
    {
        xBins[i] = xmin + TMath::Power(10, logXmin + i * binwidth);
    }

    axis->Set(nbins, xBins);
}


/**
 * replaces the x-axis with a logarithmic x-axis of 1000 bins
 * root has no logarithmic binning, so it requires generating a bin array
 * @param h
 *
 */
void logaxis(TH1* h)
{
    TAxis* axis = h->GetXaxis();
    const Int_t nBins = 1000;
    Double_t xMin = axis->GetXmin();
    Double_t xMax = axis->GetXmax();
    Double_t logXmin = TMath::Log10(xMin);
    Double_t logXmax= TMath::Log10(xMax);
    Double_t binWidth = (logXmax- logXmin) / nBins;
    Double_t xBins[nBins + 1];
    xBins[0] = xMin;
    for (Int_t binsItr = 1; binsItr <= nBins; binsItr++)
    {
        xBins[binsItr] = xMin + TMath::Power(10, logXmin + binsItr * binWidth);
    }

    axis->Set(nBins, xBins);
}


/**
 * rebins a logarithmic x-axis from 1000 bins to 100
 * @param h
 *
 */
void rebinX(TH1* h)
{
    const Int_t nBinsPrev = 1000;
    const Int_t nBins = 100;
    TAxis* axis = h->GetXaxis();
    Double_t xMin = axis->GetXmin();
    Double_t xMax = axis->GetXmax();

    Double_t logXmin = TMath::Log10(xMin);
    Double_t logXmax= TMath::Log10(xMax);
    Double_t binwidth = (logXmax- logXmin) / nBins;
    Double_t xBins[nBins + 1];
    xBins[0] = xMin;

    for (Int_t i = 1; i <= nBins; i++)
    {
        xBins[i] = xMin + TMath::Power(10, logXmin + i * binwidth);
    }
    Double_t binContent[nBins + 1];
    for (Int_t i = 1; i <= nBins; i++) { binContent[i] = 0; }

    int binNo;

    for (Int_t i = 1; i <= nBinsPrev + 1; i++)
    {
        binNo = i / 10;
        binContent[binNo] = binContent[binNo] + h->GetBinContent(i);
    }

    for (Int_t i = 1; i <= nBinsPrev + 1; i++)
    {
        h->SetBinContent(i, 0);
    }

    //h->Clear();

    axis->Set(nBins, xBins);

    for (Int_t i = 1; i <= nBins; i++)
    {
        h->Fill(h->GetBinCenter(i), binContent[i]);
    }
}


/**
 * replaces the y-axis with a logarithmic y-axis of 100 bins
 * root has no logarithmic binning, so it requires generating a bin array
 * @param h
 *
 */
void logYaxis(TH2* h)
{
    TAxis* axis = h->GetYaxis();
    const Int_t nbins = 100;
    Double_t xmin = axis->GetXmin();
    Double_t xmax = axis->GetXmax();
    Double_t logXmin = TMath::Log10(xmin);
    Double_t logXmax= TMath::Log10(xmax);
    Double_t binwidth = (logXmax- logXmin) / nbins;
    Double_t xBins[nbins + 1];
    xBins[0] = xmin;
    for (Int_t i = 1; i <= nbins; i++)
    {
        xBins[i] = xmin + TMath::Power(10, logXmin + i * binwidth);
    }

    axis->Set(nbins, xBins);
}


/**
 * calculates neutron transport time in mus
 * z0 and z0Alt in mm, energy in MeV
 *
 * @param z0.
 * @param z0Alt.
 * @param energy
 * @param cosTheta
 * @return the Neutron transport Time .
 */
double calcNeutronDiffTime(double z0, double z0Alt, double energy, double cosTheta)
{
    //if (energy<20)	nSpeed = 3.9560339/sqrt(81.81/energy/1e9) * 1000.*1000.;
    //else 	nSpeed = 3.9560339/sqrt(81.81/20./1e9) * 1000.*1000.;
    //if (energy < nSpeedEnergyCutoff) nSpeed = 3.9560339/sqrt(81.81/nSpeedEnergyCutoff/1e9) * 1000.*1000.;

    if (z0Alt == z0) return 0;

    double nSpeed = 3.9560339 / sqrt(81.81 / energy / 1e9);

    //timeTemp = wwRange/nSpeed;
    return fabs((z0Alt - z0) / cosTheta / nSpeed);
}


/**
 * generates a random number in MeV from an evaporation spectrum with central energy theta
 * pointer to an already seeded Random generator is needed
 * @param theta
 * @param r
 * @return abszRnd  .
 *
 */
double getEvaporationEnergy(double theta, TRandom* r)
{
    bool gotIt;
    double abszRnd, ordRnd;

    TF1* spectrumFuncEvaporation = new TF1("spectrumFuncEvaporation", "x*TMath::Exp(-x/[0])", 0.0000, 20);
    spectrumFuncEvaporation->SetParameter(0, theta / 1e6);
    double maximum = spectrumFuncEvaporation->GetMaximum(0.2, 10);

    gotIt = false;
    while (!gotIt)
    {
        abszRnd = r->Rndm() * 19.;
        ordRnd = r->Rndm() * maximum;
        //gotIt = true;
        if (spectrumFuncEvaporation->Eval(abszRnd) > ordRnd) gotIt = true;
    }

    delete spectrumFuncEvaporation;

    return abszRnd;
}


/**
 * generates a random number in MeV from a moderated Californium source spectrum
 * pointer to an already seeded Random generator is needed
 * @param r
 * @return abszRnd
 */
double getModeratedCfEnergy(TRandom* r)
{
    bool gotIt;
    double abszRnd, ordRnd;

    //this function is in eV, not MeV like the others
    TF1* spectrumMaxwellPhiTempModModFission = new TF1("spectrumMaxwellPhiTempModModFission", "[0]/(TMath::Power([1]/11604.5,2))*TMath::Power(x,2)*TMath::Exp(-x/[1]*11604.5)+[2]*TMath::Power([1]/11604.5,1)/(1+TMath::Power(([3]/x),7)) * 1./(1-[4]/(1+TMath::Power(x/[5],5))) + [6]*x*TMath::SinH(sqrt(2.*(x-[7])/1e6))*TMath::Exp(-(x-[7])/1e6)", 1e-3, 1e7);
    spectrumMaxwellPhiTempModModFission->SetParameters(6868, 243, 9600, 0.7171, 0.6023, 0.2103, 0.003933, -3.394e6);

    //spectrumMaxwellPhiTempModModFission->SetParNames("Intensity th","Temperature", "Intensity ratio","scaling 1","scaling 2 [eV]","scaling 3 [eV]", "FissionPar1", "FissionPar2");
    double maximum = 3800;

    gotIt = false;
    while (!gotIt)
    {
        abszRnd = TMath::Power(10, r->Rndm() * 9. - 2.4);
        ordRnd = r->Rndm() * maximum;
        //gotIt = true;
        if (spectrumMaxwellPhiTempModModFission->Eval(abszRnd) > ordRnd) gotIt = true;
    }

    delete spectrumMaxwellPhiTempModModFission;

    return (1e-6 * abszRnd);
}


/**
 *  generates a random number in MeV from a fission spectrum
 * pointer to an already seeded Random generator is needed
 * @param r
 * @return abszRnd
 */
double getFissionEnergy(TRandom* r)
{
    bool gotIt;
    double abszRnd, ordRnd;

    TF1* spectrumFuncFission = new TF1("spectrumFuncFission", "0.4865*TMath::SinH(sqrt(2*x))*TMath::Exp(-x)", 0.0000, 10);

    double maximum = spectrumFuncFission->GetMaximum(0.2, 3);

    gotIt = false;
    while (!gotIt)
    {
        abszRnd = r->Rndm() * 10.;
        ordRnd = r->Rndm() * maximum;
        //gotIt = true;
        if (spectrumFuncFission->Eval(abszRnd) > ordRnd) gotIt = true;
    }
    delete spectrumFuncFission;

    return abszRnd;
}

/**
 *  generates a random number in MeV from a fission spectrum
 *  pointer to an already seeded Random generator is needed
 * @param r
 * @return abszRnd
 */
double getFissionEnergy2(TRandom* r)
{
    bool gotIt;
    double abszRnd, ordRnd;

    TF1* spectrumFuncFission = new TF1("spectrumFuncFission", "0.4865*TMath::SinH(sqrt(1.03419*x))*TMath::Exp(-x/1.18)", 0.0000, 10); // X-5 MONTE CARLO TEAM,LA-UR-03-1987,Los Alamos National  Laboratory

    double maximum = spectrumFuncFission->GetMaximum(0.2, 3);

    gotIt = false;
    while (!gotIt)
    {
        abszRnd = r->Rndm() * 10.;
        ordRnd = r->Rndm() * maximum;
        //gotIt = true;
        if (spectrumFuncFission->Eval(abszRnd) > ordRnd) gotIt = true;
    }

    delete spectrumFuncFission;

    return abszRnd;
}


/**
 *  generates a random Number representing energy in eV according to a thermal Spectrum
 *  collision partner of given mass massElm and temperature
 *  pointer to an already seeded Random generator is needed
 * @param r
 * @return vNeutron
 */
std::vector<float> getThermalPDF(const double nEnergy, const float massElm, const float temperature, TRandom* r)
{
    using namespace std;
    float rnd1 = 0, rnd2 = 0, rnd3 = 0, abszRnd, ordRnd;
    vector<float> vNeutron;
    bool gotIt = false;
    bool takeTwo = false;

    double kB = 1.38065e-23;
    //double mN = 1.6749e-27;
    double mN = 939.565 * 1.1111111e-11;
    double sqrtPi = sqrt(TMath::Pi());
    double beta = sqrt((1.66e-27) * massElm * 0.5 / kB / temperature);

    double velR = 0.;
    double velT = 0.;
    double velN = sqrt(2. * nEnergy * 1e6 / mN);

    double y = velN * beta;

    TF1* spectrumFuncTwo = new TF1("spectrumFuncTwo", "2.256758353*TMath::Power(x,2)*TMath::Exp(-x*x)", 0.0000000, 100000);
    TF1* spectrumFuncThree = new TF1("spectrumFuncThree", "2.*TMath::Power(x,3)*TMath::Exp(-x*x)", 0.0000000, 100000);
    //Plot Range x: [0,3.2] y:[0,0.85]

    TF1* spectrumFunc;

    //calc which distribution to take: low energy or high energy
    rnd1 = r->Rndm();
    if (rnd1 < 2. / (sqrtPi * y + 2.))
    {
        takeTwo = false;
        spectrumFunc = spectrumFuncThree;
    }
    else
    {
        takeTwo = true;
        spectrumFunc = spectrumFuncTwo;
    }

    //get a thermal velocity from the Spectrum
    gotIt = false;
    while (!gotIt)
    {
        abszRnd = r->Rndm() * 3.2;
        ordRnd = r->Rndm() * 0.85;        
        if (spectrumFunc->Eval(abszRnd) > ordRnd) gotIt = true;
    }

    velT = abszRnd / beta;

    gotIt = false;
    while (!gotIt)
    {
        rnd2 = r->Rndm() * 2. - 1.;
        rnd3 = r->Rndm();

        velR = sqrt(fabs(TMath::Power(velN, 2) + TMath::Power(velT, 2) - 2. * velN * velT * rnd2));

        if (rnd3 < velR / (velN + velT)) gotIt = true;
    }

    // Energy(speed) and cosine
    vNeutron.push_back(velR * velR * mN / 2.);
    vNeutron.push_back(rnd2);

    delete spectrumFuncTwo; delete spectrumFuncThree;

    return vNeutron;
}

/**
 *  generates a random energy from a logarithmic thermal neutron density function
 *  in eV
 * @param spectrumFunc
 * @param r
 * @return xRnd
 */
double getThermalEnergyLog(const TF1* spectrumFunc, TRandom* r)
{
    double xRnd, yRnd;
    bool gotIt = false;

    while (!gotIt)
    {

        xRnd = TMath::Power(10, r->Rndm() * 2.7 - 8.9);
        yRnd = r->Rndm() * 12.;

        if (spectrumFunc->Eval(xRnd) > yRnd)
        {
            gotIt = true;
        }
    }
    return xRnd;
}


/**
 * generates a random energy from a linear thermal neutron density function
 * @param spectrumFunc
 * @param r
 *
 */
double getThermalEnergy(TF1* spectrumFunc, TRandom* r)
{
    double xRnd, yRnd;
    bool gotIt = false;

    while (!gotIt)
    {
        xRnd = r->Rndm() * 0.19e-6 + 0.1e-9;
        yRnd = r->Rndm();

        if (spectrumFunc->Eval(xRnd) > yRnd)
        {
            gotIt = true;
        }
    }

    return xRnd;
}


/**
 * generates a random energy from a logarithmic thermal neutron density function like the CF moderated spectrum
 * @param spectrumFunc
 * @param r
 *
 */
double getThermalEnergyFromSource(const TF1* spectrumFunc, TRandom* r)
{
    double xRnd, yRnd;
    bool gotIt = false;

    while (!gotIt)
    {
        xRnd = TMath::Power(10, r->Rndm() * 2.2 - 2.5);
        yRnd = r->Rndm() * 3800.;

        if (spectrumFunc->Eval(xRnd) > yRnd)
        {
            gotIt = true;
        }
    }
    return xRnd;
}

/**
 * calculates a scalar product
 * @param rx1
 * @param ry1
 * @param rz1
 * @param rx2
 * @param ry2
 * @param rz2
 * @returns - evaluated scalar product
 */
double scalarProduct(double rx1, double ry1, double rz1, double rx2, double ry2, double rz2)
{
    return(rx1 * rx2 + ry1 * ry2 + rz1 * rz2);
}

/**
 * finding the intersection of a line and a cylinder mantle
 * @param stVx
 * @param stVy
 * @param stVz
 * @param phi
 * @param theta
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @returns - intersection
 */
std::vector<double> intersectCylinderMantle(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz, double dRad)
{
    using namespace std;
    double tValue1, tValue2, retValue;
    vector<double> tValues;
    tValues.push_back(0); tValues.push_back(0);

    double sinThetacosPhi = (sin(theta)) * cos(phi);
    double sinThetasinPhi = (sin(theta)) * sin(phi);
    //double cosTheta = (cos(theta));

    double c = sinThetacosPhi * (stVx - px) + sinThetasinPhi * (stVy - py);
    double c2 = pow(px - stVx, 2) + pow(py - stVy, 2); //wrong results if detector is off center

    tValue1 = -c + sqrt(c * c - (c2 - dRad * dRad));
    tValue2 = -c - sqrt(c * c - (c2 - dRad * dRad));

    double xIs1 = stVx + tValue1 * sinThetacosPhi;
    double yIs1 = stVy + tValue1 * sinThetasinPhi;

    if (scalarProduct(sinThetacosPhi, sinThetasinPhi, 0, (xIs1 - px), (yIs1 - py), 0) >= 0)
    {
        retValue = tValue2;
        tValues.at(0) = tValue2;
        tValues.at(1) = tValue1;
    }
    else
    {
        retValue = tValue1;
        tValues.at(0) = tValue1;
        tValues.at(1) = tValue2;
    }

    return tValues;
}

/**
 * finding the intersection of sphere with a line
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @returns - intersection
 */
double intersectSphere(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz, double dRad)
{
    double tValue1, tValue2, retValue;

    double sinThetacosPhi = (sin(theta)) * cos(phi);
    double sinThetasinPhi = (sin(theta)) * sin(phi);
    double cosTheta = cos(theta);

    double c = sinThetacosPhi * (stVx - px) + sinThetasinPhi * (stVy - py) + cosTheta * (stVz - pz);
    double c2 = pow(px - stVx, 2) + pow(py - stVy, 2) + pow(pz - stVz, 2);

    tValue1 = -c + sqrt(c * c - (c2 - dRad * dRad));
    tValue2 = -c - sqrt(c * c - (c2 - dRad * dRad));

    double xIs1 = stVx + tValue1 * sinThetacosPhi;
    double yIs1 = stVy + tValue1 * sinThetasinPhi;
    /*
    double zIs1 = stVz+tValue1*cos(theta);
    double xIs2 = stVx+tValue2*sin(theta)*cos(phi);
    double yIs2 = stVy+tValue2*sin(theta)*sin(phi);
    double zIs2 = stVz+tValue2*cos(theta);
    */

    bool secondPoint = false;
    if (scalarProduct(sinThetacosPhi, sinThetasinPhi, 0, (xIs1 - px), (yIs1 - py), 0) >= 0)
    {
        secondPoint = true; //take no2
        retValue = tValue2;
    }
    else
    {
        secondPoint = false;
        retValue = tValue1;
    }

    return retValue;
}

/**
 * Deprecated
 * Calculate the Cylindrical hit distance
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @param xref
 * @param yref
 * @returns - hit distance
 */
double calcCylindricalHitDist2(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz, double dRad, double xRef, double yRef)
{
    using namespace std;
    double retValue;
    vector<double> tValues;

    tValues = intersectCylinderMantle(stVx, stVy, stVz, theta, phi, px, py, pz, dRad);

    double xIs2 = stVx + tValues.at(0) * sin(theta) * cos(phi);
    double yIs2 = stVy + tValues.at(0) * sin(theta) * sin(phi);
    //double zIs2 = stVz+tValue*cos(theta);

    retValue = sqrt(pow(xRef - xIs2, 2) + pow(yRef - yIs2, 2));

    return retValue;
}


/**
 * Calculate the Cylindrical hit distance
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @param xref
 * @param yref
 * @returns - hit distance
 */
double calcCylindricalHitDist(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz, double dRad, double xRef, double yRef)
{
    double tValue, retValue;

    tValue = intersectSphere(stVx, stVy, stVz, theta, phi, px, py, pz, dRad);

    double xIs2 = stVx + tValue * sin(theta) * cos(phi);
    double yIs2 = stVy + tValue * sin(theta) * sin(phi);
    //double zIs2 = stVz+tValue*cos(theta);

    retValue = sqrt(pow(xRef - xIs2, 2) + pow(yRef - yIs2, 2));

    return retValue;
}

/**
 * Calculate the cylinder intersection
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @returns - cylinder intersection
 */
double intersectCylinder(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz, double dRad)
{
    //takes the projection of the cylinder in the x-y plane, selects between the two intersection points
    double tValue1, tValue2;

    double a = pow((sin(theta) * cos(phi)), 2) + pow((sin(theta) * sin(phi)), 2);
    double p = sin(theta) * cos(phi) * (stVx - px) + sin(theta) * sin(phi) * (stVy - py) / a;
    double q = pow((stVx - px), 2) + pow((stVy - py), 2) - dRad * dRad;

    tValue1 = -0.5 * p + sqrt(pow(0.5 * p, 2) - q);
    tValue2 = -0.5 * p - sqrt(pow(0.5 * p, 2) - q);

    double xIs1 = stVx + tValue1 * sin(theta) * cos(phi);
    double yIs1 = stVy + tValue1 * sin(theta) * sin(phi);
    double zIs1 = stVz + tValue1 * cos(theta);
    double xIs2 = stVx + tValue2 * sin(theta) * cos(phi);
    double yIs2 = stVy + tValue2 * sin(theta) * sin(phi);
    double zIs2 = stVz + tValue2 * cos(theta);

    bool secondPoint = false;
    if (scalarProduct(sin(theta) * cos(phi), sin(theta) * sin(phi), 0, (xIs1 - px), (yIs1 - py), 0) > 0) secondPoint = true; //take no2
    else secondPoint = false;

    return 0; //unfinished
}

/**
 * calculates 3D track ('stVx', 'stVy', 'stVz', 'phi', 'theta') to a line ('px','py','pz') distance
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @returns - distance to line
 */
double getDistanceToLine(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz)
{
    double distance;

    theta = TMath::Pi() - theta;

    double a1 = sin(theta) * sin(phi);
    double a2 = -sin(theta) * cos(phi);

    distance = abs(((px - stVx) * a1 + (py - stVy) * a2) / sqrt(a1 * a1 + a2 * a2));

    return distance;
}


/**
 * calculates 3D track ('stVx', 'stVy', 'stVz', 'phi', 'theta') to point  ('px','py','pz') distance
 * @param stVx
 * @param stVy
 * @param stVz
 * @param theta
 * @param phi
 * @param px
 * @param py
 * @param pz
 * @param dRad
 * @returns - distance to point
 */
double getDistanceToPoint(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz)
{
    double distance;

    theta = TMath::Pi() - theta; //changed 29.11.2018

    double gx = sin(theta) * cos(phi);
    double gy = sin(theta) * sin(phi);
    double gz = -cos(theta); //changed 30.11.2018

    double krx = gy * (stVz - pz) - gz * (stVy - py);
    double kry = gz * (stVx - px) - gx * (stVz - pz);
    double krz = gx * (stVy - py) - gy * (stVx - px);

    distance = sqrt(krx * krx + kry * kry + krz * krz) / sqrt(gx * gx + gy * gy + gz * gz);

    return distance;
}


//DEPRECATED
double getDistanceToPointOLD(double stVx, double stVy, double stVz, double theta, double phi, double px, double py, double pz)
{
    double distance;

    double tx = cos(phi) * (stVz - pz) - tan(theta) * (stVy - py);
    double ty = tan(theta) * (stVx - px) - sin(phi) * (stVz - pz);
    double tz = sin(phi) * (stVy - py) - cos(phi) * (stVx - px);

    distance = sqrt(1. / (1. + tan(theta) * tan(theta))) * sqrt(tx * tx + ty * ty + tz * tz);

    return distance;
}

/**
 * returns a e polynom of order par[1] at x, given scale factor par[0]
 * @param x
 * @param par
 * @returns - legrendian
 */
Double_t legendrian(double* x, Double_t* par)
{
    return par[0] * legendre_Pl(par[1], x[0]);
}

/**
 * returns a normalized sum of Legendre polynoms of order 0 to 10 at x
 * @param x
 * @param par
 * @returns - legrendian 10 fold
 */
Double_t legendrian10fold(double* x, Double_t* par)
{
    x[0] = TMath::Cos(x[0]);
    return 0.159155 * (0.5 * 1. * legendre_Pl(0, x[0]) + 1.5 * par[0] * legendre_Pl(1, x[0]) + 2.5 * par[1] * legendre_Pl(2, x[0]) + 3.5 * par[2] * legendre_Pl(3, x[0]) + 4.5 * par[3] * legendre_Pl(4, x[0]) + 5.5 * par[4] * legendre_Pl(5, x[0]) + 6.5 * par[5] * legendre_Pl(6, x[0]) + 7.5 * par[6] * legendre_Pl(7, x[0]) + 8.5 * par[7] * legendre_Pl(8, x[0]) + 9.5 * par[8] * legendre_Pl(9, x[0]) + 10.5 * par[9] * legendre_Pl(10, x[0]));
}

/**
 * returns a normalized sum of Legendre polynoms of order 0 to 10 at x as a STRING
 * @param par
 * @returns - legrendian 10 fold as string
 */
std::string legendrian10folded(Float_t* par)
{
    return "0.159155*(0.5*1*legendre_Pl(0, TMath::Cos(x)) + 1.5*" + castDoubleToString(par[0]) + "*legendre_Pl(1, TMath::Cos(x)) + 2.5*" + castDoubleToString(par[1]) + "*legendre_Pl(2, TMath::Cos(x)) + 3.5*" + castDoubleToString(par[2]) + "*legendre_Pl(3,TMath::Cos(x)) + 4.5*" + castDoubleToString(par[3]) + "*legendre_Pl(4,TMath::Cos(x)) + 5.5*" + castDoubleToString(par[4]) + "*legendre_Pl(5, TMath::Cos(x)) + 6.5*" + castDoubleToString(par[5]) + "*legendre_Pl(6, TMath::Cos(x))";
}

/**
 * returns the size of an array
 * @param array
 * @returns - array size
 */
static int ArraySize(double array[])
{
    int i = 0;
    while (array[i] != NULL) i++;
    return i;
}

int legendreElements = 0;

/**
 * returns a normalized sum of Legendre polynoms of order 0 to N (given by the size of the parameter array, which sets the relative scaling factors) at x,
 * @param x
 * @param par
 * @returns - legrendian N fold
 */
static double legendrianNfold(double* x, Double_t* par)
{
    double result = 0.5;

    int elements = legendreElements;

    x[0] = TMath::Cos(x[0]);

    for (int l = 1; l < elements; l++)
    {
        result = result + (2. * (l)+1.) * 0.5 * par[l - 1] * legendre_Pl(l, x[0]);
    }

    return 0.159155 * result;
}


/**
 * converts an endf formatted string to a float number
 * @param str
 * @returns - float of a string
 */
float endfNumberConv(std::string str)
{
    using namespace std;
    float pos;
    string sign;
    int length = str.length();

    if (length < 5) return 0;
    if (str.substr(5, 1) == " ") return 0;
    sign = str.substr(length - 1, 1);
    if ((sign == "+") || (sign == "-")) pos = 1;
    sign = str.substr(length - 2, 1);
    if ((sign == "+") || (sign == "-")) pos = 2;
    sign = str.substr(length - 3, 1);
    if ((sign == "+") || (sign == "-")) pos = 3;
    sign = str.substr(length - 4, 1);
    if ((sign == "+") || (sign == "-")) pos = 4;

    string part1 = str.substr(0, length - pos);
    string part2 = str.substr(length - pos, pos);
    //float number = atof(part1.c_str()) * TMath::Power(10, atof(part2.c_str()));
    try
    {
        float number = std::stof(part1) * std::pow(10, std::stof(part2));
        return number;
    } catch (const std::invalid_argument& e)
    {
        // Handle conversion errors (e.g., invalid input)
        std::cerr << "Error: " << e.what() << " "<<part1<<" " <<part2<<std::endl;
        return 0;
    } catch (const std::out_of_range& e)
    {
        // Handle out-of-range errors
        std::cerr << "Error: " << e.what() << " "<<part1<<" " <<part2<<std::endl;
        return 0;
    }
}


/**
 * searches a value in a matrix by an interval algorithm
 * searches the cumulated probability distribution (at row line) for a given probability (value) and extrapolates linearly between upper and lower bound
 * additionally log interval search can be activated
 * @param matrix
 * @param line
 * @param value
 * @param doLogSearch
 * @returns - index of the horizontal position
 */
float getIndexHorizontalPosition(const TMatrixF& matrix, int line, double value, bool doLogSearch)
{
    using namespace std;
    float result;

    //int index;

    int counter = 0;

    int cols = matrix.GetNcols();

    int pos;

    //energy is stored in column lower bound
    int left = matrix.GetColLwb() + 1;
    int right = matrix.GetColUpb();

    if (value > matrix(line, right)) return right - 1;
    if (value < matrix(line, left)) return left;

    while (1)
    {
        if (doLogSearch)
        {
            //pos = left+ (log(value)-log(matrix(left,0)))/(log(matrix(right,0))-log(matrix(left,0))) * (right-left);
        }
        else
        {
            //pos = left+ (value-matrix(left,0))/(matrix(right,0)-matrix(left,0)) * (right-left);
        }

        pos = left + (right - left) * 0.5;

        if (pos >= cols - 2) pos = cols - 2;
        if (pos <= 0) pos = 0;

        if (matrix(line, pos) < value)
        {
            if ((pos < cols - 1) && (matrix(line, pos + 1) > value)) break;
            left = pos + 1;
        }
        else
        {
            if ((pos > 0) && (matrix(line, pos - 1) < value)) { pos = pos - 1; break; }
            right = pos - 1;
        }

        counter++;
        if ((counter > 300) && (!doLogSearch)) doLogSearch = true;
        if ((counter > 100) && (doLogSearch)) doLogSearch = false;

        if (counter > 500)
        {
            cout << "ERROR in Matrix Search" << endl;
            if (doLogSearch) cout << "Log  "; cout << "Value: " << value << " failed at position: " << pos << " in " << cols << " columns at line " << line << endl;
            return 0;
        }
    }

    result = pos + (value - matrix(line, pos)) / (matrix(line, pos + 1) - matrix(line, pos));

    if (result >= pos + 1) result = pos + 0.999;

    return result;
}


/**
 * searches a value in a matrix by an interval algorithm
 * searches the index/energy (first column) and extrapolates linearly between upper and lower bound
 * additionally log interval search can be activated
 * @param matrix
 * @param line
 * @param value
 * @param doLogSearch
 * @returns - index of the position
 */
float getIndexPosition(const TMatrixF& matrix, double value, bool doLogSearch)
{
    using namespace std;
    float result;

    int counter = 0;

    int rows = matrix.GetNrows();

    int pos;

    int left = matrix.GetRowLwb();
    int right = matrix.GetRowUpb();

    if (value > matrix(right, 0)) return right - 1;
    if (value < matrix(left, 0)) return left;

    while (1)
    {
        if (doLogSearch)
        {
            //pos = left+ (log(value)-log(matrix(left,0)))/(log(matrix(right,0))-log(matrix(left,0))) * (right-left);
        }
        else
        {
            //pos = left+ (value-matrix(left,0))/(matrix(right,0)-matrix(left,0)) * (right-left);
        }

        pos = left + (right - left) * 0.5;

        if (pos >= rows - 2) pos = rows - 2;
        if (pos <= 0) pos = 0;

        if (matrix(pos, 0) < value)
        {
            if ((pos < rows - 1) && (matrix(pos + 1, 0) > value)) break;
            left = pos + 1;
        }
        else
        {
            if ((pos > 0) && (matrix(pos - 1, 0) < value)) { pos = pos - 1; break; }
            right = pos - 1;
        }

        counter++;
        if ((counter > 300) && (!doLogSearch)) doLogSearch = true;
        if ((counter > 100) && (doLogSearch)) doLogSearch = false;

        if (counter > 500)
        {
            cout << "ERROR in Matrix Search" << endl;
            if (doLogSearch) cout << "Log  "; cout << "Value: " << value << " failed at position: " << pos << " in " << rows << " rows" << endl;
            return 0;
        }
    }

    result = pos + (value - matrix(pos, 0)) / (matrix(pos + 1, 0) - matrix(pos, 0));

    if (result >= pos + 1) result = pos + 0.999;

    return result;
}

double lastEnergy = 0;
TMatrixF* lastMatrix;
double lastcsInterpolated = 0;

/**
 * searches a matrix for an energy value (first column) and returns a linearly extrapolated cross section value (second column)
 * @param matrix
 * @param energy
 * @returns - cross section value
 */
double calcMeanCS(TMatrixF& matrix, double energy)
{
    //if ((&matrix == lastMatrix) && (energy == lastEnergy)) { return lastcsInterpolated; } // only works if the same cross section is read twice (which in practice never happens)
    float position;
    position = getIndexPosition(matrix, energy, true);
    int index = position;
    float frac = position - index;
    double csInterpolated = (1. - frac) * matrix(index, 1) + frac * matrix(index + 1, 1);
    lastcsInterpolated = csInterpolated;
    lastMatrix = &matrix;
    lastEnergy = energy;
    return  csInterpolated;
}

/**
 * returns cos(thetha) for high energy angle distributions
 * for a given energy the index in the the angular distribution probability matrix is searched
 * then a linearly interpolated cos(theta) from a random number prob is returned
 * @param angleMatrix
 * @param cumulatedProbMatrix
 * @param energy
 * @param prob
 * @returns - High Energy Cos Theta
 */
double getHighEnergyCosTheta(const TMatrixF& angleMatrix, const TMatrixF& cumulatedProbMatrix, double energy, double prob)
{
    float energyPosition = getIndexPosition(angleMatrix, energy, false);
    int energyIndex = energyPosition;
    float energyFrac = energyPosition - energyIndex;

    float anglePosition1 = getIndexHorizontalPosition(cumulatedProbMatrix, energyIndex, prob, false);
    int angleIndex1 = anglePosition1;

    float anglePosition2 = getIndexHorizontalPosition(cumulatedProbMatrix, energyIndex + 1, prob, false);
    int angleIndex2 = anglePosition2;

    float angleFrac1 = anglePosition1 - angleIndex1; //creates the 0.x fracions
    float angleFrac2 = anglePosition2 - angleIndex2;

    double cosThetaLw = (1. - angleFrac1) * angleMatrix(energyIndex, angleIndex1) + angleFrac1 * angleMatrix(energyIndex, angleIndex1 + 1);
    double cosThetaHigh = (1. - angleFrac2) * angleMatrix(energyIndex + 1, angleIndex2) + angleFrac2 * angleMatrix(energyIndex + 1, angleIndex2 + 1);

    double cosTheta = (1. - energyFrac) * cosThetaLw + energyFrac * cosThetaHigh;

    return cosTheta;
}

/**
 * generates a random number (from a cumulative dsitribution function) in the range min to max
 * pointer to an already seeded Random generator is needed
 * @param spectrumFunc
 * @param min
 * @param max
 * @param r
 * @returns - angle from cumulative function
 */
double getAngleFromCumulativeFunction(const TF1* spectrumFunc, float min, float max, TRandom* r)
{
    const int steps = 51;

    float result = 0;

    // (cumulative Function | xValues)
    TMatrixF cumulativeValueMatrix(steps, 2);

    cumulativeValueMatrix(0, 0) = 0; cumulativeValueMatrix(0, 1) = 0;

    //float stepwidth = fabs(max-min/(steps-1));
    double stepwidth = fabs((max - min) / ((steps - 1) * 1.));

    for (int i = 1; i < steps; i++)
    {
        cumulativeValueMatrix(i, 1) = min + (i - 1) * stepwidth;
        cumulativeValueMatrix(i, 0) = cumulativeValueMatrix(i - 1, 0) + spectrumFunc->Eval(min + (i - 1) * stepwidth);
    }

    result = getIndexPosition(cumulativeValueMatrix, cumulativeValueMatrix(steps - 1, 0) * r->Rndm(), false);
    int bin = result;
    float frac = result - bin;

    if (bin >= (steps - 1)) bin--;
    return (1. - frac) * cumulativeValueMatrix(bin, 1) + frac * cumulativeValueMatrix(bin + 1, 1);
}


/**
 * generates a legendre function according to the polynomial coefficients which are linearly extrapolated from the matrix according to the given energy
 * @param matrix
 * @param energy
 * @returns - mean Angular Distribution
 */
TF1* calcMeanAngularDistribution(const TMatrixF& matrix, double energy)
{
    Float_t cn1[21];
    Float_t cn2[21];

    cn1[0] = 0; cn1[1] = 0;
    cn2[0] = 0; cn2[1] = 0;

    float position = getIndexPosition(matrix, energy, false);

    int index = position;
    float frac = position - index;
    int  l;

    for (l = 1; l < 21; l++)
    {
        cn1[l - 1] = matrix(index, l);
        cn2[l - 1] = matrix(index + 1, l);
        if ((cn1[l - 1] == 0) && (cn2[l - 1] == 0)) break;
        if ((frac == 0) && (cn1[l - 1] == 0)) break;
        if (l == 21) break;
    }

    if (l < 3) { l = 3; frac = 0; }
    const int lastcn = l - 1;

    legendreElements = lastcn;
    TF1* legendrian = new TF1("legendrian", legendrianNfold, 0, TMath::Pi(), lastcn);

    for (int l = 0; l < lastcn; l++)
    {
        legendrian->SetParameter(l, (1 - frac) * cn1[l] + frac * cn2[l]);
    }

    return legendrian;
}

/**
 * transposes a matrix (esp. for input images)
 * @param inputMatrix
 *
 */
void turnInputMatrix(TMatrixF& inputMatrix)
{
    Float_t tmpchg;
    Int_t imageSize = inputMatrix.GetNrows();
    for (Int_t im = 0; im < imageSize / 2.; im++)
    {
        for (Int_t jm = 0; jm < imageSize; jm++)
        {
            tmpchg = inputMatrix(jm, im);
            inputMatrix(jm, im) = inputMatrix(jm, imageSize - 1 - im);
            inputMatrix(jm, imageSize - 1 - im) = tmpchg;
        }
    }
    for (Int_t im = 0; im < imageSize; im++)
    {
        for (Int_t jm = 0; jm < imageSize; jm++)
        {
            if (imageSize - 1 - im < jm)
            {
                tmpchg = inputMatrix(jm, im);
                inputMatrix(jm, im) = inputMatrix(imageSize - 1 - im, imageSize - 1 - jm);
                inputMatrix(imageSize - 1 - im, imageSize - 1 - jm) = tmpchg;
            }
        }
    }
}


/**
 * converts old gray scale identifiers to new ones
 * @param inputMatrix
 * @returns - converted old gray scale
 */
int changeInputMatrixValue(int value)
{
    return 0;
}

TH1F* getIncomingSpectrumRel()
{
    //========= Macro generated from object: scatteredSurfaceSpectrumRel/Relative Scattered Neutron at Surface Spectrum
    //========= by ROOT version6.30/02
    Double_t xAxis1[1001] = {1e-09, 2.03039e-09, 2.0617e-09, 2.09396e-09, 2.1272e-09, 2.16145e-09, 2.19674e-09, 2.2331e-09, 2.27057e-09, 2.30918e-09, 2.34896e-09, 2.38995e-09, 2.43219e-09, 2.47571e-09, 2.52055e-09, 2.56675e-09, 2.61436e-09, 2.66341e-09, 2.71396e-09, 2.76604e-09, 2.8197e-09, 2.87499e-09, 2.93197e-09, 2.99067e-09, 3.05116e-09, 3.11349e-09, 3.17771e-09, 3.24388e-09, 3.31206e-09, 3.38232e-09, 3.45471e-09, 3.5293e-09, 3.60615e-09, 3.68534e-09, 3.76694e-09, 3.85102e-09, 3.93765e-09, 4.02691e-09, 4.11889e-09, 4.21366e-09, 4.31131e-09, 4.41193e-09, 4.5156e-09, 4.62243e-09, 4.7325e-09, 4.84592e-09, 4.96278e-09, 5.08319e-09, 5.20727e-09, 5.33511e-09, 5.46684e-09, 5.60257e-09, 5.74242e-09, 5.88652e-09, 6.03501e-09, 6.188e-09, 6.34564e-09, 6.50808e-09, 6.67545e-09, 6.8479e-09, 7.0256e-09, 7.20869e-09, 7.39735e-09, 7.59174e-09, 7.79204e-09, 7.99842e-09, 8.21107e-09, 8.43019e-09, 8.65597e-09, 8.8886e-09, 9.12831e-09, 9.37529e-09, 9.62979e-09, 9.89201e-09, 1.01622e-08, 1.04406e-08, 1.07275e-08, 1.10231e-08, 1.13276e-08, 1.16414e-08, 1.19648e-08, 1.2298e-08, 1.26413e-08, 1.2995e-08, 1.33595e-08, 1.3735e-08, 1.4122e-08, 1.45207e-08, 1.49316e-08, 1.53549e-08, 1.57911e-08, 1.62405e-08, 1.67036e-08, 1.71808e-08, 1.76725e-08, 1.81791e-08, 1.87011e-08, 1.9239e-08, 1.97932e-08, 2.03642e-08, 2.09526e-08, 2.15589e-08, 2.21836e-08, 2.28273e-08, 2.34905e-08, 2.41739e-08, 2.48781e-08, 2.56037e-08, 2.63513e-08, 2.71216e-08, 2.79153e-08, 2.87332e-08, 2.95759e-08, 3.04442e-08, 3.13389e-08, 3.22608e-08, 3.32107e-08, 3.41894e-08, 3.51979e-08, 3.62371e-08, 3.73078e-08, 3.84111e-08, 3.95478e-08, 4.07192e-08, 4.19261e-08, 4.31697e-08, 4.4451e-08, 4.57713e-08, 4.71318e-08, 4.85335e-08, 4.99779e-08, 5.14661e-08, 5.29996e-08, 5.45797e-08, 5.62077e-08, 5.78853e-08, 5.96138e-08, 6.13949e-08, 6.323e-08, 6.5121e-08, 6.70693e-08, 6.90769e-08, 7.11455e-08, 7.3277e-08, 7.54732e-08, 7.77361e-08, 8.00679e-08, 8.24704e-08, 8.4946e-08, 8.74968e-08, 9.01251e-08, 9.28333e-08, 9.56237e-08, 9.8499e-08, 1.01462e-07, 1.04514e-07, 1.0766e-07, 1.10901e-07, 1.1424e-07, 1.17681e-07, 1.21226e-07, 1.2488e-07, 1.28644e-07, 1.32522e-07, 1.36519e-07, 1.40637e-07, 1.4488e-07, 1.49252e-07, 1.53757e-07, 1.58398e-07, 1.63181e-07, 1.68109e-07, 1.73187e-07, 1.78419e-07, 1.8381e-07, 1.89365e-07, 1.95089e-07, 2.00986e-07, 2.07063e-07, 2.13324e-07, 2.19776e-07, 2.26424e-07, 2.33274e-07, 2.40332e-07, 2.47604e-07, 2.55097e-07, 2.62818e-07, 2.70774e-07, 2.78971e-07, 2.87418e-07, 2.96121e-07, 3.05089e-07, 3.14329e-07, 3.23849e-07, 3.3366e-07, 3.43768e-07, 3.54183e-07, 3.64915e-07, 3.75973e-07, 3.87367e-07, 3.99107e-07, 4.11204e-07, 4.23669e-07, 4.36512e-07, 4.49745e-07, 4.63381e-07, 4.77431e-07, 4.91908e-07, 5.06825e-07, 5.22195e-07, 5.38032e-07, 5.5435e-07, 5.71164e-07, 5.88489e-07, 6.06341e-07, 6.24735e-07, 6.43688e-07, 6.63217e-07, 6.83339e-07, 7.04072e-07, 7.25436e-07, 7.47449e-07, 7.7013e-07, 7.93501e-07, 8.17582e-07, 8.42395e-07, 8.67962e-07, 8.94305e-07, 9.2145e-07, 9.49418e-07, 9.78237e-07, 1.00793e-06, 1.03853e-06, 1.07005e-06, 1.10254e-06, 1.13601e-06, 1.1705e-06, 1.20604e-06, 1.24265e-06, 1.28038e-06, 1.31926e-06, 1.35931e-06, 1.40059e-06, 1.44312e-06, 1.48694e-06, 1.53209e-06, 1.57861e-06, 1.62655e-06, 1.67594e-06, 1.72684e-06, 1.77928e-06, 1.83331e-06, 1.88899e-06, 1.94636e-06, 2.00547e-06, 2.06638e-06, 2.12914e-06, 2.1938e-06, 2.26044e-06, 2.32909e-06, 2.39983e-06, 2.47272e-06, 2.54783e-06, 2.62522e-06, 2.70496e-06, 2.78712e-06, 2.87178e-06, 2.95901e-06, 3.04889e-06, 3.14151e-06, 3.23694e-06, 3.33526e-06, 3.43658e-06, 3.54097e-06, 3.64854e-06, 3.75937e-06, 3.87358e-06, 3.99125e-06, 4.1125e-06, 4.23743e-06, 4.36616e-06, 4.4988e-06, 4.63547e-06, 4.77629e-06, 4.9214e-06, 5.07091e-06, 5.22496e-06, 5.3837e-06, 5.54726e-06, 5.71579e-06, 5.88944e-06, 6.06836e-06, 6.25273e-06, 6.44269e-06, 6.63843e-06, 6.84012e-06, 7.04793e-06, 7.26206e-06, 7.4827e-06, 7.71003e-06, 7.94428e-06, 8.18565e-06, 8.43435e-06, 8.6906e-06, 8.95465e-06, 9.22671e-06, 9.50705e-06, 9.7959e-06, 1.00935e-05, 1.04002e-05, 1.07162e-05, 1.10418e-05, 1.13773e-05, 1.1723e-05, 1.20791e-05, 1.24461e-05, 1.28243e-05, 1.3214e-05, 1.36154e-05, 1.40291e-05, 1.44554e-05, 1.48946e-05, 1.53472e-05, 1.58135e-05, 1.6294e-05, 1.6789e-05, 1.72992e-05, 1.78248e-05, 1.83664e-05, 1.89244e-05, 1.94994e-05, 2.00919e-05, 2.07024e-05, 2.13314e-05, 2.19796e-05, 2.26474e-05, 2.33356e-05, 2.40446e-05, 2.47752e-05, 2.5528e-05, 2.63037e-05, 2.71029e-05, 2.79264e-05, 2.8775e-05, 2.96493e-05, 3.05502e-05, 3.14785e-05, 3.2435e-05, 3.34205e-05, 3.4436e-05, 3.54823e-05, 3.65605e-05, 3.76714e-05, 3.8816e-05, 3.99955e-05, 4.12108e-05, 4.2463e-05, 4.37532e-05, 4.50827e-05, 4.64525e-05, 4.7864e-05, 4.93184e-05, 5.08169e-05, 5.2361e-05, 5.39521e-05, 5.55914e-05, 5.72806e-05, 5.90211e-05, 6.08145e-05, 6.26624e-05, 6.45664e-05, 6.65283e-05, 6.85498e-05, 7.06328e-05, 7.2779e-05, 7.49904e-05, 7.72691e-05, 7.96169e-05, 8.20362e-05, 8.45289e-05, 8.70974e-05, 8.97439e-05, 9.24708e-05, 9.52806e-05, 9.81758e-05, 0.000101159, 0.000104233, 0.0001074, 0.000110663, 0.000114026, 0.000117491, 0.000121061, 0.000124739, 0.00012853, 0.000132435, 0.000136459, 0.000140606, 0.000144878, 0.00014928, 0.000153816, 0.00015849, 0.000163306, 0.000168268, 0.000173381, 0.00017865, 0.000184078, 0.000189672, 0.000195435, 0.000201373, 0.000207492, 0.000213797, 0.000220294, 0.000226987, 0.000233885, 0.000240992, 0.000248314, 0.00025586, 0.000263634, 0.000271645, 0.000279899, 0.000288404, 0.000297168, 0.000306197, 0.000315501, 0.000325088, 0.000334966, 0.000345145, 0.000355632, 0.000366439, 0.000377573, 0.000389046, 0.000400868, 0.000413049, 0.000425599, 0.000438532, 0.000451857, 0.000465587, 0.000479734, 0.000494312, 0.000509332, 0.000524808, 0.000540755, 0.000557187, 0.000574117, 0.000591563, 0.000609538, 0.000628059, 0.000647144, 0.000666808, 0.000687069, 0.000707947, 0.000729459, 0.000751624, 0.000774463, 0.000797996, 0.000822244, 0.000847228, 0.000872972, 0.000899499, 0.000926831, 0.000954994, 0.000984012, 0.00101391, 0.00104472, 0.00107647, 0.00110918, 0.00114288, 0.00117761, 0.00121339, 0.00125026, 0.00128825, 0.0013274, 0.00136773, 0.00140929, 0.00145211, 0.00149624, 0.0015417, 0.00158855, 0.00163682, 0.00168655, 0.0017378, 0.00179061, 0.00184502, 0.00190108, 0.00195885, 0.00201837, 0.0020797, 0.00214289, 0.00220801, 0.0022751, 0.00234423, 0.00241546, 0.00248886, 0.00256449, 0.00264241, 0.0027227, 0.00280543, 0.00289068, 0.00297852, 0.00306902, 0.00316228, 0.00325837, 0.00335738, 0.00345939, 0.00356451, 0.00367282, 0.00378443, 0.00389942, 0.00401791, 0.00414, 0.0042658, 0.00439542, 0.00452898, 0.00466659, 0.00480839, 0.0049545, 0.00510505, 0.00526017, 0.00542001, 0.0055847, 0.0057544, 0.00592925, 0.00610942, 0.00629506, 0.00648635, 0.00668344, 0.00688652, 0.00709578, 0.00731139, 0.00753356, 0.00776247, 0.00799834, 0.00824138, 0.00849181, 0.00874984, 0.00901571, 0.00928966, 0.00957194, 0.0098628, 0.0101625, 0.0104713, 0.0107895, 0.0111173, 0.0114551, 0.0118032, 0.0121619, 0.0125314, 0.0129122, 0.0133045, 0.0137088, 0.0141254, 0.0145546, 0.0149968, 0.0154525, 0.0159221, 0.0164059, 0.0169044, 0.0174181, 0.0179473, 0.0184927, 0.0190546, 0.0196336, 0.0202302, 0.0208449, 0.0214783, 0.0221309, 0.0228034, 0.0234963, 0.0242103, 0.0249459, 0.025704, 0.026485, 0.0272898, 0.028119, 0.0289734, 0.0298538, 0.030761, 0.0316957, 0.0326588, 0.0336512, 0.0346737, 0.0357273, 0.0368129, 0.0379315, 0.0390841, 0.0402717, 0.0414954, 0.0427563, 0.0440555, 0.0453942, 0.0467735, 0.0481948, 0.0496592, 0.0511682, 0.052723, 0.054325, 0.0559758, 0.0576766, 0.0594292, 0.061235, 0.0630957, 0.065013, 0.0669885, 0.069024, 0.0711214, 0.0732825, 0.0755092, 0.0778037, 0.0801678, 0.0826038, 0.0851138, 0.0877001, 0.0903649, 0.0931108, 0.0959401, 0.0988553, 0.101859, 0.104954, 0.108143, 0.111429, 0.114815, 0.118304, 0.121899, 0.125603, 0.12942, 0.133352, 0.137404, 0.141579, 0.145881, 0.150314, 0.154882, 0.159588, 0.164437, 0.169434, 0.174582, 0.179887, 0.185353, 0.190985, 0.196789, 0.202768, 0.20893, 0.215278, 0.22182, 0.22856, 0.235505, 0.242661, 0.250035, 0.257632, 0.265461, 0.273527, 0.281838, 0.290402, 0.299226, 0.308319, 0.317687, 0.327341, 0.337287, 0.347536, 0.358096, 0.368978, 0.380189, 0.391742, 0.403645, 0.415911, 0.428549, 0.44157, 0.454988, 0.468813, 0.483059, 0.497737, 0.512861, 0.528445, 0.544503, 0.561048, 0.578096, 0.595662, 0.613762, 0.632412, 0.651628, 0.671429, 0.691831, 0.712853, 0.734514, 0.756833, 0.77983, 0.803526, 0.827942, 0.8531, 0.879023, 0.905733, 0.933254, 0.961612, 0.990832, 1.02094, 1.05196, 1.08393, 1.11686, 1.1508, 1.18577, 1.2218, 1.25893, 1.29718, 1.3366, 1.37721, 1.41906, 1.46218, 1.50661, 1.55239, 1.59956, 1.64816, 1.69824, 1.74985, 1.80302, 1.8578, 1.91426, 1.97242, 2.03236, 2.09411, 2.15774, 2.22331, 2.29087, 2.36048, 2.4322, 2.50611, 2.58226, 2.66073, 2.74157, 2.82488, 2.91072, 2.99916, 3.0903, 3.1842, 3.28095, 3.38065, 3.48337, 3.58922, 3.69828, 3.81066, 3.92645, 4.04576, 4.16869, 4.29536, 4.42588, 4.56037, 4.69894, 4.84172, 4.98884, 5.14044, 5.29663, 5.45758, 5.62341, 5.79429, 5.97035, 6.15177, 6.3387, 6.53131, 6.72977, 6.93426, 7.14496, 7.36207, 7.58578, 7.81628, 8.05378, 8.29851, 8.55067, 8.81049, 9.07821, 9.35406, 9.63829, 9.93116, 10.2329, 10.5439, 10.8643, 11.1944, 11.5345, 11.885, 12.2462, 12.6183, 13.0017, 13.3968, 13.8038, 14.2233, 14.6555, 15.1008, 15.5597, 16.0325, 16.5196, 17.0216, 17.5388, 18.0717, 18.6209, 19.1867, 19.7697, 20.3704, 20.9894, 21.6272, 22.2844, 22.9615, 23.6592, 24.3781, 25.1189, 25.8821, 26.6686, 27.4789, 28.3139, 29.1743, 30.0608, 30.9742, 31.9154, 32.8852, 33.8844, 34.914, 35.9749, 37.0681, 38.1944, 39.355, 40.5509, 41.783, 43.0527, 44.3609, 45.7088, 47.0977, 48.5289, 50.0035, 51.5229, 53.0884, 54.7016, 56.3638, 58.0764, 59.8412, 61.6595, 63.5331, 65.4636, 67.4528, 69.5024, 71.6143, 73.7904, 76.0326, 78.343, 80.7235, 83.1764, 85.7038, 88.308, 90.9913, 93.7562, 96.6051, 99.5405, 102.565, 105.682, 108.893, 112.202, 115.611, 119.124, 122.744, 126.474, 130.317, 134.276, 138.357, 142.561, 146.893, 151.356, 155.955, 160.694, 165.577, 170.608, 175.792, 181.134, 186.638, 192.309, 198.153, 204.174, 210.378, 216.77, 223.357, 230.144, 237.137, 244.343, 251.768, 259.418, 267.301, 275.423, 283.792, 292.415, 301.301, 310.456, 319.89, 329.61, 339.625, 349.945, 360.579, 371.535, 382.825, 394.457, 406.443, 418.794, 431.519, 444.631, 458.142, 472.063, 486.407, 501.187, 516.416, 532.108, 548.277, 564.937, 582.103, 599.791, 618.016, 636.796, 656.145, 676.083, 696.627, 717.794, 739.605, 762.079, 785.236, 809.096, 833.681, 859.014, 885.116, 912.011, 939.723, 968.278, 997.7, 1028.02, 1059.25, 1091.44, 1124.6, 1158.78, 1193.99, 1230.27, 1267.65, 1306.17, 1345.86, 1386.76, 1428.89, 1472.31, 1517.05, 1563.15, 1610.65, 1659.59, 1710.02, 1761.98, 1815.52, 1870.68, 1927.52, 1986.09, 2046.44, 2108.63, 2172.7, 2238.72, 2306.75, 2376.84, 2449.06, 2523.48, 2600.16, 2679.17, 2760.58, 2844.46, 2930.89, 3019.95, 3111.72, 3206.27, 3303.7, 3404.08, 3507.52, 3614.1, 3723.92, 3837.07, 3953.67, 4073.8, 4197.59, 4325.14, 4456.56, 4591.98, 4731.51, 4875.28, 5023.43, 5176.07, 5333.35, 5495.41, 5662.39, 5834.45, 6011.74, 6194.41, 6382.63, 6576.58, 6776.42, 6982.32, 7194.49, 7413.1, 7638.36, 7870.46, 8109.61, 8356.03, 8609.94, 8871.56, 9141.13, 9418.9, 9705.1, 10000};

    TH1F *scatteredSurfaceSpectrumRel__1 = new TH1F("scatteredSurfaceSpectrumRel__1","Relative Scattered Neutron at Surface Spectrum",1000, xAxis1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(3,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(4,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(5,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(6,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(7,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(8,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(9,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(10,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(11,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(12,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(13,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(14,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(15,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(16,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(17,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(18,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(19,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(20,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(21,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(22,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(23,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(24,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(25,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(26,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(27,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(28,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(29,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(30,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(31,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(32,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(33,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(34,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(35,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(36,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(37,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(38,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(39,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(40,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(41,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(42,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(43,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(44,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(45,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(46,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(47,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(48,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(49,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(50,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(51,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(52,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(53,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(54,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(55,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(56,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(57,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(58,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(59,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(60,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(61,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(62,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(63,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(64,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(65,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(66,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(67,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(68,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(69,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(70,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(71,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(72,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(73,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(74,0.986842);
    scatteredSurfaceSpectrumRel__1->SetBinContent(75,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(76,0.989011);
    scatteredSurfaceSpectrumRel__1->SetBinContent(77,0.979167);
    scatteredSurfaceSpectrumRel__1->SetBinContent(78,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(79,0.991071);
    scatteredSurfaceSpectrumRel__1->SetBinContent(80,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(81,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(82,0.990476);
    scatteredSurfaceSpectrumRel__1->SetBinContent(83,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(84,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(85,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(86,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(87,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(88,0.991597);
    scatteredSurfaceSpectrumRel__1->SetBinContent(89,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(90,0.990385);
    scatteredSurfaceSpectrumRel__1->SetBinContent(91,0.990826);
    scatteredSurfaceSpectrumRel__1->SetBinContent(92,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(93,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(94,1);
    scatteredSurfaceSpectrumRel__1->SetBinContent(95,0.976562);
    scatteredSurfaceSpectrumRel__1->SetBinContent(96,0.980583);
    scatteredSurfaceSpectrumRel__1->SetBinContent(97,0.992593);
    scatteredSurfaceSpectrumRel__1->SetBinContent(98,0.969466);
    scatteredSurfaceSpectrumRel__1->SetBinContent(99,0.992647);
    scatteredSurfaceSpectrumRel__1->SetBinContent(100,0.972973);
    scatteredSurfaceSpectrumRel__1->SetBinContent(101,0.981366);
    scatteredSurfaceSpectrumRel__1->SetBinContent(102,0.977612);
    scatteredSurfaceSpectrumRel__1->SetBinContent(103,0.984848);
    scatteredSurfaceSpectrumRel__1->SetBinContent(104,0.977444);
    scatteredSurfaceSpectrumRel__1->SetBinContent(105,0.968);
    scatteredSurfaceSpectrumRel__1->SetBinContent(106,0.984375);
    scatteredSurfaceSpectrumRel__1->SetBinContent(107,0.978417);
    scatteredSurfaceSpectrumRel__1->SetBinContent(108,0.964789);
    scatteredSurfaceSpectrumRel__1->SetBinContent(109,0.969325);
    scatteredSurfaceSpectrumRel__1->SetBinContent(110,0.981707);
    scatteredSurfaceSpectrumRel__1->SetBinContent(111,0.994012);
    scatteredSurfaceSpectrumRel__1->SetBinContent(112,0.972973);
    scatteredSurfaceSpectrumRel__1->SetBinContent(113,0.945946);
    scatteredSurfaceSpectrumRel__1->SetBinContent(114,0.987179);
    scatteredSurfaceSpectrumRel__1->SetBinContent(115,0.975155);
    scatteredSurfaceSpectrumRel__1->SetBinContent(116,0.95679);
    scatteredSurfaceSpectrumRel__1->SetBinContent(117,0.964824);
    scatteredSurfaceSpectrumRel__1->SetBinContent(118,0.951351);
    scatteredSurfaceSpectrumRel__1->SetBinContent(119,0.973822);
    scatteredSurfaceSpectrumRel__1->SetBinContent(120,0.982456);
    scatteredSurfaceSpectrumRel__1->SetBinContent(121,0.964646);
    scatteredSurfaceSpectrumRel__1->SetBinContent(122,0.949686);
    scatteredSurfaceSpectrumRel__1->SetBinContent(123,0.956731);
    scatteredSurfaceSpectrumRel__1->SetBinContent(124,0.949495);
    scatteredSurfaceSpectrumRel__1->SetBinContent(125,0.952128);
    scatteredSurfaceSpectrumRel__1->SetBinContent(126,0.958763);
    scatteredSurfaceSpectrumRel__1->SetBinContent(127,0.967742);
    scatteredSurfaceSpectrumRel__1->SetBinContent(128,0.981651);
    scatteredSurfaceSpectrumRel__1->SetBinContent(129,0.962085);
    scatteredSurfaceSpectrumRel__1->SetBinContent(130,0.912621);
    scatteredSurfaceSpectrumRel__1->SetBinContent(131,0.958974);
    scatteredSurfaceSpectrumRel__1->SetBinContent(132,0.925926);
    scatteredSurfaceSpectrumRel__1->SetBinContent(133,0.934498);
    scatteredSurfaceSpectrumRel__1->SetBinContent(134,0.934211);
    scatteredSurfaceSpectrumRel__1->SetBinContent(135,0.925926);
    scatteredSurfaceSpectrumRel__1->SetBinContent(136,0.907834);
    scatteredSurfaceSpectrumRel__1->SetBinContent(137,0.912442);
    scatteredSurfaceSpectrumRel__1->SetBinContent(138,0.9);
    scatteredSurfaceSpectrumRel__1->SetBinContent(139,0.886878);
    scatteredSurfaceSpectrumRel__1->SetBinContent(140,0.878378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(141,0.926829);
    scatteredSurfaceSpectrumRel__1->SetBinContent(142,0.868217);
    scatteredSurfaceSpectrumRel__1->SetBinContent(143,0.91129);
    scatteredSurfaceSpectrumRel__1->SetBinContent(144,0.875486);
    scatteredSurfaceSpectrumRel__1->SetBinContent(145,0.895683);
    scatteredSurfaceSpectrumRel__1->SetBinContent(146,0.889273);
    scatteredSurfaceSpectrumRel__1->SetBinContent(147,0.883392);
    scatteredSurfaceSpectrumRel__1->SetBinContent(148,0.905797);
    scatteredSurfaceSpectrumRel__1->SetBinContent(149,0.882353);
    scatteredSurfaceSpectrumRel__1->SetBinContent(150,0.885714);
    scatteredSurfaceSpectrumRel__1->SetBinContent(151,0.860927);
    scatteredSurfaceSpectrumRel__1->SetBinContent(152,0.871875);
    scatteredSurfaceSpectrumRel__1->SetBinContent(153,0.883721);
    scatteredSurfaceSpectrumRel__1->SetBinContent(154,0.858065);
    scatteredSurfaceSpectrumRel__1->SetBinContent(155,0.827381);
    scatteredSurfaceSpectrumRel__1->SetBinContent(156,0.810734);
    scatteredSurfaceSpectrumRel__1->SetBinContent(157,0.829851);
    scatteredSurfaceSpectrumRel__1->SetBinContent(158,0.828169);
    scatteredSurfaceSpectrumRel__1->SetBinContent(159,0.84136);
    scatteredSurfaceSpectrumRel__1->SetBinContent(160,0.832861);
    scatteredSurfaceSpectrumRel__1->SetBinContent(161,0.851459);
    scatteredSurfaceSpectrumRel__1->SetBinContent(162,0.815584);
    scatteredSurfaceSpectrumRel__1->SetBinContent(163,0.814721);
    scatteredSurfaceSpectrumRel__1->SetBinContent(164,0.837772);
    scatteredSurfaceSpectrumRel__1->SetBinContent(165,0.840102);
    scatteredSurfaceSpectrumRel__1->SetBinContent(166,0.820399);
    scatteredSurfaceSpectrumRel__1->SetBinContent(167,0.848718);
    scatteredSurfaceSpectrumRel__1->SetBinContent(168,0.822844);
    scatteredSurfaceSpectrumRel__1->SetBinContent(169,0.780749);
    scatteredSurfaceSpectrumRel__1->SetBinContent(170,0.81746);
    scatteredSurfaceSpectrumRel__1->SetBinContent(171,0.828283);
    scatteredSurfaceSpectrumRel__1->SetBinContent(172,0.842222);
    scatteredSurfaceSpectrumRel__1->SetBinContent(173,0.811111);
    scatteredSurfaceSpectrumRel__1->SetBinContent(174,0.779698);
    scatteredSurfaceSpectrumRel__1->SetBinContent(175,0.820175);
    scatteredSurfaceSpectrumRel__1->SetBinContent(176,0.816038);
    scatteredSurfaceSpectrumRel__1->SetBinContent(177,0.835498);
    scatteredSurfaceSpectrumRel__1->SetBinContent(178,0.80597);
    scatteredSurfaceSpectrumRel__1->SetBinContent(179,0.79386);
    scatteredSurfaceSpectrumRel__1->SetBinContent(180,0.815195);
    scatteredSurfaceSpectrumRel__1->SetBinContent(181,0.804671);
    scatteredSurfaceSpectrumRel__1->SetBinContent(182,0.822811);
    scatteredSurfaceSpectrumRel__1->SetBinContent(183,0.825221);
    scatteredSurfaceSpectrumRel__1->SetBinContent(184,0.811765);
    scatteredSurfaceSpectrumRel__1->SetBinContent(185,0.788785);
    scatteredSurfaceSpectrumRel__1->SetBinContent(186,0.808061);
    scatteredSurfaceSpectrumRel__1->SetBinContent(187,0.795591);
    scatteredSurfaceSpectrumRel__1->SetBinContent(188,0.769231);
    scatteredSurfaceSpectrumRel__1->SetBinContent(189,0.801818);
    scatteredSurfaceSpectrumRel__1->SetBinContent(190,0.81768);
    scatteredSurfaceSpectrumRel__1->SetBinContent(191,0.783835);
    scatteredSurfaceSpectrumRel__1->SetBinContent(192,0.773408);
    scatteredSurfaceSpectrumRel__1->SetBinContent(193,0.76998);
    scatteredSurfaceSpectrumRel__1->SetBinContent(194,0.770245);
    scatteredSurfaceSpectrumRel__1->SetBinContent(195,0.781609);
    scatteredSurfaceSpectrumRel__1->SetBinContent(196,0.75);
    scatteredSurfaceSpectrumRel__1->SetBinContent(197,0.753398);
    scatteredSurfaceSpectrumRel__1->SetBinContent(198,0.804089);
    scatteredSurfaceSpectrumRel__1->SetBinContent(199,0.759582);
    scatteredSurfaceSpectrumRel__1->SetBinContent(200,0.735849);
    scatteredSurfaceSpectrumRel__1->SetBinContent(201,0.784946);
    scatteredSurfaceSpectrumRel__1->SetBinContent(202,0.732441);
    scatteredSurfaceSpectrumRel__1->SetBinContent(203,0.748768);
    scatteredSurfaceSpectrumRel__1->SetBinContent(204,0.785832);
    scatteredSurfaceSpectrumRel__1->SetBinContent(205,0.734114);
    scatteredSurfaceSpectrumRel__1->SetBinContent(206,0.739394);
    scatteredSurfaceSpectrumRel__1->SetBinContent(207,0.790164);
    scatteredSurfaceSpectrumRel__1->SetBinContent(208,0.762215);
    scatteredSurfaceSpectrumRel__1->SetBinContent(209,0.795764);
    scatteredSurfaceSpectrumRel__1->SetBinContent(210,0.748823);
    scatteredSurfaceSpectrumRel__1->SetBinContent(211,0.746201);
    scatteredSurfaceSpectrumRel__1->SetBinContent(212,0.757225);
    scatteredSurfaceSpectrumRel__1->SetBinContent(213,0.771028);
    scatteredSurfaceSpectrumRel__1->SetBinContent(214,0.737389);
    scatteredSurfaceSpectrumRel__1->SetBinContent(215,0.779661);
    scatteredSurfaceSpectrumRel__1->SetBinContent(216,0.752846);
    scatteredSurfaceSpectrumRel__1->SetBinContent(217,0.732006);
    scatteredSurfaceSpectrumRel__1->SetBinContent(218,0.774194);
    scatteredSurfaceSpectrumRel__1->SetBinContent(219,0.749258);
    scatteredSurfaceSpectrumRel__1->SetBinContent(220,0.759547);
    scatteredSurfaceSpectrumRel__1->SetBinContent(221,0.739788);
    scatteredSurfaceSpectrumRel__1->SetBinContent(222,0.769444);
    scatteredSurfaceSpectrumRel__1->SetBinContent(223,0.766117);
    scatteredSurfaceSpectrumRel__1->SetBinContent(224,0.752161);
    scatteredSurfaceSpectrumRel__1->SetBinContent(225,0.757576);
    scatteredSurfaceSpectrumRel__1->SetBinContent(226,0.74344);
    scatteredSurfaceSpectrumRel__1->SetBinContent(227,0.727136);
    scatteredSurfaceSpectrumRel__1->SetBinContent(228,0.74743);
    scatteredSurfaceSpectrumRel__1->SetBinContent(229,0.754335);
    scatteredSurfaceSpectrumRel__1->SetBinContent(230,0.75436);
    scatteredSurfaceSpectrumRel__1->SetBinContent(231,0.748191);
    scatteredSurfaceSpectrumRel__1->SetBinContent(232,0.725517);
    scatteredSurfaceSpectrumRel__1->SetBinContent(233,0.71887);
    scatteredSurfaceSpectrumRel__1->SetBinContent(234,0.728307);
    scatteredSurfaceSpectrumRel__1->SetBinContent(235,0.730929);
    scatteredSurfaceSpectrumRel__1->SetBinContent(236,0.703757);
    scatteredSurfaceSpectrumRel__1->SetBinContent(237,0.732194);
    scatteredSurfaceSpectrumRel__1->SetBinContent(238,0.713235);
    scatteredSurfaceSpectrumRel__1->SetBinContent(239,0.747899);
    scatteredSurfaceSpectrumRel__1->SetBinContent(240,0.733784);
    scatteredSurfaceSpectrumRel__1->SetBinContent(241,0.756428);
    scatteredSurfaceSpectrumRel__1->SetBinContent(242,0.72423);
    scatteredSurfaceSpectrumRel__1->SetBinContent(243,0.708628);
    scatteredSurfaceSpectrumRel__1->SetBinContent(244,0.746939);
    scatteredSurfaceSpectrumRel__1->SetBinContent(245,0.715436);
    scatteredSurfaceSpectrumRel__1->SetBinContent(246,0.71657);
    scatteredSurfaceSpectrumRel__1->SetBinContent(247,0.737796);
    scatteredSurfaceSpectrumRel__1->SetBinContent(248,0.686464);
    scatteredSurfaceSpectrumRel__1->SetBinContent(249,0.706595);
    scatteredSurfaceSpectrumRel__1->SetBinContent(250,0.716981);
    scatteredSurfaceSpectrumRel__1->SetBinContent(251,0.702836);
    scatteredSurfaceSpectrumRel__1->SetBinContent(252,0.704762);
    scatteredSurfaceSpectrumRel__1->SetBinContent(253,0.733513);
    scatteredSurfaceSpectrumRel__1->SetBinContent(254,0.714096);
    scatteredSurfaceSpectrumRel__1->SetBinContent(255,0.721024);
    scatteredSurfaceSpectrumRel__1->SetBinContent(256,0.740209);
    scatteredSurfaceSpectrumRel__1->SetBinContent(257,0.726272);
    scatteredSurfaceSpectrumRel__1->SetBinContent(258,0.694019);
    scatteredSurfaceSpectrumRel__1->SetBinContent(259,0.726573);
    scatteredSurfaceSpectrumRel__1->SetBinContent(260,0.713535);
    scatteredSurfaceSpectrumRel__1->SetBinContent(261,0.701453);
    scatteredSurfaceSpectrumRel__1->SetBinContent(262,0.730914);
    scatteredSurfaceSpectrumRel__1->SetBinContent(263,0.706649);
    scatteredSurfaceSpectrumRel__1->SetBinContent(264,0.721569);
    scatteredSurfaceSpectrumRel__1->SetBinContent(265,0.706378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(266,0.686047);
    scatteredSurfaceSpectrumRel__1->SetBinContent(267,0.707835);
    scatteredSurfaceSpectrumRel__1->SetBinContent(268,0.682045);
    scatteredSurfaceSpectrumRel__1->SetBinContent(269,0.700865);
    scatteredSurfaceSpectrumRel__1->SetBinContent(270,0.682809);
    scatteredSurfaceSpectrumRel__1->SetBinContent(271,0.703794);
    scatteredSurfaceSpectrumRel__1->SetBinContent(272,0.676471);
    scatteredSurfaceSpectrumRel__1->SetBinContent(273,0.705506);
    scatteredSurfaceSpectrumRel__1->SetBinContent(274,0.705479);
    scatteredSurfaceSpectrumRel__1->SetBinContent(275,0.683528);
    scatteredSurfaceSpectrumRel__1->SetBinContent(276,0.693154);
    scatteredSurfaceSpectrumRel__1->SetBinContent(277,0.702801);
    scatteredSurfaceSpectrumRel__1->SetBinContent(278,0.681818);
    scatteredSurfaceSpectrumRel__1->SetBinContent(279,0.70229);
    scatteredSurfaceSpectrumRel__1->SetBinContent(280,0.69201);
    scatteredSurfaceSpectrumRel__1->SetBinContent(281,0.683814);
    scatteredSurfaceSpectrumRel__1->SetBinContent(282,0.673418);
    scatteredSurfaceSpectrumRel__1->SetBinContent(283,0.683374);
    scatteredSurfaceSpectrumRel__1->SetBinContent(284,0.668639);
    scatteredSurfaceSpectrumRel__1->SetBinContent(285,0.6796);
    scatteredSurfaceSpectrumRel__1->SetBinContent(286,0.667082);
    scatteredSurfaceSpectrumRel__1->SetBinContent(287,0.674728);
    scatteredSurfaceSpectrumRel__1->SetBinContent(288,0.677262);
    scatteredSurfaceSpectrumRel__1->SetBinContent(289,0.699615);
    scatteredSurfaceSpectrumRel__1->SetBinContent(290,0.680827);
    scatteredSurfaceSpectrumRel__1->SetBinContent(291,0.691773);
    scatteredSurfaceSpectrumRel__1->SetBinContent(292,0.686845);
    scatteredSurfaceSpectrumRel__1->SetBinContent(293,0.662883);
    scatteredSurfaceSpectrumRel__1->SetBinContent(294,0.666667);
    scatteredSurfaceSpectrumRel__1->SetBinContent(295,0.70892);
    scatteredSurfaceSpectrumRel__1->SetBinContent(296,0.692982);
    scatteredSurfaceSpectrumRel__1->SetBinContent(297,0.684588);
    scatteredSurfaceSpectrumRel__1->SetBinContent(298,0.653664);
    scatteredSurfaceSpectrumRel__1->SetBinContent(299,0.679659);
    scatteredSurfaceSpectrumRel__1->SetBinContent(300,0.683908);
    scatteredSurfaceSpectrumRel__1->SetBinContent(301,0.655443);
    scatteredSurfaceSpectrumRel__1->SetBinContent(302,0.664368);
    scatteredSurfaceSpectrumRel__1->SetBinContent(303,0.684884);
    scatteredSurfaceSpectrumRel__1->SetBinContent(304,0.631881);
    scatteredSurfaceSpectrumRel__1->SetBinContent(305,0.649507);
    scatteredSurfaceSpectrumRel__1->SetBinContent(306,0.651584);
    scatteredSurfaceSpectrumRel__1->SetBinContent(307,0.679634);
    scatteredSurfaceSpectrumRel__1->SetBinContent(308,0.649394);
    scatteredSurfaceSpectrumRel__1->SetBinContent(309,0.656943);
    scatteredSurfaceSpectrumRel__1->SetBinContent(310,0.697816);
    scatteredSurfaceSpectrumRel__1->SetBinContent(311,0.691796);
    scatteredSurfaceSpectrumRel__1->SetBinContent(312,0.644493);
    scatteredSurfaceSpectrumRel__1->SetBinContent(313,0.693057);
    scatteredSurfaceSpectrumRel__1->SetBinContent(314,0.668867);
    scatteredSurfaceSpectrumRel__1->SetBinContent(315,0.659686);
    scatteredSurfaceSpectrumRel__1->SetBinContent(316,0.640223);
    scatteredSurfaceSpectrumRel__1->SetBinContent(317,0.652968);
    scatteredSurfaceSpectrumRel__1->SetBinContent(318,0.638761);
    scatteredSurfaceSpectrumRel__1->SetBinContent(319,0.654684);
    scatteredSurfaceSpectrumRel__1->SetBinContent(320,0.664692);
    scatteredSurfaceSpectrumRel__1->SetBinContent(321,0.657385);
    scatteredSurfaceSpectrumRel__1->SetBinContent(322,0.654906);
    scatteredSurfaceSpectrumRel__1->SetBinContent(323,0.659277);
    scatteredSurfaceSpectrumRel__1->SetBinContent(324,0.649123);
    scatteredSurfaceSpectrumRel__1->SetBinContent(325,0.637647);
    scatteredSurfaceSpectrumRel__1->SetBinContent(326,0.644815);
    scatteredSurfaceSpectrumRel__1->SetBinContent(327,0.644049);
    scatteredSurfaceSpectrumRel__1->SetBinContent(328,0.636848);
    scatteredSurfaceSpectrumRel__1->SetBinContent(329,0.633222);
    scatteredSurfaceSpectrumRel__1->SetBinContent(330,0.651294);
    scatteredSurfaceSpectrumRel__1->SetBinContent(331,0.643411);
    scatteredSurfaceSpectrumRel__1->SetBinContent(332,0.628289);
    scatteredSurfaceSpectrumRel__1->SetBinContent(333,0.628729);
    scatteredSurfaceSpectrumRel__1->SetBinContent(334,0.632898);
    scatteredSurfaceSpectrumRel__1->SetBinContent(335,0.643098);
    scatteredSurfaceSpectrumRel__1->SetBinContent(336,0.642619);
    scatteredSurfaceSpectrumRel__1->SetBinContent(337,0.610169);
    scatteredSurfaceSpectrumRel__1->SetBinContent(338,0.652778);
    scatteredSurfaceSpectrumRel__1->SetBinContent(339,0.628415);
    scatteredSurfaceSpectrumRel__1->SetBinContent(340,0.642191);
    scatteredSurfaceSpectrumRel__1->SetBinContent(341,0.62322);
    scatteredSurfaceSpectrumRel__1->SetBinContent(342,0.649438);
    scatteredSurfaceSpectrumRel__1->SetBinContent(343,0.659142);
    scatteredSurfaceSpectrumRel__1->SetBinContent(344,0.643177);
    scatteredSurfaceSpectrumRel__1->SetBinContent(345,0.60827);
    scatteredSurfaceSpectrumRel__1->SetBinContent(346,0.617778);
    scatteredSurfaceSpectrumRel__1->SetBinContent(347,0.632979);
    scatteredSurfaceSpectrumRel__1->SetBinContent(348,0.642539);
    scatteredSurfaceSpectrumRel__1->SetBinContent(349,0.631243);
    scatteredSurfaceSpectrumRel__1->SetBinContent(350,0.634254);
    scatteredSurfaceSpectrumRel__1->SetBinContent(351,0.632584);
    scatteredSurfaceSpectrumRel__1->SetBinContent(352,0.616103);
    scatteredSurfaceSpectrumRel__1->SetBinContent(353,0.650342);
    scatteredSurfaceSpectrumRel__1->SetBinContent(354,0.614458);
    scatteredSurfaceSpectrumRel__1->SetBinContent(355,0.612903);
    scatteredSurfaceSpectrumRel__1->SetBinContent(356,0.632804);
    scatteredSurfaceSpectrumRel__1->SetBinContent(357,0.634066);
    scatteredSurfaceSpectrumRel__1->SetBinContent(358,0.624502);
    scatteredSurfaceSpectrumRel__1->SetBinContent(359,0.615556);
    scatteredSurfaceSpectrumRel__1->SetBinContent(360,0.604396);
    scatteredSurfaceSpectrumRel__1->SetBinContent(361,0.632674);
    scatteredSurfaceSpectrumRel__1->SetBinContent(362,0.632563);
    scatteredSurfaceSpectrumRel__1->SetBinContent(363,0.589613);
    scatteredSurfaceSpectrumRel__1->SetBinContent(364,0.630458);
    scatteredSurfaceSpectrumRel__1->SetBinContent(365,0.595927);
    scatteredSurfaceSpectrumRel__1->SetBinContent(366,0.630184);
    scatteredSurfaceSpectrumRel__1->SetBinContent(367,0.571906);
    scatteredSurfaceSpectrumRel__1->SetBinContent(368,0.613944);
    scatteredSurfaceSpectrumRel__1->SetBinContent(369,0.603409);
    scatteredSurfaceSpectrumRel__1->SetBinContent(370,0.63245);
    scatteredSurfaceSpectrumRel__1->SetBinContent(371,0.610638);
    scatteredSurfaceSpectrumRel__1->SetBinContent(372,0.604236);
    scatteredSurfaceSpectrumRel__1->SetBinContent(373,0.653039);
    scatteredSurfaceSpectrumRel__1->SetBinContent(374,0.616904);
    scatteredSurfaceSpectrumRel__1->SetBinContent(375,0.611597);
    scatteredSurfaceSpectrumRel__1->SetBinContent(376,0.628507);
    scatteredSurfaceSpectrumRel__1->SetBinContent(377,0.603279);
    scatteredSurfaceSpectrumRel__1->SetBinContent(378,0.605967);
    scatteredSurfaceSpectrumRel__1->SetBinContent(379,0.616904);
    scatteredSurfaceSpectrumRel__1->SetBinContent(380,0.585153);
    scatteredSurfaceSpectrumRel__1->SetBinContent(381,0.638829);
    scatteredSurfaceSpectrumRel__1->SetBinContent(382,0.630363);
    scatteredSurfaceSpectrumRel__1->SetBinContent(383,0.58587);
    scatteredSurfaceSpectrumRel__1->SetBinContent(384,0.597471);
    scatteredSurfaceSpectrumRel__1->SetBinContent(385,0.602892);
    scatteredSurfaceSpectrumRel__1->SetBinContent(386,0.588235);
    scatteredSurfaceSpectrumRel__1->SetBinContent(387,0.601307);
    scatteredSurfaceSpectrumRel__1->SetBinContent(388,0.586777);
    scatteredSurfaceSpectrumRel__1->SetBinContent(389,0.609756);
    scatteredSurfaceSpectrumRel__1->SetBinContent(390,0.615058);
    scatteredSurfaceSpectrumRel__1->SetBinContent(391,0.605292);
    scatteredSurfaceSpectrumRel__1->SetBinContent(392,0.581319);
    scatteredSurfaceSpectrumRel__1->SetBinContent(393,0.608035);
    scatteredSurfaceSpectrumRel__1->SetBinContent(394,0.592593);
    scatteredSurfaceSpectrumRel__1->SetBinContent(395,0.57458);
    scatteredSurfaceSpectrumRel__1->SetBinContent(396,0.59164);
    scatteredSurfaceSpectrumRel__1->SetBinContent(397,0.585567);
    scatteredSurfaceSpectrumRel__1->SetBinContent(398,0.576923);
    scatteredSurfaceSpectrumRel__1->SetBinContent(399,0.599797);
    scatteredSurfaceSpectrumRel__1->SetBinContent(400,0.579583);
    scatteredSurfaceSpectrumRel__1->SetBinContent(401,0.588295);
    scatteredSurfaceSpectrumRel__1->SetBinContent(402,0.623203);
    scatteredSurfaceSpectrumRel__1->SetBinContent(403,0.604301);
    scatteredSurfaceSpectrumRel__1->SetBinContent(404,0.567427);
    scatteredSurfaceSpectrumRel__1->SetBinContent(405,0.594771);
    scatteredSurfaceSpectrumRel__1->SetBinContent(406,0.572435);
    scatteredSurfaceSpectrumRel__1->SetBinContent(407,0.593348);
    scatteredSurfaceSpectrumRel__1->SetBinContent(408,0.57971);
    scatteredSurfaceSpectrumRel__1->SetBinContent(409,0.574093);
    scatteredSurfaceSpectrumRel__1->SetBinContent(410,0.57185);
    scatteredSurfaceSpectrumRel__1->SetBinContent(411,0.564885);
    scatteredSurfaceSpectrumRel__1->SetBinContent(412,0.56962);
    scatteredSurfaceSpectrumRel__1->SetBinContent(413,0.617713);
    scatteredSurfaceSpectrumRel__1->SetBinContent(414,0.568478);
    scatteredSurfaceSpectrumRel__1->SetBinContent(415,0.60334);
    scatteredSurfaceSpectrumRel__1->SetBinContent(416,0.563996);
    scatteredSurfaceSpectrumRel__1->SetBinContent(417,0.577703);
    scatteredSurfaceSpectrumRel__1->SetBinContent(418,0.561077);
    scatteredSurfaceSpectrumRel__1->SetBinContent(419,0.599604);
    scatteredSurfaceSpectrumRel__1->SetBinContent(420,0.568507);
    scatteredSurfaceSpectrumRel__1->SetBinContent(421,0.564);
    scatteredSurfaceSpectrumRel__1->SetBinContent(422,0.542253);
    scatteredSurfaceSpectrumRel__1->SetBinContent(423,0.545639);
    scatteredSurfaceSpectrumRel__1->SetBinContent(424,0.570194);
    scatteredSurfaceSpectrumRel__1->SetBinContent(425,0.561587);
    scatteredSurfaceSpectrumRel__1->SetBinContent(426,0.548596);
    scatteredSurfaceSpectrumRel__1->SetBinContent(427,0.531219);
    scatteredSurfaceSpectrumRel__1->SetBinContent(428,0.546403);
    scatteredSurfaceSpectrumRel__1->SetBinContent(429,0.532009);
    scatteredSurfaceSpectrumRel__1->SetBinContent(430,0.556923);
    scatteredSurfaceSpectrumRel__1->SetBinContent(431,0.557778);
    scatteredSurfaceSpectrumRel__1->SetBinContent(432,0.554705);
    scatteredSurfaceSpectrumRel__1->SetBinContent(433,0.566161);
    scatteredSurfaceSpectrumRel__1->SetBinContent(434,0.543158);
    scatteredSurfaceSpectrumRel__1->SetBinContent(435,0.554404);
    scatteredSurfaceSpectrumRel__1->SetBinContent(436,0.564378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(437,0.570122);
    scatteredSurfaceSpectrumRel__1->SetBinContent(438,0.55836);
    scatteredSurfaceSpectrumRel__1->SetBinContent(439,0.553015);
    scatteredSurfaceSpectrumRel__1->SetBinContent(440,0.539896);
    scatteredSurfaceSpectrumRel__1->SetBinContent(441,0.544634);
    scatteredSurfaceSpectrumRel__1->SetBinContent(442,0.547347);
    scatteredSurfaceSpectrumRel__1->SetBinContent(443,0.537988);
    scatteredSurfaceSpectrumRel__1->SetBinContent(444,0.531027);
    scatteredSurfaceSpectrumRel__1->SetBinContent(445,0.534884);
    scatteredSurfaceSpectrumRel__1->SetBinContent(446,0.567762);
    scatteredSurfaceSpectrumRel__1->SetBinContent(447,0.547548);
    scatteredSurfaceSpectrumRel__1->SetBinContent(448,0.557652);
    scatteredSurfaceSpectrumRel__1->SetBinContent(449,0.556522);
    scatteredSurfaceSpectrumRel__1->SetBinContent(450,0.552941);
    scatteredSurfaceSpectrumRel__1->SetBinContent(451,0.52381);
    scatteredSurfaceSpectrumRel__1->SetBinContent(452,0.518672);
    scatteredSurfaceSpectrumRel__1->SetBinContent(453,0.506849);
    scatteredSurfaceSpectrumRel__1->SetBinContent(454,0.504032);
    scatteredSurfaceSpectrumRel__1->SetBinContent(455,0.532481);
    scatteredSurfaceSpectrumRel__1->SetBinContent(456,0.540366);
    scatteredSurfaceSpectrumRel__1->SetBinContent(457,0.531313);
    scatteredSurfaceSpectrumRel__1->SetBinContent(458,0.533543);
    scatteredSurfaceSpectrumRel__1->SetBinContent(459,0.526639);
    scatteredSurfaceSpectrumRel__1->SetBinContent(460,0.526371);
    scatteredSurfaceSpectrumRel__1->SetBinContent(461,0.498378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(462,0.498452);
    scatteredSurfaceSpectrumRel__1->SetBinContent(463,0.539698);
    scatteredSurfaceSpectrumRel__1->SetBinContent(464,0.520124);
    scatteredSurfaceSpectrumRel__1->SetBinContent(465,0.509298);
    scatteredSurfaceSpectrumRel__1->SetBinContent(466,0.513889);
    scatteredSurfaceSpectrumRel__1->SetBinContent(467,0.509378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(468,0.488978);
    scatteredSurfaceSpectrumRel__1->SetBinContent(469,0.503408);
    scatteredSurfaceSpectrumRel__1->SetBinContent(470,0.541448);
    scatteredSurfaceSpectrumRel__1->SetBinContent(471,0.514811);
    scatteredSurfaceSpectrumRel__1->SetBinContent(472,0.516751);
    scatteredSurfaceSpectrumRel__1->SetBinContent(473,0.529889);
    scatteredSurfaceSpectrumRel__1->SetBinContent(474,0.532551);
    scatteredSurfaceSpectrumRel__1->SetBinContent(475,0.491604);
    scatteredSurfaceSpectrumRel__1->SetBinContent(476,0.501545);
    scatteredSurfaceSpectrumRel__1->SetBinContent(477,0.492795);
    scatteredSurfaceSpectrumRel__1->SetBinContent(478,0.485398);
    scatteredSurfaceSpectrumRel__1->SetBinContent(479,0.505528);
    scatteredSurfaceSpectrumRel__1->SetBinContent(480,0.490094);
    scatteredSurfaceSpectrumRel__1->SetBinContent(481,0.507804);
    scatteredSurfaceSpectrumRel__1->SetBinContent(482,0.491592);
    scatteredSurfaceSpectrumRel__1->SetBinContent(483,0.498522);
    scatteredSurfaceSpectrumRel__1->SetBinContent(484,0.461756);
    scatteredSurfaceSpectrumRel__1->SetBinContent(485,0.518974);
    scatteredSurfaceSpectrumRel__1->SetBinContent(486,0.465327);
    scatteredSurfaceSpectrumRel__1->SetBinContent(487,0.517602);
    scatteredSurfaceSpectrumRel__1->SetBinContent(488,0.500954);
    scatteredSurfaceSpectrumRel__1->SetBinContent(489,0.504951);
    scatteredSurfaceSpectrumRel__1->SetBinContent(490,0.490758);
    scatteredSurfaceSpectrumRel__1->SetBinContent(491,0.488024);
    scatteredSurfaceSpectrumRel__1->SetBinContent(492,0.475962);
    scatteredSurfaceSpectrumRel__1->SetBinContent(493,0.463391);
    scatteredSurfaceSpectrumRel__1->SetBinContent(494,0.471866);
    scatteredSurfaceSpectrumRel__1->SetBinContent(495,0.446429);
    scatteredSurfaceSpectrumRel__1->SetBinContent(496,0.486726);
    scatteredSurfaceSpectrumRel__1->SetBinContent(497,0.489275);
    scatteredSurfaceSpectrumRel__1->SetBinContent(498,0.497674);
    scatteredSurfaceSpectrumRel__1->SetBinContent(499,0.458824);
    scatteredSurfaceSpectrumRel__1->SetBinContent(500,0.472906);
    scatteredSurfaceSpectrumRel__1->SetBinContent(501,0.48599);
    scatteredSurfaceSpectrumRel__1->SetBinContent(502,0.478615);
    scatteredSurfaceSpectrumRel__1->SetBinContent(503,0.482824);
    scatteredSurfaceSpectrumRel__1->SetBinContent(504,0.463938);
    scatteredSurfaceSpectrumRel__1->SetBinContent(505,0.474045);
    scatteredSurfaceSpectrumRel__1->SetBinContent(506,0.457407);
    scatteredSurfaceSpectrumRel__1->SetBinContent(507,0.478095);
    scatteredSurfaceSpectrumRel__1->SetBinContent(508,0.452775);
    scatteredSurfaceSpectrumRel__1->SetBinContent(509,0.431018);
    scatteredSurfaceSpectrumRel__1->SetBinContent(510,0.436911);
    scatteredSurfaceSpectrumRel__1->SetBinContent(511,0.479602);
    scatteredSurfaceSpectrumRel__1->SetBinContent(512,0.446911);
    scatteredSurfaceSpectrumRel__1->SetBinContent(513,0.443526);
    scatteredSurfaceSpectrumRel__1->SetBinContent(514,0.442922);
    scatteredSurfaceSpectrumRel__1->SetBinContent(515,0.444004);
    scatteredSurfaceSpectrumRel__1->SetBinContent(516,0.451705);
    scatteredSurfaceSpectrumRel__1->SetBinContent(517,0.453153);
    scatteredSurfaceSpectrumRel__1->SetBinContent(518,0.436813);
    scatteredSurfaceSpectrumRel__1->SetBinContent(519,0.453096);
    scatteredSurfaceSpectrumRel__1->SetBinContent(520,0.435516);
    scatteredSurfaceSpectrumRel__1->SetBinContent(521,0.44247);
    scatteredSurfaceSpectrumRel__1->SetBinContent(522,0.447573);
    scatteredSurfaceSpectrumRel__1->SetBinContent(523,0.430087);
    scatteredSurfaceSpectrumRel__1->SetBinContent(524,0.449584);
    scatteredSurfaceSpectrumRel__1->SetBinContent(525,0.431881);
    scatteredSurfaceSpectrumRel__1->SetBinContent(526,0.436293);
    scatteredSurfaceSpectrumRel__1->SetBinContent(527,0.427112);
    scatteredSurfaceSpectrumRel__1->SetBinContent(528,0.41327);
    scatteredSurfaceSpectrumRel__1->SetBinContent(529,0.425656);
    scatteredSurfaceSpectrumRel__1->SetBinContent(530,0.410796);
    scatteredSurfaceSpectrumRel__1->SetBinContent(531,0.436297);
    scatteredSurfaceSpectrumRel__1->SetBinContent(532,0.44131);
    scatteredSurfaceSpectrumRel__1->SetBinContent(533,0.410086);
    scatteredSurfaceSpectrumRel__1->SetBinContent(534,0.404216);
    scatteredSurfaceSpectrumRel__1->SetBinContent(535,0.41953);
    scatteredSurfaceSpectrumRel__1->SetBinContent(536,0.415814);
    scatteredSurfaceSpectrumRel__1->SetBinContent(537,0.41115);
    scatteredSurfaceSpectrumRel__1->SetBinContent(538,0.419325);
    scatteredSurfaceSpectrumRel__1->SetBinContent(539,0.405429);
    scatteredSurfaceSpectrumRel__1->SetBinContent(540,0.428054);
    scatteredSurfaceSpectrumRel__1->SetBinContent(541,0.399461);
    scatteredSurfaceSpectrumRel__1->SetBinContent(542,0.429384);
    scatteredSurfaceSpectrumRel__1->SetBinContent(543,0.402089);
    scatteredSurfaceSpectrumRel__1->SetBinContent(544,0.395083);
    scatteredSurfaceSpectrumRel__1->SetBinContent(545,0.391697);
    scatteredSurfaceSpectrumRel__1->SetBinContent(546,0.415159);
    scatteredSurfaceSpectrumRel__1->SetBinContent(547,0.445217);
    scatteredSurfaceSpectrumRel__1->SetBinContent(548,0.388938);
    scatteredSurfaceSpectrumRel__1->SetBinContent(549,0.414286);
    scatteredSurfaceSpectrumRel__1->SetBinContent(550,0.402174);
    scatteredSurfaceSpectrumRel__1->SetBinContent(551,0.396022);
    scatteredSurfaceSpectrumRel__1->SetBinContent(552,0.402908);
    scatteredSurfaceSpectrumRel__1->SetBinContent(553,0.40066);
    scatteredSurfaceSpectrumRel__1->SetBinContent(554,0.417582);
    scatteredSurfaceSpectrumRel__1->SetBinContent(555,0.432684);
    scatteredSurfaceSpectrumRel__1->SetBinContent(556,0.394198);
    scatteredSurfaceSpectrumRel__1->SetBinContent(557,0.384808);
    scatteredSurfaceSpectrumRel__1->SetBinContent(558,0.364566);
    scatteredSurfaceSpectrumRel__1->SetBinContent(559,0.4131);
    scatteredSurfaceSpectrumRel__1->SetBinContent(560,0.390365);
    scatteredSurfaceSpectrumRel__1->SetBinContent(561,0.409821);
    scatteredSurfaceSpectrumRel__1->SetBinContent(562,0.386042);
    scatteredSurfaceSpectrumRel__1->SetBinContent(563,0.390347);
    scatteredSurfaceSpectrumRel__1->SetBinContent(564,0.409091);
    scatteredSurfaceSpectrumRel__1->SetBinContent(565,0.37946);
    scatteredSurfaceSpectrumRel__1->SetBinContent(566,0.395887);
    scatteredSurfaceSpectrumRel__1->SetBinContent(567,0.355298);
    scatteredSurfaceSpectrumRel__1->SetBinContent(568,0.386326);
    scatteredSurfaceSpectrumRel__1->SetBinContent(569,0.389687);
    scatteredSurfaceSpectrumRel__1->SetBinContent(570,0.378092);
    scatteredSurfaceSpectrumRel__1->SetBinContent(571,0.371921);
    scatteredSurfaceSpectrumRel__1->SetBinContent(572,0.380913);
    scatteredSurfaceSpectrumRel__1->SetBinContent(573,0.374188);
    scatteredSurfaceSpectrumRel__1->SetBinContent(574,0.382114);
    scatteredSurfaceSpectrumRel__1->SetBinContent(575,0.36407);
    scatteredSurfaceSpectrumRel__1->SetBinContent(576,0.363793);
    scatteredSurfaceSpectrumRel__1->SetBinContent(577,0.384679);
    scatteredSurfaceSpectrumRel__1->SetBinContent(578,0.371074);
    scatteredSurfaceSpectrumRel__1->SetBinContent(579,0.3426);
    scatteredSurfaceSpectrumRel__1->SetBinContent(580,0.359055);
    scatteredSurfaceSpectrumRel__1->SetBinContent(581,0.368006);
    scatteredSurfaceSpectrumRel__1->SetBinContent(582,0.380727);
    scatteredSurfaceSpectrumRel__1->SetBinContent(583,0.374606);
    scatteredSurfaceSpectrumRel__1->SetBinContent(584,0.358566);
    scatteredSurfaceSpectrumRel__1->SetBinContent(585,0.379608);
    scatteredSurfaceSpectrumRel__1->SetBinContent(586,0.36872);
    scatteredSurfaceSpectrumRel__1->SetBinContent(587,0.361474);
    scatteredSurfaceSpectrumRel__1->SetBinContent(588,0.375101);
    scatteredSurfaceSpectrumRel__1->SetBinContent(589,0.366951);
    scatteredSurfaceSpectrumRel__1->SetBinContent(590,0.369102);
    scatteredSurfaceSpectrumRel__1->SetBinContent(591,0.334866);
    scatteredSurfaceSpectrumRel__1->SetBinContent(592,0.368421);
    scatteredSurfaceSpectrumRel__1->SetBinContent(593,0.350524);
    scatteredSurfaceSpectrumRel__1->SetBinContent(594,0.333825);
    scatteredSurfaceSpectrumRel__1->SetBinContent(595,0.330269);
    scatteredSurfaceSpectrumRel__1->SetBinContent(596,0.329386);
    scatteredSurfaceSpectrumRel__1->SetBinContent(597,0.348638);
    scatteredSurfaceSpectrumRel__1->SetBinContent(598,0.319383);
    scatteredSurfaceSpectrumRel__1->SetBinContent(599,0.364561);
    scatteredSurfaceSpectrumRel__1->SetBinContent(600,0.366881);
    scatteredSurfaceSpectrumRel__1->SetBinContent(601,0.337455);
    scatteredSurfaceSpectrumRel__1->SetBinContent(602,0.348604);
    scatteredSurfaceSpectrumRel__1->SetBinContent(603,0.326152);
    scatteredSurfaceSpectrumRel__1->SetBinContent(604,0.334737);
    scatteredSurfaceSpectrumRel__1->SetBinContent(605,0.335584);
    scatteredSurfaceSpectrumRel__1->SetBinContent(606,0.333103);
    scatteredSurfaceSpectrumRel__1->SetBinContent(607,0.34094);
    scatteredSurfaceSpectrumRel__1->SetBinContent(608,0.304408);
    scatteredSurfaceSpectrumRel__1->SetBinContent(609,0.324718);
    scatteredSurfaceSpectrumRel__1->SetBinContent(610,0.315646);
    scatteredSurfaceSpectrumRel__1->SetBinContent(611,0.310956);
    scatteredSurfaceSpectrumRel__1->SetBinContent(612,0.325111);
    scatteredSurfaceSpectrumRel__1->SetBinContent(613,0.304483);
    scatteredSurfaceSpectrumRel__1->SetBinContent(614,0.30829);
    scatteredSurfaceSpectrumRel__1->SetBinContent(615,0.315068);
    scatteredSurfaceSpectrumRel__1->SetBinContent(616,0.311309);
    scatteredSurfaceSpectrumRel__1->SetBinContent(617,0.296165);
    scatteredSurfaceSpectrumRel__1->SetBinContent(618,0.309885);
    scatteredSurfaceSpectrumRel__1->SetBinContent(619,0.298709);
    scatteredSurfaceSpectrumRel__1->SetBinContent(620,0.286393);
    scatteredSurfaceSpectrumRel__1->SetBinContent(621,0.302243);
    scatteredSurfaceSpectrumRel__1->SetBinContent(622,0.273428);
    scatteredSurfaceSpectrumRel__1->SetBinContent(623,0.320208);
    scatteredSurfaceSpectrumRel__1->SetBinContent(624,0.295517);
    scatteredSurfaceSpectrumRel__1->SetBinContent(625,0.292811);
    scatteredSurfaceSpectrumRel__1->SetBinContent(626,0.31621);
    scatteredSurfaceSpectrumRel__1->SetBinContent(627,0.288839);
    scatteredSurfaceSpectrumRel__1->SetBinContent(628,0.284516);
    scatteredSurfaceSpectrumRel__1->SetBinContent(629,0.296154);
    scatteredSurfaceSpectrumRel__1->SetBinContent(630,0.280587);
    scatteredSurfaceSpectrumRel__1->SetBinContent(631,0.295);
    scatteredSurfaceSpectrumRel__1->SetBinContent(632,0.287454);
    scatteredSurfaceSpectrumRel__1->SetBinContent(633,0.30871);
    scatteredSurfaceSpectrumRel__1->SetBinContent(634,0.300623);
    scatteredSurfaceSpectrumRel__1->SetBinContent(635,0.291839);
    scatteredSurfaceSpectrumRel__1->SetBinContent(636,0.292556);
    scatteredSurfaceSpectrumRel__1->SetBinContent(637,0.270062);
    scatteredSurfaceSpectrumRel__1->SetBinContent(638,0.285079);
    scatteredSurfaceSpectrumRel__1->SetBinContent(639,0.254921);
    scatteredSurfaceSpectrumRel__1->SetBinContent(640,0.291859);
    scatteredSurfaceSpectrumRel__1->SetBinContent(641,0.290892);
    scatteredSurfaceSpectrumRel__1->SetBinContent(642,0.284304);
    scatteredSurfaceSpectrumRel__1->SetBinContent(643,0.26179);
    scatteredSurfaceSpectrumRel__1->SetBinContent(644,0.274596);
    scatteredSurfaceSpectrumRel__1->SetBinContent(645,0.287843);
    scatteredSurfaceSpectrumRel__1->SetBinContent(646,0.27281);
    scatteredSurfaceSpectrumRel__1->SetBinContent(647,0.278378);
    scatteredSurfaceSpectrumRel__1->SetBinContent(648,0.276673);
    scatteredSurfaceSpectrumRel__1->SetBinContent(649,0.273665);
    scatteredSurfaceSpectrumRel__1->SetBinContent(650,0.268152);
    scatteredSurfaceSpectrumRel__1->SetBinContent(651,0.252525);
    scatteredSurfaceSpectrumRel__1->SetBinContent(652,0.256209);
    scatteredSurfaceSpectrumRel__1->SetBinContent(653,0.276623);
    scatteredSurfaceSpectrumRel__1->SetBinContent(654,0.3018);
    scatteredSurfaceSpectrumRel__1->SetBinContent(655,0.297184);
    scatteredSurfaceSpectrumRel__1->SetBinContent(656,0.275192);
    scatteredSurfaceSpectrumRel__1->SetBinContent(657,0.27379);
    scatteredSurfaceSpectrumRel__1->SetBinContent(658,0.272227);
    scatteredSurfaceSpectrumRel__1->SetBinContent(659,0.23893);
    scatteredSurfaceSpectrumRel__1->SetBinContent(660,0.243004);
    scatteredSurfaceSpectrumRel__1->SetBinContent(661,0.240018);
    scatteredSurfaceSpectrumRel__1->SetBinContent(662,0.217215);
    scatteredSurfaceSpectrumRel__1->SetBinContent(663,0.234929);
    scatteredSurfaceSpectrumRel__1->SetBinContent(664,0.216592);
    scatteredSurfaceSpectrumRel__1->SetBinContent(665,0.234973);
    scatteredSurfaceSpectrumRel__1->SetBinContent(666,0.225394);
    scatteredSurfaceSpectrumRel__1->SetBinContent(667,0.198533);
    scatteredSurfaceSpectrumRel__1->SetBinContent(668,0.21464);
    scatteredSurfaceSpectrumRel__1->SetBinContent(669,0.209087);
    scatteredSurfaceSpectrumRel__1->SetBinContent(670,0.221858);
    scatteredSurfaceSpectrumRel__1->SetBinContent(671,0.195579);
    scatteredSurfaceSpectrumRel__1->SetBinContent(672,0.192387);
    scatteredSurfaceSpectrumRel__1->SetBinContent(673,0.195114);
    scatteredSurfaceSpectrumRel__1->SetBinContent(674,0.197334);
    scatteredSurfaceSpectrumRel__1->SetBinContent(675,0.193118);
    scatteredSurfaceSpectrumRel__1->SetBinContent(676,0.195066);
    scatteredSurfaceSpectrumRel__1->SetBinContent(677,0.190052);
    scatteredSurfaceSpectrumRel__1->SetBinContent(678,0.199577);
    scatteredSurfaceSpectrumRel__1->SetBinContent(679,0.19936);
    scatteredSurfaceSpectrumRel__1->SetBinContent(680,0.205671);
    scatteredSurfaceSpectrumRel__1->SetBinContent(681,0.195358);
    scatteredSurfaceSpectrumRel__1->SetBinContent(682,0.192771);
    scatteredSurfaceSpectrumRel__1->SetBinContent(683,0.202484);
    scatteredSurfaceSpectrumRel__1->SetBinContent(684,0.213908);
    scatteredSurfaceSpectrumRel__1->SetBinContent(685,0.212351);
    scatteredSurfaceSpectrumRel__1->SetBinContent(686,0.204789);
    scatteredSurfaceSpectrumRel__1->SetBinContent(687,0.187067);
    scatteredSurfaceSpectrumRel__1->SetBinContent(688,0.181794);
    scatteredSurfaceSpectrumRel__1->SetBinContent(689,0.174537);
    scatteredSurfaceSpectrumRel__1->SetBinContent(690,0.156401);
    scatteredSurfaceSpectrumRel__1->SetBinContent(691,0.151847);
    scatteredSurfaceSpectrumRel__1->SetBinContent(692,0.156441);
    scatteredSurfaceSpectrumRel__1->SetBinContent(693,0.157515);
    scatteredSurfaceSpectrumRel__1->SetBinContent(694,0.15027);
    scatteredSurfaceSpectrumRel__1->SetBinContent(695,0.136364);
    scatteredSurfaceSpectrumRel__1->SetBinContent(696,0.133009);
    scatteredSurfaceSpectrumRel__1->SetBinContent(697,0.14517);
    scatteredSurfaceSpectrumRel__1->SetBinContent(698,0.141463);
    scatteredSurfaceSpectrumRel__1->SetBinContent(699,0.150013);
    scatteredSurfaceSpectrumRel__1->SetBinContent(700,0.135307);
    scatteredSurfaceSpectrumRel__1->SetBinContent(701,0.134062);
    scatteredSurfaceSpectrumRel__1->SetBinContent(702,0.127203);
    scatteredSurfaceSpectrumRel__1->SetBinContent(703,0.142061);
    scatteredSurfaceSpectrumRel__1->SetBinContent(704,0.1429);
    scatteredSurfaceSpectrumRel__1->SetBinContent(705,0.133466);
    scatteredSurfaceSpectrumRel__1->SetBinContent(706,0.124303);
    scatteredSurfaceSpectrumRel__1->SetBinContent(707,0.122029);
    scatteredSurfaceSpectrumRel__1->SetBinContent(708,0.119284);
    scatteredSurfaceSpectrumRel__1->SetBinContent(709,0.118398);
    scatteredSurfaceSpectrumRel__1->SetBinContent(710,0.118908);
    scatteredSurfaceSpectrumRel__1->SetBinContent(711,0.117553);
    scatteredSurfaceSpectrumRel__1->SetBinContent(712,0.128425);
    scatteredSurfaceSpectrumRel__1->SetBinContent(713,0.106745);
    scatteredSurfaceSpectrumRel__1->SetBinContent(714,0.0907042);
    scatteredSurfaceSpectrumRel__1->SetBinContent(715,0.0900725);
    scatteredSurfaceSpectrumRel__1->SetBinContent(716,0.0968002);
    scatteredSurfaceSpectrumRel__1->SetBinContent(717,0.106645);
    scatteredSurfaceSpectrumRel__1->SetBinContent(718,0.100865);
    scatteredSurfaceSpectrumRel__1->SetBinContent(719,0.1025);
    scatteredSurfaceSpectrumRel__1->SetBinContent(720,0.0945589);
    scatteredSurfaceSpectrumRel__1->SetBinContent(721,0.107738);
    scatteredSurfaceSpectrumRel__1->SetBinContent(722,0.115463);
    scatteredSurfaceSpectrumRel__1->SetBinContent(723,0.104568);
    scatteredSurfaceSpectrumRel__1->SetBinContent(724,0.118677);
    scatteredSurfaceSpectrumRel__1->SetBinContent(725,0.123153);
    scatteredSurfaceSpectrumRel__1->SetBinContent(726,0.136412);
    scatteredSurfaceSpectrumRel__1->SetBinContent(727,0.116671);
    scatteredSurfaceSpectrumRel__1->SetBinContent(728,0.100058);
    scatteredSurfaceSpectrumRel__1->SetBinContent(729,0.129568);
    scatteredSurfaceSpectrumRel__1->SetBinContent(730,0.108321);
    scatteredSurfaceSpectrumRel__1->SetBinContent(731,0.10985);
    scatteredSurfaceSpectrumRel__1->SetBinContent(732,0.09129);
    scatteredSurfaceSpectrumRel__1->SetBinContent(733,0.101913);
    scatteredSurfaceSpectrumRel__1->SetBinContent(734,0.0872483);
    scatteredSurfaceSpectrumRel__1->SetBinContent(735,0.0868102);
    scatteredSurfaceSpectrumRel__1->SetBinContent(736,0.0823328);
    scatteredSurfaceSpectrumRel__1->SetBinContent(737,0.0770975);
    scatteredSurfaceSpectrumRel__1->SetBinContent(738,0.0866228);
    scatteredSurfaceSpectrumRel__1->SetBinContent(739,0.0911206);
    scatteredSurfaceSpectrumRel__1->SetBinContent(740,0.0944255);
    scatteredSurfaceSpectrumRel__1->SetBinContent(741,0.0988927);
    scatteredSurfaceSpectrumRel__1->SetBinContent(742,0.0855089);
    scatteredSurfaceSpectrumRel__1->SetBinContent(743,0.0786212);
    scatteredSurfaceSpectrumRel__1->SetBinContent(744,0.0778488);
    scatteredSurfaceSpectrumRel__1->SetBinContent(745,0.0705568);
    scatteredSurfaceSpectrumRel__1->SetBinContent(746,0.0705318);
    scatteredSurfaceSpectrumRel__1->SetBinContent(747,0.080083);
    scatteredSurfaceSpectrumRel__1->SetBinContent(748,0.0684597);
    scatteredSurfaceSpectrumRel__1->SetBinContent(749,0.0762852);
    scatteredSurfaceSpectrumRel__1->SetBinContent(750,0.0839161);
    scatteredSurfaceSpectrumRel__1->SetBinContent(751,0.0826621);
    scatteredSurfaceSpectrumRel__1->SetBinContent(752,0.0797984);
    scatteredSurfaceSpectrumRel__1->SetBinContent(753,0.0477454);
    scatteredSurfaceSpectrumRel__1->SetBinContent(754,0.0445469);
    scatteredSurfaceSpectrumRel__1->SetBinContent(755,0.0406053);
    scatteredSurfaceSpectrumRel__1->SetBinContent(756,0.0394089);
    scatteredSurfaceSpectrumRel__1->SetBinContent(757,0.0338228);
    scatteredSurfaceSpectrumRel__1->SetBinContent(758,0.034733);
    scatteredSurfaceSpectrumRel__1->SetBinContent(759,0.0304);
    scatteredSurfaceSpectrumRel__1->SetBinContent(760,0.0312239);
    scatteredSurfaceSpectrumRel__1->SetBinContent(761,0.0274334);
    scatteredSurfaceSpectrumRel__1->SetBinContent(762,0.0309623);
    scatteredSurfaceSpectrumRel__1->SetBinContent(763,0.0301318);
    scatteredSurfaceSpectrumRel__1->SetBinContent(764,0.0393289);
    scatteredSurfaceSpectrumRel__1->SetBinContent(765,0.0364395);
    scatteredSurfaceSpectrumRel__1->SetBinContent(766,0.036031);
    scatteredSurfaceSpectrumRel__1->SetBinContent(767,0.0341981);
    scatteredSurfaceSpectrumRel__1->SetBinContent(768,0.0298552);
    scatteredSurfaceSpectrumRel__1->SetBinContent(769,0.0223942);
    scatteredSurfaceSpectrumRel__1->SetBinContent(770,0.0219914);
    scatteredSurfaceSpectrumRel__1->SetBinContent(771,0.0222222);
    scatteredSurfaceSpectrumRel__1->SetBinContent(772,0.0216541);
    scatteredSurfaceSpectrumRel__1->SetBinContent(773,0.0186446);
    scatteredSurfaceSpectrumRel__1->SetBinContent(774,0.0182944);
    scatteredSurfaceSpectrumRel__1->SetBinContent(775,0.0141908);
    scatteredSurfaceSpectrumRel__1->SetBinContent(776,0.0186858);
    scatteredSurfaceSpectrumRel__1->SetBinContent(777,0.0179602);
    scatteredSurfaceSpectrumRel__1->SetBinContent(778,0.0159803);
    scatteredSurfaceSpectrumRel__1->SetBinContent(779,0.0134796);
    scatteredSurfaceSpectrumRel__1->SetBinContent(780,0.0133411);
    scatteredSurfaceSpectrumRel__1->SetBinContent(781,0.0146575);
    scatteredSurfaceSpectrumRel__1->SetBinContent(782,0.0127294);
    scatteredSurfaceSpectrumRel__1->SetBinContent(783,0.0148546);
    scatteredSurfaceSpectrumRel__1->SetBinContent(784,0.0167398);
    scatteredSurfaceSpectrumRel__1->SetBinContent(785,0.00750075);
    scatteredSurfaceSpectrumRel__1->SetBinContent(786,0.00853138);
    scatteredSurfaceSpectrumRel__1->SetBinContent(787,0.00590884);
    scatteredSurfaceSpectrumRel__1->SetBinContent(788,0.00406504);
    scatteredSurfaceSpectrumRel__1->SetBinContent(789,0.00305386);
    scatteredSurfaceSpectrumRel__1->SetBinContent(790,0.00281452);
    scatteredSurfaceSpectrumRel__1->SetBinContent(791,0.000270197);
    scatteredSurfaceSpectrumRel__1->SetBinContent(793,0.000552944);
    scatteredSurfaceSpectrumRel__1->SetBinContent(830,0.000166778);
    scatteredSurfaceSpectrumRel__1->SetBinContent(834,0.000160359);
    scatteredSurfaceSpectrumRel__1->SetBarOffset(-0.3);
    scatteredSurfaceSpectrumRel__1->SetEntries(996);

    return scatteredSurfaceSpectrumRel__1;
}


