# URANOS Release Notes


##  URANOS version 1.27 (October 10, 2024)

## Changelog:
- added option for adding soil organic carbon to soils
- added option for using "default" as the incomingSpectrum file in order to bypass specifying the .root-file location
- Bugfix for oxygen absorption cross section in cellulose


##  URANOS version 1.26 (June 23, 2024)

## Changelog:
- added option for creating a ptrac file from the virtual detector
- Bugfix for virtual sphere detector (not occasionally counting double hits)


## URANOS version 1.25 (March 18, 2024)

## Changelog:
- Added material: contaminated plant mixture (with boron content from settings for soil #18): material layer code 42, voxel code 249
- Bugfix for island preset
- Bugfix for B11 cross section
- Bugfix for angle setting in detectorBatchRun2


## URANOS version 1.24 (March 3, 2024)

## Changelog:
- Added option for exponential scaling of air humidity in the air column


## URANOS version 1.23 (February 7, 2024)

## Changelog:
- Bugfix for absorption cross section calculation in soil


## URANOS version 1.21 (January 21, 2024)

## Changelog:
- Issue warning for incompatible ROOT version
- replace deprecated file system access methods


## URANOS version 1.20 (January 21, 2024)

## Changelog:
- URANOS 64bit version released for Windows with ROOT 6.30.02+
- bugfix in tracking unit and ENDF conversion unit for angular coefficients
- material added

## URANOS version 1.19b (December 25, 2023)

## Changelog:
- Due to security vulnerabilities in ROOT 6.26. - 6.28 URANOS was migrated to 6.30.02
- URANOS 64bit version released for Windows requires now ROOT 6.30.02+


## URANOS version 1.19 (November 6, 2023)

## Changelog:
- URANOS 64bit version released for Windows. This version requires now ROOT 6.28.06+
- code restructuring for updated environment
- added command line parameter for manual file name scheme.
- added higher resolved output data options


## URANOS version 1.16 (August 07, 2023)

## Changelog:
- CLI parameter adaptations


## URANOS version 1.15 (June 25, 2023)

## Changelog:
- fixed issue #24
- added option to enable or disable the angular response function when using the physics model


## URANOS version 1.14 (May 17, 2023)

## Changelog:
- minor code restructuring
- bugfix for scoring if the vertical cylinder virtual detector is off-center


## URANOS version 1.13 (May 3, 2023)

## Changelog:
- improved labels in uranos.cfg
- added option to manually change the seed of the random generator in the uranos.cfg
- minor fixes in the GUI
- fixed high res output for command line


## URANOS version 1.12 (April 30, 2023)

## Changelog:
- option for changing the thermal sensitivity to the response function of a bare counter (in Detector tab)


## URANOS version 1.12alpha (April 6, 2023)

## Changelog:
- update for Nix builds
- added high resolution track mode for 1500 pixels


## URANOS version 1.11 (March 12, 2023)

## Changelog:
- code restructuring for URANOS builds and various cleanups
- small GUI adjustments
- added plattform information in uranos.cfg
- fixed issue #15
- ENDF data base updates
- please also have a look at the toolset for running URANOS using batch jobs (on clusters for example): https://github.com/mkoehli/uranos/tree/main/tools


## URANOS version 1.10 (February 27, 2023)

## Changelog:
- fixed porosity declaration in uranos.cfg
- fixed issues #12, #13, #14


## URANOS version 1.09 (February 11, 2023)

## Changelog:
- bugfixes for layer control and high res track output
- fixed issues #9, #10, #11


## URANOS version 1.08 (February 8, 2023)

## Changelog:
- use command line parameter "tnw" to activate the custom source configuration panel
- (hidden option, prototype) Additional detector layers: use material code to 500 + material code to declare the respective layer also as detector layer
- (hidden option, tnw mode): in the enlarged view ("+" symbol) the source can be place by right clicking into the view area 


## URANOS version 1.07 (January 24, 2023)

## Changelog:
- bugfixes for GUI and computation module
- bugfixes for the cross sections


## URANOS version 1.06 (January 17, 2023)
- temporarily withdrawn - 
### Changelog
- bugfixes for the cross sections
- improved performance


## URANOS version 1.05b (January 14, 2023)

### Changelog
- bugfixes
- improved performance for the GUI data display


## URANOS version 1.05 (January 4, 2023)

### Changelog
- bugfixes
- added material: 231 = Polyvinyl chloride (1.45 g/cm^3)


## URANOS version 1.04 (January 2, 2023)

### Changelog
- additional high resolution track output added
- URANOS CentOS 7 64bit version release with QT 5.9.7 and ROOT 6.22.08.


## URANOS version 1.03 (December 30, 2022)

### Changelog
- URANOS Linux version release. 
- URANOS has currently been tested on Ubuntu 20 64bit with QT 5.12.8 and ROOT 6.22.08.
- Compiled binaries are now available for Ubuntu 20. For running under Linux, please also update the ENDF data.


## URANOS version 1.02 (December 26, 2022)

### Changelog
- bugfixes for command line options


## URANOS version 1.01 (December 5, 2022)

### Changelog
- smaller bugfixes
- updated QCustomPlot to version 2.1.1


## URANOS version 1.00 beta (September 22, 2022)

### Changelog
- bugfixes for material settings


## URANOS version 1.00a (May 24, 2022)

### New installation guidelines

Changed the program architecture to more recent versions of compilers and dependencies, also to prepare the Linux release.
URANOS now requires:
- Visual Studio 2019 community. Install with the development option "C++
application" (approx. 7 GB). Unfortunately, this installation is required to
run ROOT 6. [Link](https://my.visualstudio.com/Downloads?q=visual%20studio%202019&wt.mc_id=o~msft~vscom~older-downloads)
- ROOT (Installer provided on the website, approx. 100 MB) 6.22.08 (a virus
scan warning might appear here). [Link](https://root.cern/download/root_v6.22.08.win32.vc16.exe)
(In case you do not need to run older versions of URANOS it is recommended
to uninstall ROOT before installing the new version)

### Backward-incompatible changes:
- Due to the change in the high energy model, the intensity might scale differently in absolute numbers compared to v0.99.

### GUI improvements:
- "Save Configuration" button added.
