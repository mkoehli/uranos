# Building URANOS

## Linux

First, clone the URANOS repository and `cd` into it:
```console
$ git clone https://github.com/mkoehli/uranos
$ cd uranos
```

### Building with Nix
Currently, the easiest way to build URANOS from scratch is via the provided Nix derivations. Nix can be installed on almost any Linux distribution with the following command:
```console
$ sh <(curl -L https://nixos.org/nix/install) --daemon
```
For more information on Nix, see [https://nixos.org](https://nixos.org).

Once you have Nix installed, run:
```console
$ nix-build
```
Nix will automatically fetch all dependencies for you. The resulting program will be located at `result/bin/uranos-gui`.

To install URANOS on your machine, run:
```console
$ nix-env --install -f default.nix
```

#### Development shell

If you want to modify the source code, run
```console
$ nix-shell
```

to open an interactive shell with all required dependencies. Then, you can build URANOS with
```console
$ qmake
$ make
```

The resulting executable will be at `build/uranos-gui`.

### Without Nix
If you do not want to use Nix, you will need to manually install the following dependencies:

* [Qt 5](https://qt.io)
* [ROOT](https://root.cern.ch/)
* libtbb libxxhash (as -dev, probably available through your distribution package manager)

* copy the ROOT folder /lib /include and /etc into a folder /ROOT into the main URANOS directory where the .pro file is located

To build URANOS, run the following commands inside the URANOS source tree:
```console
$ qmake
$ make
```
The resulting programm will be at `build/uranos-gui`.

To install URANOS, run:
```console
$ sudo make install
```

## Windows

(work in progress)
