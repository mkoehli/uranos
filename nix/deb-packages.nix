{ stdenv
, lib
, releaseTools
, vmTools
, rootPackagesNewest
, src
}:

let
  extraPackages = dist: [
    "cmake"
    # ROOT deps
    "dpkg-dev" "libx11-dev" "libxpm-dev" "libxft-dev" "libxext-dev" "python3" "libssl-dev"
  ] ++ (
    if dist == "ubuntu2204x86_64"
    then [ "qtbase5-dev" "qt5-qmake" "cmake" ]
    else [ "qt5-default" ]
  );
  debBuild = args: import overrides/debian-build.nix (
  { inherit lib stdenv vmTools;
  } // args);
in
  lib.genAttrs
  [
    "ubuntu2004x86_64"
    "ubuntu2204x86_64"
  ]
  (dist:
    debBuild {
      diskImage = vmTools.diskImageFuns."${dist}" {
        extraPackages = (extraPackages dist) ++ [ "checkinstall" ];
      };
      diskImageFormat = "qcow2";
      memSize = 4096;
      inherit src;
      name = "uranos-deb";
      debName = "uranos";
      debMaintainer = "Markus Koehli <koehli@physi.uni-heidelberg.de>, Alex Hollmeier <hollmeier@physi.uni-heidelberg.de>";
      debRequires = extraPackages dist;
      buildInputs = [];
      buildPhase = ''
        ln -s ${rootPackagesNewest dist} root
        cp src/ForLinux/mainwindow.ui src/
        qmake .
        make
      '';
    }
  )
