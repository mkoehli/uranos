# Documentation

This folder contains:
- [Release notes](https://gitlab.com/mkoehli/uranos/-/tree/main/doc/RELEASES.md), and
- [List of publications using URANOS](https://gitlab.com/mkoehli/uranos/-/tree/main/doc/PUBLICATIONS.md).

Detailed documentation can be found in the [URANOS Wiki](https://gitlab.com/mkoehli/uranos/-/wikis/home).
