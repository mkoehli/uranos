# Binaries

| file                | OS              | requires                |
|:------------------- |:--------------- |:----------------------- |
| `URANOS*`           | Windows         | ROOT 6.22.08            |
| `URANOS64bit`       | Windows         | ROOT 6.30.02            |
| `URANOS-Ubuntu20-*` | Linux/Ubuntu 20 | ROOT 6.30.02, QT 5.14.2 |
| `URANOS-Ubuntu22-*` | Linux/Ubuntu 22 | ROOT 6.30.02, QT 5.15.3 |
| `URANOS-Ubuntu23-*` | Linux/Ubuntu 23 | ROOT 6.30.02, QT 5.14.2 |
| `URANOS-CentOS7-*`  | Linux/CentOS 7  | ROOT 6.22.08, QT 5.9.7, QT 5.13.1  | 
