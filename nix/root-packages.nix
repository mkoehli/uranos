final: prev: {
  rootPackages = with final; {
    "6.28" = {
      ubuntu1804x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.28.00.Linux-ubuntu18-x86_64-gcc7.5.tar.gz";
        sha256 = "sha256-c0s2rNxMobMZb4LbWlnVGKA5xesXnUPvvd3LjJYFHy0=";
      };

      ubuntu2004x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.28.00.Linux-ubuntu20-x86_64-gcc9.4.tar.gz";
        sha256 = "sha256-2/QEPPei0uDaIElnHb0uDUC1QYc6CYdK7rEpkgo1bAg=";
      };

      ubuntu2204x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.28.00.Linux-ubuntu22-x86_64-gcc11.3.tar.gz";
        sha256 = "sha256-4kpeyLnWYhoWVHTNK4//HSS67NRR6JL7xmt+PqxaZno=";
      };

      centos8x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.28.00.Linux-centos8-x86_64-gcc8.5.tar.gz";
        sha256 = "sha256-vjVEbrw9Gth0t3zjFeUGPI+ZV99VwpXBIzeYKQ/nDI0=";
      };
    };

    "6.26" = {
      ubuntu1804x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.26.10.Linux-ubuntu18-x86_64-gcc7.5.tar.gz";
        sha256 = "sha256-myL9XGcZ37QIXuDM3CT0nvdAIuiC60ZK/A2lrWucL6s=";
      };

      ubuntu2004x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.26.10.Linux-ubuntu20-x86_64-gcc9.4.tar.gz";
        sha256 = "sha256-AF2TWcnx4wUxtthnVkGhcP4BZVYIyvh/0dGFAob9QtE=";
      };

      ubuntu2204x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.26.10.Linux-ubuntu22-x86_64-gcc11.3.tar.gz";
        sha256 = "sha256-XQniF3ycF7WBYcYSerFH7t32yV+oqapBksoSWBo9Ais=";
      };

      centos8x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.26.10.Linux-centos8-x86_64-gcc8.5.tar.gz";
        sha256 = "sha256-V0EUXSGe97vUFq4CkLk0AgH+6fnwBFxye0/woosa4eI=";
      };
    };

    "6.24" = {
      ubuntu1604x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.24.08.Linux-ubuntu16-x86_64-gcc5.4.tar.gz";
        sha256 = "sha256-pwSNU+5iGaYparGordAmibNlzGxDSQz3Ow3SSgR38T8=";
      };

      ubuntu1804x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.24.08.Linux-ubuntu18-x86_64-gcc7.5.tar.gz";
        sha256 = "sha256-cSg/dV64lb2ZW/tJGfwzqm3USAqmrN8YeCyEbypXJGA=";
      };

      ubuntu2004x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.24.08.Linux-ubuntu20-x86_64-gcc9.4.tar.gz";
        sha256 = "sha256-j5jYD+7fCpeO/IXcK+P1l1PKO4thxi38JCGTFe2Hwlw=";
      };
      
      centos7x86_64 = fetchzip {
        url = "https://root.cern/download/root_v6.24.08.Linux-centos7-x86_64-gcc4.8.tar.gz";
        sha256 = "sha256-TOEZa0wtbXfS1BH3bJwZkaxK8DeZg0znl9IhrHsQ1Hw=";
      };
    };
  };

  rootPackagesNewest = with final; dist:
    let
      availableVersions = builtins.attrNames (lib.filterAttrs (n: v: builtins.hasAttr dist v) rootPackages);
      newestVersion = builtins.head (builtins.sort (a: b: lib.versionOlder b a) availableVersions);
    in
      rootPackages."${newestVersion}"."${dist}";
}
