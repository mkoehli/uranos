Name:		uranos
Version:	1.10
Release:	1.10
Summary:	URANOS
LICENSE:	GPLv3

%description

%prep

%build
qmake .
make

%install
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/share/uranos
install -m 755 build/uranos %{buildroot}/usr/bin/uranos
cd data
unzip ENDFdata.zip
install -m 755 ENDFdata %{buildroot}/usr/share/uranos/ENDFdata
install -m 755 IncomingSpectrum.root %{buildroot}/usr/share/uranos/IncomingSpectrum.root

%files
/usr/bin/uranos
/usr/share/uranos/ENDFdata
/usr/share/uranos/IncomingSpectrum.root

