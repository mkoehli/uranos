{ stdenv
, lib
, qtbase
, root
, tbb
, unzip
, imagemagick
, wrapQtAppsHook
, src
}:

stdenv.mkDerivation rec {
  pname = "uranos";
  inherit (src) version;

  inherit src;

  buildInputs = [ qtbase root tbb ];
  nativeBuildInputs = [ wrapQtAppsHook unzip imagemagick ];
  qtWrapperArgs = [ "--unset LD_LIBRARY_PATH" ];

  configurePhase = ''
    runHook preConfigure
    cp src/ForLinux/mainwindow.ui src/
    qmake PREFIX=$out
    runHook postConfigure
  '';

  postInstall = ''
    mkdir -p $out/share/{applications,icons/hicolor/scalable}
    cp resources/uranos.desktop $out/share/applications/uranos.desktop
    cp resources/icon.svg $out/share/icons/hicolor/scalable/uranos.svg
    for size in 16x16 32x32 64x64 128x128 256x256 512x512; do
      mkdir -p $out/share/icons/hicolor/$size
      convert resources/icon.svg -size $size $out/share/icons/hicolor/$size/uranos.png
    done

    cd $out/share/uranos
    unzip ENDFdata.zip
    rm ENDFdata.zip
  '';
}
