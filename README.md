# URANOS - Ultra Rapid Neutron-Only Simulation
[![URANOS at Zenodo](https://img.shields.io/static/v1?label=Code&message=10.5281/zenodo.6578668&color=blue)](https://doi.org/10.5281/zenodo.6578668) [![URANOS at the Geoscientific Model Development](https://img.shields.io/static/v1?label=Publication&message=10.5194/gmd-16-449-2023&color=yellow)](https://doi.org/10.5194/gmd-16-449-2023)

![splashScreenUranos](https://user-images.githubusercontent.com/106179070/170102368-93e66f49-12ab-44a9-860a-2bd1977f715c.jpg)

URANOS is a Monte Carlo toolkit specifically tailored for environmental sciences. It can be used to simulate cosmogenic neutron radiation near the Earth's surface and to study its response to environmental factors, such as soil water content, snow, or biomass.


Key features:

- stand-alone executable,
- parallelization (can run performantly in multiple parallel instances), 
- graphical user interface (optional) for the command line,
- 3D voxel engine to allow the generation of complex environments,
- predefined sources and detector response functions.

## Stay tuned, discuss, contribute

In the **uranos-users mailing list**, users can ask questions, discuss, and contribute to the development of URANOS user community. Any news and updates from the developers will also be broadcasted through this mailing list.

- [Subscribe now!](https://www.ufz.de/index.php?en=41538) (currently 39 recipients)

## Cite as

- **The software code** is available [on GitHub](https://gitlab.com/mkoehli/uranos/) and has been released by [Zenodo, doi:10.5281/zenodo.6578668](https://doi.org/10.5281/zenodo.6578668).
- **An article** on URANOS has been published in the journal of *Geoscientific Model Development*:
    > Köhli, M., Schrön, M., Zacharias, S., and Schmidt, U.: URANOS v1.0 – the Ultra Rapid Adaptable Neutron-Only Simulation for Environmental Research, Geosci. Model Dev., 16, 449–477, [doi:10.5194/gmd-16-449-2023](https://doi.org/10.5194/gmd-16-449-2023), 2023. 

## Publications using URANOS

- [See the list of publications using URANOS](doc/PUBLICATIONS.md)

![URANOS publications statistics](doc/pubplot-shallow.png)

## Wiki

Explanations and instructions can be found in the [URANOS Wiki](https://gitlab.com/mkoehli/uranos/-/wikis/home).

## Usage

<img src="https://user-images.githubusercontent.com/7942719/179392637-f9db7458-2c2b-41e6-8117-d7ad1062a96a.png" alt="URANOS interface" style="width:50%; max-width: 100px">

### Prerequisites Windows 64bit
- [Visual Studio 2022 community version](https://visualstudio.microsoft.com/de/thank-you-downloading-visual-studio/?sku=Community&channel=Release&version=VS2022&source=VSLandingPage&passive=false&cid=2030) (prerequisite for ROOT 6.28). Install with the development option: `desktop development with C++ application` (Microsoft-account required, > 8 GB disk space)
- [ROOT 6.30.02](https://root.cern/download/root_v6.30.02.win64.vc17.exe) (prerequisite for running URANOS 64bit)

### Prerequisites Windows 32bit (Warning: Deprecated)
- [Visual Studio 2019 community version](https://my.visualstudio.com/Downloads?q=visual%20studio%202019&wt.mc_id=o~msft~vscom~older-downloads) (prerequisite for ROOT 6.22). Install with the development option: ` desktop development with C++ application` (Microsoft-account required, > 8 GB disk space)
- [ROOT 6.22.08](https://root.cern/download/root_v6.22.08.win32.vc16.exe) (prerequisite for running URANOS)
- Basic libraries (possibly missing on some systems):
    - Microsoft Visual C++ 2015-2022 redistributional package
    - Windows 10/11 SDK (With C++ development files)
    
### Prerequisites Linux
- [ROOT 6.30.02](https://root.cern/releases/release-63002/) (or the version bundeled with the respective binary)
- [QT5] (the version bundeled with the respective binary, typically the latest LTS)

### Building under Linux
- Download the URANOS source code from this repository.
- Download the ROOT version for your operating system. Compile and Install ROOT.
- Copy also the ROOT files into a subfolder `root/` relative to the `UranosGUI.pro` file.
- In the URANOS folder run `qmake`, then `make`.
- Install missing libraries if necessary. 

### Preparation

1. Download the zipped binary package as well as the ENDF data and IncomingSpectrum zip files. The binaries can be found in this repository in the [binaries folder](https://gitlab.com/mkoehli/uranos/-/tree/main/binaries) and the ENDF data and the Incoming Spectrum files can be found in the [data](https://gitlab.com/mkoehli/uranos/-/tree/main/data) folder in the repository folder tree. The binaries/executables are located in the .zip packages URANOSxxx.zip with the Windows version URANOS.zip and xxx versions for individual Linux distributions. 
2. Unzip the URANOS files into a folder of your choice.
3. Unzip the ENDF data files and the `Input Spectrum Calibration File` into a folder of your choice, preferably both into the same folder, preferably called /ENDF. 

### Starting

4. Run `UranosGUI.exe` for Windows or './URANOSGUI' for Linux. When running URANOS for the first time you will see an error message that relevant data files are missing, this can then be corrected in the next step.
Run `UranosGUI.exe tnw` for configuring the source setup. 
5. Insert the full path and filename of the `IncomingSpectrum.root` into `Input Spectrum Calibration File` under URANOS' *Setup* tab. If the destination path you entered turns red, it is not recognized by the system. Black or grey indicate valid folders or files.
6. Insert the full path to the ENDF files into `Cross Section Folder` under URANOS' *Setup* tab. 
7. Set a `work directory` where the configuration files of your scenario are to be stored. Use a trailing slash. 
8. Set an `output directory` where the simulation results will be stored.  USe a trailing slash.

### First Simulation Run

In order to perform a simulation, a physics environment has to be configured. For most settings the basic default values are already set. In the tab "Physical Parameters", press `Generate` to generate a standard layer setup. Then press `Simulate`. If the simulation has started, the GUI can be closed, the calculation will be executed in a separate command line window.

### Individual Scenarios

In order to configure your own simulation:
1. Define a layer structure of pre-defined materials according to the examples on the website or the minimal config. 
2. Press `Save` to store the geometry configuration in the `work folder`, and `Load` to load it to the GUI. 
3. Put a PNG image file into the `work folder` and name it by the number of the layer you want to define, for example `6.png` for the layer number 6.
 Ticking `Use Layer Maps` URANOS will search for these PNG files in the work folder.
 
  *Note:* The PNG files are a convenient way to define your input material composition at the horizontal scale. They have to be in grayscale (or similar RGB colors) and in an aspect ratio of 1:1 (quadratic). Predefined Materials are encoded by these grayscale values, see the file `Material Codes.txt` in the URANOS folder. There is a difference between material numbers (i.e. layer codes used to fill an entire layer) and vocel codes, which represent different configurations of materials and are used in the input matrix definitions for the voxels. The PNG will be stretched to the full domain size and each pixel will be extruded in the layer to a 3D pixel (voxel) of the given material.
  
### More detailed explanations and instructions

- See the [URANOS Wiki](https://gitlab.com/mkoehli/uranos/-/wikis/home).

