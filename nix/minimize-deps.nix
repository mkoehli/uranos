lib: final: prev: {
  root = prev.root.overrideAttrs (prevPkg:
    let
      flagsToRemove = [ "davix" "http" "opengl" "ssl" "tmva" "xml" "xrootd" ];
    in {
      cmakeFlags = (lib.foldr (name: flags: lib.remove "-D${name}=ON" flags) prevPkg.cmakeFlags [ ]) ++ builtins.map (x: "-D${x}=OFF") flagsToRemove;
      #buildInputs = (lib.subtractLists [ prev.xrootd prev.python3.pkgs.numpy ] prevPkg.buildInputs) ++ ;
  });
}
