/***************************************************************************
**                                                                        **
**  URANOS - Ultra RApid Neutron-Only Simulation                          **
**  designed for Environmental Research                                   **
**  Copyright (C) 2015-2025 Markus Koehli,                                **
**  Physikalisches Institut, Heidelberg University, Germany               **
**                                                                        **
****************************************************************************/

#ifdef _WIN32
#include <io.h>
#define access _access
#elif __linux__
#include <inttypes.h>
#include <unistd.h>
#define __int64 int64_t
#define _close close
#define _read read
#define _lseek64 lseek64
#define _O_RDONLY O_RDONLY
#define _open open
#define _lseeki64 lseek64
#define _lseek lseek
#define stricmp strcasecmp
#endif

#ifdef _WIN32
#pragma warning (disable: 4018 4100 4101 4189 4305)
#elif __linux__
#endif

#include "Toolkit.h"

#include "mainwindow.h"
//#include "customSplashScreen.h"
#include <QApplication>
#include <QPixmap>
#include <QSplashScreen>
#include <QCoreApplication>
#include <QTimer>
#include <QThread>
#include <map>


std::string versionStringMain = "v1.28 (23.02.2025)";

class I : public QThread
{
public:
    static void sleep(unsigned long secs)
    {
        QThread::sleep(secs);
    }
};


void displayLogo()
{
    std::cout << "Welcome to URANOS " << versionStringMain << "\n";
    {
        std::cout<<"                                                                       ./%%/** "<<std::endl;
        std::cout<<"**/,         /((((((((/.        ((     /((((.    ((((   /( (#( (/    ./%%/*    "<<std::endl;
        std::cout<<"*%%,   *****  *%%,   ,%%/      /%%/      /%%%/    */  /..#,/#/,#../  *%%*      "<<std::endl;
        std::cout<<"*%%,    ./(   *%%,   /%%*     */*%%*     /*,#%#,  */ .*##* *#* *##*. %%%%/*    "<<std::endl;
        std::cout<<"*%%,    .*/   *%%(((%%*      *#  #%#.    /* .%%%/.*/  .(#  *#*  #(.   /%%%%%%/ "<<std::endl;
        std::cout<<"/%%,    .*/   *%%*  ,%%*    *#((((%%(    /*   *%%%%/  .##. *#* .##.        /#%/"<<std::endl;
        std::cout<<"/%%,    .*/   /%%*   #%%   *#(    *%%(   /*     (%%/ ,#*,#.*#*.#.*#, #%/    %%/"<<std::endl;
        std::cout<<"/%%,.....*/..//%%*..../#%%/#(....../%%/../*......,%/   ,(#,###,#(,   /%%%%%%/  "<<std::endl;
        std::cout<<",%%/    */, ...**.....**%%%(.......((((((((......((((..((.(###(.((..((((%%%/.. "<<std::endl;
        std::cout<<" ,#%#%%#/,     ........//////////////////////////////////////////////////***..."<<std::endl;
        std::cout<<"                        ...................................................    "<<std::endl;
    }
}


//BOOL dpi_result = SetProcessDPIAware();
//BOOL dpi_result = SetProcessDpiAwareness();

// MAIN method
// the main URANOS code has been transferred to one window application w.
// the window can be closed and URANOS continues to run with the command line output.
// the window w is used to generate the settings, which are saved in the uranos.cfg and in the geometryConfig.dat
// upon clicking 'simulate' URNAOS loads the settings from these files and executes the main simulation which loops over n initial neutrons
// during the simulation it shows the live output of many different parameters including the spatial distribution

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    bool disableGUI = false;
    bool silent = false;
    std::string configFilePath = "";
    int jobID = 0;

    QPixmap splashImage(":/resources/splashScreen.png");
    QSplashScreen splash(splashImage, Qt::WindowStaysOnTopHint);

    #ifdef _WIN32
    #elif __linux__
        QFont custom_font = QFont();
        //custom_font.setWeight(20);
        //custom_font.setPixelSize(9);
        custom_font.setPointSize(8);
        app.setFont(custom_font, "QLabel");
        app.setFont(custom_font, "QTabWidget");
        app.setFont(custom_font, "QPushButton");
        app.setFont(custom_font, "QLineEdit");
        app.setFont(custom_font, "QGroupBox");
        app.setFont(custom_font, "QCheckBox");
        app.setFont(custom_font, "QRadioButton");
        app.setFont(custom_font, "QTableView");
        app.setFont(custom_font, "QTableWidgetItem");
        app.setFont(custom_font, "QStandardItemModel");
        app.setFont(custom_font, "QStandardItem");
        app.setFont(custom_font, "QString");
        app.setFont(custom_font, "QSpinBox");
    #endif

    rootlogon();

    if (ROOT_VERSION_CODE < ROOT_VERSION(6, 30, 2))
    {
        std::cout << "Warning! Update ROOT version to at least 6.30.02" << std::endl;
    }

   std::map<std::string, int> argIndex;

    for (int i = 1; i < argc; ++i)
    {
        argIndex[argv[i]] = i;
    }

    // activates thermal neutron transport
    if (argIndex.count("thermal"))
    {
        w.activateThermal();
    }

    // disables command line output
    if (argIndex.count("silent"))
    {
        w.beSilent();
        silent = true;
    }

    //  hides the GUI
    if (argIndex.count("noGUI"))
    {
        disableGUI = true;
    }

    if (argIndex.count("--version") || argIndex.count("version"))
    {
        std::cout << versionStringMain << std::endl;
        return 0;
    }

    if (argIndex.count("--buildversion") || argIndex.count("buildversion"))
    {
        std::cout << QSysInfo::buildAbi().toStdString() << std::endl;
        return 0;
    }

    // starts URANOS with the source control panel
    if (argIndex.count("tnw"))
    {
        w.activateThermalSkyEvaporation();
    }

    // starts URANOS in the BSS mode
    if (argIndex.count("BS"))
    {
        w.activateBS();
    }

    // activates the source control panel which allows other sources than the cosmic spectrum including thermal neutron transport
    if (argIndex.count("source"))
    {
        w.activateThermalSkyEvaporation();
    }

    if (argIndex.count("config"))
    {
        int index = argIndex["config"] + 1;
        if (index < argc)
        {
            configFilePath = argv[index];
            std::replace(configFilePath.begin(), configFilePath.end(), '\\', '/');
            if (!std::filesystem::exists(configFilePath))
            {
                std::cout << "Config File not found" << std::endl;
                return 0;
            }
        }
    }

    // starts the batch run for n monoenergetic runs with neutrons directly downward
    if (argIndex.count("detectorBatchRun"))
    {
        w.activateThermalSkyEvaporation();
        w.activateDetectorBatchRun();
    }

    // starts the batch run for n monoenergetic runs with neutrons having a random angular distribution
    if (argIndex.count("detectorBatchRun2"))
    {
        w.activateThermalSkyEvaporation();
        w.activateDetectorBatchRun2();
    }

    // starts the batch run for n monoenergetic runs with batch run on angles
    if (argIndex.count("detectorAngleBatchRun"))
    {
        w.activateThermalSkyEvaporation();
        w.activateDetectorAngleBatchRun();
    }

    if (argIndex.count("nuclear warfare"))
    {
         w.activateSkyEvaporation();
    }

    if (argIndex.count("j"))
    {
        int index = argIndex["j"] + 1;
        if (index < argc)
        {
            jobID = atoi(argv[index]);
            w.setJobID(jobID);
        }
    }

    // activates the 2D batchrun over two different parameter sets like soil moisture and air humidity
    if (argIndex.count("batchrun"))
    {
        int index = argIndex["batchrun"];
        if (index + 2 < argc)
        {
            int param1 = std::stoi(argv[index + 1]);
            int param2 = std::stoi(argv[index + 2]);
            w.activateParameterBatchRun(param1, param2);
            std::cout << "Batch run from Parameter " << param1 << " to " << param2 << std::endl;
        }
        else
        {
            std::cout << "Error: batchrun requires two numeric parameters" << std::endl;
            return 1;
        }
    }

    if (!silent)
    {
        if (!configFilePath.empty())
        {
            std::cout << "Using external config file " << configFilePath << std::endl;
            w.setConfigFilePath(configFilePath);
        }
        if (jobID > 0) std::cout << " (Job-ID " << jobID << ")" << std::endl;
    }

    if (!silent)
    {
        displayLogo();
    }


    if (disableGUI)
    {
        w.disabledGUIRun(configFilePath);
        I::sleep(0.5);
        w.close();
        app.closeAllWindows();
        return 0;
    }
    else
    {
        w.getGraphConfig();
        splash.show();
        if(splashImage.isNull());
        else  I::sleep(1.25);
        w.show();
        splash.finish(&w);
    }

    return app.exec();
}

