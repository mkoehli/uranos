/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../URANOS111/src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[193];
    char stringdata0[5606];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "setSourcePosX"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 2), // "xc"
QT_MOC_LITERAL(4, 29, 13), // "setSourcePosY"
QT_MOC_LITERAL(5, 43, 2), // "yc"
QT_MOC_LITERAL(6, 46, 15), // "setGodzillaMode"
QT_MOC_LITERAL(7, 62, 3), // "var"
QT_MOC_LITERAL(8, 66, 8), // "setPause"
QT_MOC_LITERAL(9, 75, 11), // "setContinue"
QT_MOC_LITERAL(10, 87, 11), // "setStopMode"
QT_MOC_LITERAL(11, 99, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(12, 121, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(13, 145, 25), // "on_spinBox_2_valueChanged"
QT_MOC_LITERAL(14, 171, 4), // "arg1"
QT_MOC_LITERAL(15, 176, 30), // "on_pushButton_Simulate_clicked"
QT_MOC_LITERAL(16, 207, 26), // "on_pushButton_stop_clicked"
QT_MOC_LITERAL(17, 234, 25), // "on_checkBoxR_stateChanged"
QT_MOC_LITERAL(18, 260, 25), // "on_checkBoxD_stateChanged"
QT_MOC_LITERAL(19, 286, 33), // "on_sliderSoilMoisture_sliderM..."
QT_MOC_LITERAL(20, 320, 8), // "position"
QT_MOC_LITERAL(21, 329, 27), // "on_sliderAirHum_sliderMoved"
QT_MOC_LITERAL(22, 357, 36), // "on_lineEditNeutrinsTotal_text..."
QT_MOC_LITERAL(23, 394, 25), // "on_sliderAtm1_sliderMoved"
QT_MOC_LITERAL(24, 420, 29), // "on_sliderRigidity_sliderMoved"
QT_MOC_LITERAL(25, 450, 32), // "on_lineEditSquareDim_textChanged"
QT_MOC_LITERAL(26, 483, 30), // "on_lineEditRefresh_textChanged"
QT_MOC_LITERAL(27, 514, 27), // "on_pushButton_Clear_clicked"
QT_MOC_LITERAL(28, 542, 27), // "on_pushButton_Pause_clicked"
QT_MOC_LITERAL(29, 570, 29), // "on_lineEditDetRad_textChanged"
QT_MOC_LITERAL(30, 600, 27), // "on_lineEditDetX_textChanged"
QT_MOC_LITERAL(31, 628, 27), // "on_lineEditDety_textChanged"
QT_MOC_LITERAL(32, 656, 20), // "on_beamRound_clicked"
QT_MOC_LITERAL(33, 677, 21), // "on_beamSquare_clicked"
QT_MOC_LITERAL(34, 699, 21), // "on_radioRiver_clicked"
QT_MOC_LITERAL(35, 721, 21), // "on_radioCoast_clicked"
QT_MOC_LITERAL(36, 743, 22), // "on_radioIsland_clicked"
QT_MOC_LITERAL(37, 766, 20), // "on_radioLake_clicked"
QT_MOC_LITERAL(38, 787, 30), // "on_lineEditBeamRad_textChanged"
QT_MOC_LITERAL(39, 818, 21), // "on_radioRiver_toggled"
QT_MOC_LITERAL(40, 840, 7), // "checked"
QT_MOC_LITERAL(41, 848, 22), // "on_radioButton_clicked"
QT_MOC_LITERAL(42, 871, 30), // "on_lineEditTHLhigh_textChanged"
QT_MOC_LITERAL(43, 902, 29), // "on_lineEditTHLlow_textChanged"
QT_MOC_LITERAL(44, 932, 21), // "on_checkBoxRS_clicked"
QT_MOC_LITERAL(45, 954, 29), // "on_lineEdit_River_textChanged"
QT_MOC_LITERAL(46, 984, 31), // "on_lineEdit_River_2_textChanged"
QT_MOC_LITERAL(47, 1016, 30), // "on_lineEdit_Island_textChanged"
QT_MOC_LITERAL(48, 1047, 33), // "on_lineEdit_Lake_selectionCha..."
QT_MOC_LITERAL(49, 1081, 28), // "on_lineEdit_Lake_textChanged"
QT_MOC_LITERAL(50, 1110, 29), // "on_checkBoxFileOutput_clicked"
QT_MOC_LITERAL(51, 1140, 26), // "on_radioButton_map_clicked"
QT_MOC_LITERAL(52, 1167, 31), // "on_radioButton_mapInter_clicked"
QT_MOC_LITERAL(53, 1199, 30), // "on_radioButton_mapFast_clicked"
QT_MOC_LITERAL(54, 1230, 32), // "on_radioButton_mapAlbedo_clicked"
QT_MOC_LITERAL(55, 1263, 47), // "on_lineEdit_InputSpectrumFold..."
QT_MOC_LITERAL(56, 1311, 46), // "on_lineEdit_CrosssectionFolde..."
QT_MOC_LITERAL(57, 1358, 40), // "on_lineEdit_OutputFolder_edit..."
QT_MOC_LITERAL(58, 1399, 27), // "on_pushButton_about_clicked"
QT_MOC_LITERAL(59, 1427, 30), // "on_checkBoxTransparent_clicked"
QT_MOC_LITERAL(60, 1458, 32), // "on_checkBoxBasicSpectrum_clicked"
QT_MOC_LITERAL(61, 1491, 30), // "on_pushButton_AddLayer_clicked"
QT_MOC_LITERAL(62, 1522, 11), // "setIntType1"
QT_MOC_LITERAL(63, 1534, 11), // "setIntType2"
QT_MOC_LITERAL(64, 1546, 11), // "setIntType3"
QT_MOC_LITERAL(65, 1558, 11), // "eventFilter"
QT_MOC_LITERAL(66, 1570, 6), // "target"
QT_MOC_LITERAL(67, 1577, 7), // "QEvent*"
QT_MOC_LITERAL(68, 1585, 5), // "event"
QT_MOC_LITERAL(69, 1591, 8), // "setFocus"
QT_MOC_LITERAL(70, 1600, 3), // "idx"
QT_MOC_LITERAL(71, 1604, 6), // "setRow"
QT_MOC_LITERAL(72, 1611, 9), // "rowNumber"
QT_MOC_LITERAL(73, 1621, 12), // "setSourcePos"
QT_MOC_LITERAL(74, 1634, 10), // "typebutton"
QT_MOC_LITERAL(75, 1645, 12), // "QPushButton*"
QT_MOC_LITERAL(76, 1658, 33), // "on_pushButton_RemoveLayer_cli..."
QT_MOC_LITERAL(77, 1692, 34), // "on_pushButton_ReadGeometry_cl..."
QT_MOC_LITERAL(78, 1727, 34), // "on_pushButton_SaveGeometry_cl..."
QT_MOC_LITERAL(79, 1762, 37), // "on_spinBox_StartingLayer_valu..."
QT_MOC_LITERAL(80, 1800, 37), // "on_spinBox_DetectorLayer_valu..."
QT_MOC_LITERAL(81, 1838, 35), // "on_spinBox_GroundLayer_valueC..."
QT_MOC_LITERAL(82, 1874, 34), // "on_pushButton_LoadGeometry_cl..."
QT_MOC_LITERAL(83, 1909, 38), // "on_lineEdit_WorkFolder_editin..."
QT_MOC_LITERAL(84, 1948, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(85, 1972, 30), // "on_radioButton_fission_clicked"
QT_MOC_LITERAL(86, 2003, 29), // "on_radioButton_fusion_clicked"
QT_MOC_LITERAL(87, 2033, 28), // "on_checkBox_useImage_clicked"
QT_MOC_LITERAL(88, 2062, 26), // "on_pushButton_Show_clicked"
QT_MOC_LITERAL(89, 2089, 36), // "on_horizontalSliderColor_slid..."
QT_MOC_LITERAL(90, 2126, 40), // "on_horizontalSliderColorZero_..."
QT_MOC_LITERAL(91, 2167, 35), // "on_radioButton_NeutronNight_c..."
QT_MOC_LITERAL(92, 2203, 34), // "on_radioButton_NeutronCold_cl..."
QT_MOC_LITERAL(93, 2238, 35), // "on_radioButton_NeutronPolar_c..."
QT_MOC_LITERAL(94, 2274, 37), // "on_radioButton_NeutronRainbow..."
QT_MOC_LITERAL(95, 2312, 38), // "on_horizontalSliderFPMoist_sl..."
QT_MOC_LITERAL(96, 2351, 36), // "on_horizontalSliderFPHum_slid..."
QT_MOC_LITERAL(97, 2388, 24), // "on_checkBoxFPLog_clicked"
QT_MOC_LITERAL(98, 2413, 38), // "on_horizontalSliderFPHum_2_sl..."
QT_MOC_LITERAL(99, 2452, 32), // "on_pushButton_ActivateFP_clicked"
QT_MOC_LITERAL(100, 2485, 34), // "on_lineEditScotoma_editingFin..."
QT_MOC_LITERAL(101, 2520, 28), // "on_checkBoxGradient2_clicked"
QT_MOC_LITERAL(102, 2549, 31), // "on_checkBoxSelectedData_clicked"
QT_MOC_LITERAL(103, 2581, 27), // "on_checkBoxFastData_clicked"
QT_MOC_LITERAL(104, 2609, 35), // "on_checkBoxIntermediateData_c..."
QT_MOC_LITERAL(105, 2645, 33), // "on_checkBoxEpithermalData_cli..."
QT_MOC_LITERAL(106, 2679, 33), // "on_sliderSoilPorosity_sliderM..."
QT_MOC_LITERAL(107, 2713, 34), // "on_checkBoxFileOutputPDF_2_cl..."
QT_MOC_LITERAL(108, 2748, 31), // "on_checkBoxCreateFolder_clicked"
QT_MOC_LITERAL(109, 2780, 27), // "on_radioButton_AmBe_clicked"
QT_MOC_LITERAL(110, 2808, 28), // "on_lineEdit_xPos_textChanged"
QT_MOC_LITERAL(111, 2837, 28), // "on_lineEdit_yPos_textChanged"
QT_MOC_LITERAL(112, 2866, 28), // "on_lineEdit_zPos_textChanged"
QT_MOC_LITERAL(113, 2895, 30), // "on_lineEdit_zPos_2_textChanged"
QT_MOC_LITERAL(114, 2926, 26), // "on_checkBoxThermal_toggled"
QT_MOC_LITERAL(115, 2953, 31), // "on_radioButton_mapTrack_clicked"
QT_MOC_LITERAL(116, 2985, 38), // "on_lineEditManualColorZero_te..."
QT_MOC_LITERAL(117, 3024, 34), // "on_lineEditManualColor_textCh..."
QT_MOC_LITERAL(118, 3059, 36), // "on_radioButton_mapTrackInter_..."
QT_MOC_LITERAL(119, 3096, 35), // "on_radioButton_mapTrackFast_c..."
QT_MOC_LITERAL(120, 3132, 37), // "on_radioButton_mapTrackAlbedo..."
QT_MOC_LITERAL(121, 3170, 26), // "on_checkBoxNoTrack_toggled"
QT_MOC_LITERAL(122, 3197, 33), // "on_radioButton_mapThermal_cli..."
QT_MOC_LITERAL(123, 3231, 38), // "on_radioButton_mapTrackTherma..."
QT_MOC_LITERAL(124, 3270, 30), // "on_checkBoxThermalData_clicked"
QT_MOC_LITERAL(125, 3301, 28), // "on_checkBoxSaveEvery_toggled"
QT_MOC_LITERAL(126, 3330, 31), // "on_radioButton_NoSource_clicked"
QT_MOC_LITERAL(127, 3362, 29), // "on_lineEdit_xSize_textChanged"
QT_MOC_LITERAL(128, 3392, 29), // "on_lineEdit_ySize_textChanged"
QT_MOC_LITERAL(129, 3422, 36), // "on_radioButton_ThermalSource_..."
QT_MOC_LITERAL(130, 3459, 42), // "on_radioButton_MonoenergeticS..."
QT_MOC_LITERAL(131, 3502, 36), // "on_lineEdit_SourceEnergy_text..."
QT_MOC_LITERAL(132, 3539, 32), // "on_lineEditScotoma_2_textChanged"
QT_MOC_LITERAL(133, 3572, 19), // "on_checkBox_clicked"
QT_MOC_LITERAL(134, 3592, 40), // "on_checkBox_NoMultipleScatter..."
QT_MOC_LITERAL(135, 3633, 25), // "on_checkBoxManual_toggled"
QT_MOC_LITERAL(136, 3659, 34), // "on_checkBox_TrackAllLayers_to..."
QT_MOC_LITERAL(137, 3694, 30), // "on_checkBoxLogarithmic_toggled"
QT_MOC_LITERAL(138, 3725, 39), // "on_radioButton_NeutronGraySca..."
QT_MOC_LITERAL(139, 3765, 33), // "on_radioButton_NeutronHot_cli..."
QT_MOC_LITERAL(140, 3799, 37), // "on_radioButton_NeutronThermal..."
QT_MOC_LITERAL(141, 3837, 34), // "on_radioButton_ModeratedCf_cl..."
QT_MOC_LITERAL(142, 3872, 31), // "on_radioButton_Cylinder_toggled"
QT_MOC_LITERAL(143, 3904, 29), // "on_radioButton_Sphere_toggled"
QT_MOC_LITERAL(144, 3934, 46), // "on_radioButton_detectorLayerE..."
QT_MOC_LITERAL(145, 3981, 45), // "on_radioButton_detectorLayerR..."
QT_MOC_LITERAL(146, 4027, 41), // "on_radioButton_detectorEnergy..."
QT_MOC_LITERAL(147, 4069, 40), // "on_radioButton_detectorRealis..."
QT_MOC_LITERAL(148, 4110, 32), // "on_checkBox_VolumeSource_toggled"
QT_MOC_LITERAL(149, 4143, 27), // "on_checkBox_HEModel_toggled"
QT_MOC_LITERAL(150, 4171, 35), // "on_checkBox_activateThermal_t..."
QT_MOC_LITERAL(151, 4207, 39), // "on_horizontalSliderDetector_s..."
QT_MOC_LITERAL(152, 4247, 44), // "on_horizontalSliderDetectorCo..."
QT_MOC_LITERAL(153, 4292, 14), // "exportSettings"
QT_MOC_LITERAL(154, 4307, 6), // "string"
QT_MOC_LITERAL(155, 4314, 3), // "str"
QT_MOC_LITERAL(156, 4318, 14), // "importSettings"
QT_MOC_LITERAL(157, 4333, 29), // "on_pushButton_Enlarge_clicked"
QT_MOC_LITERAL(158, 4363, 38), // "on_lineEditAntiScotoma_editin..."
QT_MOC_LITERAL(159, 4402, 30), // "on_checkBoxSaveEvery_2_clicked"
QT_MOC_LITERAL(160, 4433, 46), // "on_lineEditClearEveryXNeutron..."
QT_MOC_LITERAL(161, 4480, 42), // "on_lineEditClearEveryXNeutron..."
QT_MOC_LITERAL(162, 4523, 34), // "on_lineEdit_AutoUpdate_textCh..."
QT_MOC_LITERAL(163, 4558, 34), // "on_checkBoxAutoRefreshRate_to..."
QT_MOC_LITERAL(164, 4593, 43), // "on_checkBoxClearEveryDisplayR..."
QT_MOC_LITERAL(165, 4637, 43), // "on_checkBoxClearEveryDisplayR..."
QT_MOC_LITERAL(166, 4681, 31), // "on_checkBoxWarnMAterial_toggled"
QT_MOC_LITERAL(167, 4713, 37), // "on_radioButton_mapTrackEnergy..."
QT_MOC_LITERAL(168, 4751, 31), // "on_checkBoxTrackingData_clicked"
QT_MOC_LITERAL(169, 4783, 38), // "on_checkBoxHighResTrackingDat..."
QT_MOC_LITERAL(170, 4822, 29), // "on_radioButton_ySheet_clicked"
QT_MOC_LITERAL(171, 4852, 29), // "on_radioButton_xSheet_clicked"
QT_MOC_LITERAL(172, 4882, 32), // "on_lineEditDetLength_textChanged"
QT_MOC_LITERAL(173, 4915, 29), // "on_lineEdit_zSize_textChanged"
QT_MOC_LITERAL(174, 4945, 34), // "on_radioButton_TopToBottom_to..."
QT_MOC_LITERAL(175, 4980, 34), // "on_radioButton_BottomToTop_to..."
QT_MOC_LITERAL(176, 5015, 34), // "on_radioButton_LeftToRight_to..."
QT_MOC_LITERAL(177, 5050, 34), // "on_radioButton_RightToLeft_to..."
QT_MOC_LITERAL(178, 5085, 27), // "on_radioButton_Omni_toggled"
QT_MOC_LITERAL(179, 5113, 32), // "on_checkBox_DomainCutoff_toggled"
QT_MOC_LITERAL(180, 5146, 35), // "on_lineEditDomainFactor_textC..."
QT_MOC_LITERAL(181, 5182, 38), // "on_checkBox_DomainCutoffMeter..."
QT_MOC_LITERAL(182, 5221, 35), // "on_lineEditDomainMeters_textC..."
QT_MOC_LITERAL(183, 5257, 30), // "on_checkBoxFileOutput2_toggled"
QT_MOC_LITERAL(184, 5288, 34), // "on_checkBoxExportAllTracks_to..."
QT_MOC_LITERAL(185, 5323, 40), // "on_checkBox_ReflectiveBoundar..."
QT_MOC_LITERAL(186, 5364, 38), // "on_checkBox_PeriodicBoundarie..."
QT_MOC_LITERAL(187, 5403, 40), // "on_lineEdit_DetectorFile_edit..."
QT_MOC_LITERAL(188, 5444, 36), // "on_lineEdit_DetectorFile_text..."
QT_MOC_LITERAL(189, 5481, 29), // "on_checkBoxFileOutput_toggled"
QT_MOC_LITERAL(190, 5511, 30), // "on_checkBoxFileOutput3_toggled"
QT_MOC_LITERAL(191, 5542, 32), // "on_pushButton_SaveConfig_clicked"
QT_MOC_LITERAL(192, 5575, 30) // "on_pushButton_Enlarge2_clicked"

    },
    "MainWindow\0setSourcePosX\0\0xc\0setSourcePosY\0"
    "yc\0setGodzillaMode\0var\0setPause\0"
    "setContinue\0setStopMode\0on_pushButton_clicked\0"
    "on_pushButton_2_clicked\0"
    "on_spinBox_2_valueChanged\0arg1\0"
    "on_pushButton_Simulate_clicked\0"
    "on_pushButton_stop_clicked\0"
    "on_checkBoxR_stateChanged\0"
    "on_checkBoxD_stateChanged\0"
    "on_sliderSoilMoisture_sliderMoved\0"
    "position\0on_sliderAirHum_sliderMoved\0"
    "on_lineEditNeutrinsTotal_textChanged\0"
    "on_sliderAtm1_sliderMoved\0"
    "on_sliderRigidity_sliderMoved\0"
    "on_lineEditSquareDim_textChanged\0"
    "on_lineEditRefresh_textChanged\0"
    "on_pushButton_Clear_clicked\0"
    "on_pushButton_Pause_clicked\0"
    "on_lineEditDetRad_textChanged\0"
    "on_lineEditDetX_textChanged\0"
    "on_lineEditDety_textChanged\0"
    "on_beamRound_clicked\0on_beamSquare_clicked\0"
    "on_radioRiver_clicked\0on_radioCoast_clicked\0"
    "on_radioIsland_clicked\0on_radioLake_clicked\0"
    "on_lineEditBeamRad_textChanged\0"
    "on_radioRiver_toggled\0checked\0"
    "on_radioButton_clicked\0"
    "on_lineEditTHLhigh_textChanged\0"
    "on_lineEditTHLlow_textChanged\0"
    "on_checkBoxRS_clicked\0"
    "on_lineEdit_River_textChanged\0"
    "on_lineEdit_River_2_textChanged\0"
    "on_lineEdit_Island_textChanged\0"
    "on_lineEdit_Lake_selectionChanged\0"
    "on_lineEdit_Lake_textChanged\0"
    "on_checkBoxFileOutput_clicked\0"
    "on_radioButton_map_clicked\0"
    "on_radioButton_mapInter_clicked\0"
    "on_radioButton_mapFast_clicked\0"
    "on_radioButton_mapAlbedo_clicked\0"
    "on_lineEdit_InputSpectrumFolder_editingFinished\0"
    "on_lineEdit_CrosssectionFolder_editingFinished\0"
    "on_lineEdit_OutputFolder_editingFinished\0"
    "on_pushButton_about_clicked\0"
    "on_checkBoxTransparent_clicked\0"
    "on_checkBoxBasicSpectrum_clicked\0"
    "on_pushButton_AddLayer_clicked\0"
    "setIntType1\0setIntType2\0setIntType3\0"
    "eventFilter\0target\0QEvent*\0event\0"
    "setFocus\0idx\0setRow\0rowNumber\0"
    "setSourcePos\0typebutton\0QPushButton*\0"
    "on_pushButton_RemoveLayer_clicked\0"
    "on_pushButton_ReadGeometry_clicked\0"
    "on_pushButton_SaveGeometry_clicked\0"
    "on_spinBox_StartingLayer_valueChanged\0"
    "on_spinBox_DetectorLayer_valueChanged\0"
    "on_spinBox_GroundLayer_valueChanged\0"
    "on_pushButton_LoadGeometry_clicked\0"
    "on_lineEdit_WorkFolder_editingFinished\0"
    "on_pushButton_6_clicked\0"
    "on_radioButton_fission_clicked\0"
    "on_radioButton_fusion_clicked\0"
    "on_checkBox_useImage_clicked\0"
    "on_pushButton_Show_clicked\0"
    "on_horizontalSliderColor_sliderMoved\0"
    "on_horizontalSliderColorZero_sliderMoved\0"
    "on_radioButton_NeutronNight_clicked\0"
    "on_radioButton_NeutronCold_clicked\0"
    "on_radioButton_NeutronPolar_clicked\0"
    "on_radioButton_NeutronRainbow_clicked\0"
    "on_horizontalSliderFPMoist_sliderMoved\0"
    "on_horizontalSliderFPHum_sliderMoved\0"
    "on_checkBoxFPLog_clicked\0"
    "on_horizontalSliderFPHum_2_sliderMoved\0"
    "on_pushButton_ActivateFP_clicked\0"
    "on_lineEditScotoma_editingFinished\0"
    "on_checkBoxGradient2_clicked\0"
    "on_checkBoxSelectedData_clicked\0"
    "on_checkBoxFastData_clicked\0"
    "on_checkBoxIntermediateData_clicked\0"
    "on_checkBoxEpithermalData_clicked\0"
    "on_sliderSoilPorosity_sliderMoved\0"
    "on_checkBoxFileOutputPDF_2_clicked\0"
    "on_checkBoxCreateFolder_clicked\0"
    "on_radioButton_AmBe_clicked\0"
    "on_lineEdit_xPos_textChanged\0"
    "on_lineEdit_yPos_textChanged\0"
    "on_lineEdit_zPos_textChanged\0"
    "on_lineEdit_zPos_2_textChanged\0"
    "on_checkBoxThermal_toggled\0"
    "on_radioButton_mapTrack_clicked\0"
    "on_lineEditManualColorZero_textChanged\0"
    "on_lineEditManualColor_textChanged\0"
    "on_radioButton_mapTrackInter_clicked\0"
    "on_radioButton_mapTrackFast_clicked\0"
    "on_radioButton_mapTrackAlbedo_clicked\0"
    "on_checkBoxNoTrack_toggled\0"
    "on_radioButton_mapThermal_clicked\0"
    "on_radioButton_mapTrackThermal_clicked\0"
    "on_checkBoxThermalData_clicked\0"
    "on_checkBoxSaveEvery_toggled\0"
    "on_radioButton_NoSource_clicked\0"
    "on_lineEdit_xSize_textChanged\0"
    "on_lineEdit_ySize_textChanged\0"
    "on_radioButton_ThermalSource_clicked\0"
    "on_radioButton_MonoenergeticSource_clicked\0"
    "on_lineEdit_SourceEnergy_textChanged\0"
    "on_lineEditScotoma_2_textChanged\0"
    "on_checkBox_clicked\0"
    "on_checkBox_NoMultipleScattering_toggled\0"
    "on_checkBoxManual_toggled\0"
    "on_checkBox_TrackAllLayers_toggled\0"
    "on_checkBoxLogarithmic_toggled\0"
    "on_radioButton_NeutronGrayScale_clicked\0"
    "on_radioButton_NeutronHot_clicked\0"
    "on_radioButton_NeutronThermal_clicked\0"
    "on_radioButton_ModeratedCf_clicked\0"
    "on_radioButton_Cylinder_toggled\0"
    "on_radioButton_Sphere_toggled\0"
    "on_radioButton_detectorLayerEnergyBand_toggled\0"
    "on_radioButton_detectorLayerRealistic_toggled\0"
    "on_radioButton_detectorEnergyBand_toggled\0"
    "on_radioButton_detectorRealistic_toggled\0"
    "on_checkBox_VolumeSource_toggled\0"
    "on_checkBox_HEModel_toggled\0"
    "on_checkBox_activateThermal_toggled\0"
    "on_horizontalSliderDetector_sliderMoved\0"
    "on_horizontalSliderDetectorColor_sliderMoved\0"
    "exportSettings\0string\0str\0importSettings\0"
    "on_pushButton_Enlarge_clicked\0"
    "on_lineEditAntiScotoma_editingFinished\0"
    "on_checkBoxSaveEvery_2_clicked\0"
    "on_lineEditClearEveryXNeutrons_editingFinished\0"
    "on_lineEditClearEveryXNeutrons_textChanged\0"
    "on_lineEdit_AutoUpdate_textChanged\0"
    "on_checkBoxAutoRefreshRate_toggled\0"
    "on_checkBoxClearEveryDisplayRefresh_toggled\0"
    "on_checkBoxClearEveryDisplayRefresh_clicked\0"
    "on_checkBoxWarnMAterial_toggled\0"
    "on_radioButton_mapTrackEnergy_clicked\0"
    "on_checkBoxTrackingData_clicked\0"
    "on_checkBoxHighResTrackingData_clicked\0"
    "on_radioButton_ySheet_clicked\0"
    "on_radioButton_xSheet_clicked\0"
    "on_lineEditDetLength_textChanged\0"
    "on_lineEdit_zSize_textChanged\0"
    "on_radioButton_TopToBottom_toggled\0"
    "on_radioButton_BottomToTop_toggled\0"
    "on_radioButton_LeftToRight_toggled\0"
    "on_radioButton_RightToLeft_toggled\0"
    "on_radioButton_Omni_toggled\0"
    "on_checkBox_DomainCutoff_toggled\0"
    "on_lineEditDomainFactor_textChanged\0"
    "on_checkBox_DomainCutoffMeters_toggled\0"
    "on_lineEditDomainMeters_textChanged\0"
    "on_checkBoxFileOutput2_toggled\0"
    "on_checkBoxExportAllTracks_toggled\0"
    "on_checkBox_ReflectiveBoundaries_toggled\0"
    "on_checkBox_PeriodicBoundaries_toggled\0"
    "on_lineEdit_DetectorFile_editingFinished\0"
    "on_lineEdit_DetectorFile_textChanged\0"
    "on_checkBoxFileOutput_toggled\0"
    "on_checkBoxFileOutput3_toggled\0"
    "on_pushButton_SaveConfig_clicked\0"
    "on_pushButton_Enlarge2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
     178,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  904,    2, 0x08 /* Private */,
       4,    1,  907,    2, 0x08 /* Private */,
       6,    1,  910,    2, 0x08 /* Private */,
       8,    1,  913,    2, 0x08 /* Private */,
       9,    1,  916,    2, 0x08 /* Private */,
      10,    1,  919,    2, 0x08 /* Private */,
      11,    0,  922,    2, 0x08 /* Private */,
      12,    0,  923,    2, 0x08 /* Private */,
      13,    1,  924,    2, 0x08 /* Private */,
      15,    0,  927,    2, 0x08 /* Private */,
      16,    0,  928,    2, 0x08 /* Private */,
      17,    1,  929,    2, 0x08 /* Private */,
      18,    1,  932,    2, 0x08 /* Private */,
      19,    1,  935,    2, 0x08 /* Private */,
      21,    1,  938,    2, 0x08 /* Private */,
      22,    1,  941,    2, 0x08 /* Private */,
      23,    1,  944,    2, 0x08 /* Private */,
      24,    1,  947,    2, 0x08 /* Private */,
      25,    1,  950,    2, 0x08 /* Private */,
      26,    1,  953,    2, 0x08 /* Private */,
      27,    0,  956,    2, 0x08 /* Private */,
      28,    0,  957,    2, 0x08 /* Private */,
      29,    1,  958,    2, 0x08 /* Private */,
      30,    1,  961,    2, 0x08 /* Private */,
      31,    1,  964,    2, 0x08 /* Private */,
      32,    0,  967,    2, 0x08 /* Private */,
      33,    0,  968,    2, 0x08 /* Private */,
      34,    0,  969,    2, 0x08 /* Private */,
      35,    0,  970,    2, 0x08 /* Private */,
      36,    0,  971,    2, 0x08 /* Private */,
      37,    0,  972,    2, 0x08 /* Private */,
      38,    1,  973,    2, 0x08 /* Private */,
      39,    1,  976,    2, 0x08 /* Private */,
      41,    0,  979,    2, 0x08 /* Private */,
      42,    1,  980,    2, 0x08 /* Private */,
      43,    1,  983,    2, 0x08 /* Private */,
      44,    0,  986,    2, 0x08 /* Private */,
      45,    1,  987,    2, 0x08 /* Private */,
      46,    1,  990,    2, 0x08 /* Private */,
      47,    1,  993,    2, 0x08 /* Private */,
      48,    0,  996,    2, 0x08 /* Private */,
      49,    1,  997,    2, 0x08 /* Private */,
      50,    0, 1000,    2, 0x08 /* Private */,
      51,    0, 1001,    2, 0x08 /* Private */,
      52,    0, 1002,    2, 0x08 /* Private */,
      53,    0, 1003,    2, 0x08 /* Private */,
      54,    0, 1004,    2, 0x08 /* Private */,
      55,    0, 1005,    2, 0x08 /* Private */,
      56,    0, 1006,    2, 0x08 /* Private */,
      57,    0, 1007,    2, 0x08 /* Private */,
      58,    0, 1008,    2, 0x08 /* Private */,
      59,    0, 1009,    2, 0x08 /* Private */,
      60,    0, 1010,    2, 0x08 /* Private */,
      61,    0, 1011,    2, 0x08 /* Private */,
      62,    0, 1012,    2, 0x08 /* Private */,
      63,    0, 1013,    2, 0x08 /* Private */,
      64,    0, 1014,    2, 0x08 /* Private */,
      65,    2, 1015,    2, 0x08 /* Private */,
      69,    1, 1020,    2, 0x08 /* Private */,
      71,    1, 1023,    2, 0x08 /* Private */,
      73,    2, 1026,    2, 0x08 /* Private */,
      74,    1, 1031,    2, 0x08 /* Private */,
      76,    0, 1034,    2, 0x08 /* Private */,
      77,    0, 1035,    2, 0x08 /* Private */,
      78,    0, 1036,    2, 0x08 /* Private */,
      79,    1, 1037,    2, 0x08 /* Private */,
      79,    1, 1040,    2, 0x08 /* Private */,
      80,    1, 1043,    2, 0x08 /* Private */,
      81,    1, 1046,    2, 0x08 /* Private */,
      82,    0, 1049,    2, 0x08 /* Private */,
      83,    0, 1050,    2, 0x08 /* Private */,
      84,    0, 1051,    2, 0x08 /* Private */,
      85,    0, 1052,    2, 0x08 /* Private */,
      86,    0, 1053,    2, 0x08 /* Private */,
      87,    0, 1054,    2, 0x08 /* Private */,
      88,    0, 1055,    2, 0x08 /* Private */,
      89,    1, 1056,    2, 0x08 /* Private */,
      90,    1, 1059,    2, 0x08 /* Private */,
      91,    0, 1062,    2, 0x08 /* Private */,
      92,    0, 1063,    2, 0x08 /* Private */,
      93,    0, 1064,    2, 0x08 /* Private */,
      94,    0, 1065,    2, 0x08 /* Private */,
      95,    1, 1066,    2, 0x08 /* Private */,
      96,    1, 1069,    2, 0x08 /* Private */,
      97,    0, 1072,    2, 0x08 /* Private */,
      98,    1, 1073,    2, 0x08 /* Private */,
      99,    0, 1076,    2, 0x08 /* Private */,
     100,    0, 1077,    2, 0x08 /* Private */,
     101,    0, 1078,    2, 0x08 /* Private */,
     102,    0, 1079,    2, 0x08 /* Private */,
     103,    0, 1080,    2, 0x08 /* Private */,
     104,    0, 1081,    2, 0x08 /* Private */,
     105,    0, 1082,    2, 0x08 /* Private */,
     106,    1, 1083,    2, 0x08 /* Private */,
     107,    1, 1086,    2, 0x08 /* Private */,
     108,    1, 1089,    2, 0x08 /* Private */,
     109,    0, 1092,    2, 0x08 /* Private */,
     110,    1, 1093,    2, 0x08 /* Private */,
     111,    1, 1096,    2, 0x08 /* Private */,
     112,    1, 1099,    2, 0x08 /* Private */,
     113,    1, 1102,    2, 0x08 /* Private */,
     114,    1, 1105,    2, 0x08 /* Private */,
     115,    0, 1108,    2, 0x08 /* Private */,
     116,    1, 1109,    2, 0x08 /* Private */,
     117,    1, 1112,    2, 0x08 /* Private */,
     118,    0, 1115,    2, 0x08 /* Private */,
     119,    0, 1116,    2, 0x08 /* Private */,
     120,    0, 1117,    2, 0x08 /* Private */,
     121,    1, 1118,    2, 0x08 /* Private */,
     122,    0, 1121,    2, 0x08 /* Private */,
     123,    0, 1122,    2, 0x08 /* Private */,
     124,    0, 1123,    2, 0x08 /* Private */,
     125,    1, 1124,    2, 0x08 /* Private */,
     126,    0, 1127,    2, 0x08 /* Private */,
     127,    1, 1128,    2, 0x08 /* Private */,
     128,    1, 1131,    2, 0x08 /* Private */,
     129,    0, 1134,    2, 0x08 /* Private */,
     130,    0, 1135,    2, 0x08 /* Private */,
     131,    1, 1136,    2, 0x08 /* Private */,
     132,    1, 1139,    2, 0x08 /* Private */,
     133,    0, 1142,    2, 0x08 /* Private */,
     134,    1, 1143,    2, 0x08 /* Private */,
     135,    1, 1146,    2, 0x08 /* Private */,
     136,    1, 1149,    2, 0x08 /* Private */,
     137,    1, 1152,    2, 0x08 /* Private */,
     138,    0, 1155,    2, 0x08 /* Private */,
     139,    0, 1156,    2, 0x08 /* Private */,
     140,    0, 1157,    2, 0x08 /* Private */,
     141,    0, 1158,    2, 0x08 /* Private */,
     142,    1, 1159,    2, 0x08 /* Private */,
     143,    1, 1162,    2, 0x08 /* Private */,
     144,    1, 1165,    2, 0x08 /* Private */,
     145,    1, 1168,    2, 0x08 /* Private */,
     146,    1, 1171,    2, 0x08 /* Private */,
     147,    1, 1174,    2, 0x08 /* Private */,
     148,    1, 1177,    2, 0x08 /* Private */,
     149,    1, 1180,    2, 0x08 /* Private */,
     150,    1, 1183,    2, 0x08 /* Private */,
     151,    1, 1186,    2, 0x08 /* Private */,
     152,    1, 1189,    2, 0x08 /* Private */,
     153,    1, 1192,    2, 0x08 /* Private */,
     156,    0, 1195,    2, 0x08 /* Private */,
     157,    0, 1196,    2, 0x08 /* Private */,
     158,    0, 1197,    2, 0x08 /* Private */,
     159,    1, 1198,    2, 0x08 /* Private */,
     160,    0, 1201,    2, 0x08 /* Private */,
     161,    1, 1202,    2, 0x08 /* Private */,
     162,    1, 1205,    2, 0x08 /* Private */,
     163,    1, 1208,    2, 0x08 /* Private */,
     164,    1, 1211,    2, 0x08 /* Private */,
     165,    1, 1214,    2, 0x08 /* Private */,
     166,    1, 1217,    2, 0x08 /* Private */,
     167,    0, 1220,    2, 0x08 /* Private */,
     168,    1, 1221,    2, 0x08 /* Private */,
     169,    1, 1224,    2, 0x08 /* Private */,
     170,    0, 1227,    2, 0x08 /* Private */,
     171,    0, 1228,    2, 0x08 /* Private */,
     172,    1, 1229,    2, 0x08 /* Private */,
     173,    1, 1232,    2, 0x08 /* Private */,
     174,    1, 1235,    2, 0x08 /* Private */,
     175,    1, 1238,    2, 0x08 /* Private */,
     176,    1, 1241,    2, 0x08 /* Private */,
     177,    1, 1244,    2, 0x08 /* Private */,
     178,    1, 1247,    2, 0x08 /* Private */,
     179,    1, 1250,    2, 0x08 /* Private */,
     180,    1, 1253,    2, 0x08 /* Private */,
     181,    1, 1256,    2, 0x08 /* Private */,
     182,    1, 1259,    2, 0x08 /* Private */,
     183,    1, 1262,    2, 0x08 /* Private */,
     184,    1, 1265,    2, 0x08 /* Private */,
     185,    1, 1268,    2, 0x08 /* Private */,
     186,    1, 1271,    2, 0x08 /* Private */,
     187,    0, 1274,    2, 0x08 /* Private */,
     188,    1, 1275,    2, 0x08 /* Private */,
     189,    1, 1278,    2, 0x08 /* Private */,
     190,    1, 1281,    2, 0x08 /* Private */,
     191,    0, 1284,    2, 0x08 /* Private */,
     192,    0, 1285,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Float,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QObjectStar, 0x80000000 | 67,   66,   68,
    QMetaType::Void, QMetaType::QModelIndex,   70,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Float, QMetaType::Float,    3,    5,
    0x80000000 | 75, QMetaType::QModelIndex,   70,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, 0x80000000 | 154,  155,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setSourcePosX((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->setSourcePosY((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->setGodzillaMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setPause((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setContinue((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setStopMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_pushButton_clicked(); break;
        case 7: _t->on_pushButton_2_clicked(); break;
        case 8: _t->on_spinBox_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_pushButton_Simulate_clicked(); break;
        case 10: _t->on_pushButton_stop_clicked(); break;
        case 11: _t->on_checkBoxR_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_checkBoxD_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_sliderSoilMoisture_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_sliderAirHum_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_lineEditNeutrinsTotal_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->on_sliderAtm1_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_sliderRigidity_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_lineEditSquareDim_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: _t->on_lineEditRefresh_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 20: _t->on_pushButton_Clear_clicked(); break;
        case 21: _t->on_pushButton_Pause_clicked(); break;
        case 22: _t->on_lineEditDetRad_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: _t->on_lineEditDetX_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 24: _t->on_lineEditDety_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 25: _t->on_beamRound_clicked(); break;
        case 26: _t->on_beamSquare_clicked(); break;
        case 27: _t->on_radioRiver_clicked(); break;
        case 28: _t->on_radioCoast_clicked(); break;
        case 29: _t->on_radioIsland_clicked(); break;
        case 30: _t->on_radioLake_clicked(); break;
        case 31: _t->on_lineEditBeamRad_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 32: _t->on_radioRiver_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->on_radioButton_clicked(); break;
        case 34: _t->on_lineEditTHLhigh_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 35: _t->on_lineEditTHLlow_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 36: _t->on_checkBoxRS_clicked(); break;
        case 37: _t->on_lineEdit_River_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 38: _t->on_lineEdit_River_2_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 39: _t->on_lineEdit_Island_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 40: _t->on_lineEdit_Lake_selectionChanged(); break;
        case 41: _t->on_lineEdit_Lake_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 42: _t->on_checkBoxFileOutput_clicked(); break;
        case 43: _t->on_radioButton_map_clicked(); break;
        case 44: _t->on_radioButton_mapInter_clicked(); break;
        case 45: _t->on_radioButton_mapFast_clicked(); break;
        case 46: _t->on_radioButton_mapAlbedo_clicked(); break;
        case 47: _t->on_lineEdit_InputSpectrumFolder_editingFinished(); break;
        case 48: _t->on_lineEdit_CrosssectionFolder_editingFinished(); break;
        case 49: _t->on_lineEdit_OutputFolder_editingFinished(); break;
        case 50: _t->on_pushButton_about_clicked(); break;
        case 51: _t->on_checkBoxTransparent_clicked(); break;
        case 52: _t->on_checkBoxBasicSpectrum_clicked(); break;
        case 53: _t->on_pushButton_AddLayer_clicked(); break;
        case 54: _t->setIntType1(); break;
        case 55: _t->setIntType2(); break;
        case 56: _t->setIntType3(); break;
        case 57: { bool _r = _t->eventFilter((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QEvent*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 58: _t->setFocus((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 59: _t->setRow((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 60: _t->setSourcePos((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 61: { QPushButton* _r = _t->typebutton((*reinterpret_cast< QModelIndex(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QPushButton**>(_a[0]) = std::move(_r); }  break;
        case 62: _t->on_pushButton_RemoveLayer_clicked(); break;
        case 63: _t->on_pushButton_ReadGeometry_clicked(); break;
        case 64: _t->on_pushButton_SaveGeometry_clicked(); break;
        case 65: _t->on_spinBox_StartingLayer_valueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 66: _t->on_spinBox_StartingLayer_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 67: _t->on_spinBox_DetectorLayer_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 68: _t->on_spinBox_GroundLayer_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 69: _t->on_pushButton_LoadGeometry_clicked(); break;
        case 70: _t->on_lineEdit_WorkFolder_editingFinished(); break;
        case 71: _t->on_pushButton_6_clicked(); break;
        case 72: _t->on_radioButton_fission_clicked(); break;
        case 73: _t->on_radioButton_fusion_clicked(); break;
        case 74: _t->on_checkBox_useImage_clicked(); break;
        case 75: _t->on_pushButton_Show_clicked(); break;
        case 76: _t->on_horizontalSliderColor_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 77: _t->on_horizontalSliderColorZero_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 78: _t->on_radioButton_NeutronNight_clicked(); break;
        case 79: _t->on_radioButton_NeutronCold_clicked(); break;
        case 80: _t->on_radioButton_NeutronPolar_clicked(); break;
        case 81: _t->on_radioButton_NeutronRainbow_clicked(); break;
        case 82: _t->on_horizontalSliderFPMoist_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 83: _t->on_horizontalSliderFPHum_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 84: _t->on_checkBoxFPLog_clicked(); break;
        case 85: _t->on_horizontalSliderFPHum_2_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 86: _t->on_pushButton_ActivateFP_clicked(); break;
        case 87: _t->on_lineEditScotoma_editingFinished(); break;
        case 88: _t->on_checkBoxGradient2_clicked(); break;
        case 89: _t->on_checkBoxSelectedData_clicked(); break;
        case 90: _t->on_checkBoxFastData_clicked(); break;
        case 91: _t->on_checkBoxIntermediateData_clicked(); break;
        case 92: _t->on_checkBoxEpithermalData_clicked(); break;
        case 93: _t->on_sliderSoilPorosity_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 94: _t->on_checkBoxFileOutputPDF_2_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 95: _t->on_checkBoxCreateFolder_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 96: _t->on_radioButton_AmBe_clicked(); break;
        case 97: _t->on_lineEdit_xPos_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 98: _t->on_lineEdit_yPos_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 99: _t->on_lineEdit_zPos_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 100: _t->on_lineEdit_zPos_2_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 101: _t->on_checkBoxThermal_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 102: _t->on_radioButton_mapTrack_clicked(); break;
        case 103: _t->on_lineEditManualColorZero_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 104: _t->on_lineEditManualColor_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 105: _t->on_radioButton_mapTrackInter_clicked(); break;
        case 106: _t->on_radioButton_mapTrackFast_clicked(); break;
        case 107: _t->on_radioButton_mapTrackAlbedo_clicked(); break;
        case 108: _t->on_checkBoxNoTrack_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 109: _t->on_radioButton_mapThermal_clicked(); break;
        case 110: _t->on_radioButton_mapTrackThermal_clicked(); break;
        case 111: _t->on_checkBoxThermalData_clicked(); break;
        case 112: _t->on_checkBoxSaveEvery_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 113: _t->on_radioButton_NoSource_clicked(); break;
        case 114: _t->on_lineEdit_xSize_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 115: _t->on_lineEdit_ySize_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 116: _t->on_radioButton_ThermalSource_clicked(); break;
        case 117: _t->on_radioButton_MonoenergeticSource_clicked(); break;
        case 118: _t->on_lineEdit_SourceEnergy_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 119: _t->on_lineEditScotoma_2_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 120: _t->on_checkBox_clicked(); break;
        case 121: _t->on_checkBox_NoMultipleScattering_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 122: _t->on_checkBoxManual_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 123: _t->on_checkBox_TrackAllLayers_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 124: _t->on_checkBoxLogarithmic_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 125: _t->on_radioButton_NeutronGrayScale_clicked(); break;
        case 126: _t->on_radioButton_NeutronHot_clicked(); break;
        case 127: _t->on_radioButton_NeutronThermal_clicked(); break;
        case 128: _t->on_radioButton_ModeratedCf_clicked(); break;
        case 129: _t->on_radioButton_Cylinder_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 130: _t->on_radioButton_Sphere_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 131: _t->on_radioButton_detectorLayerEnergyBand_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 132: _t->on_radioButton_detectorLayerRealistic_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 133: _t->on_radioButton_detectorEnergyBand_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 134: _t->on_radioButton_detectorRealistic_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 135: _t->on_checkBox_VolumeSource_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 136: _t->on_checkBox_HEModel_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 137: _t->on_checkBox_activateThermal_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 138: _t->on_horizontalSliderDetector_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 139: _t->on_horizontalSliderDetectorColor_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 140: _t->exportSettings((*reinterpret_cast< string(*)>(_a[1]))); break;
        case 141: { bool _r = _t->importSettings();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 142: _t->on_pushButton_Enlarge_clicked(); break;
        case 143: _t->on_lineEditAntiScotoma_editingFinished(); break;
        case 144: _t->on_checkBoxSaveEvery_2_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 145: _t->on_lineEditClearEveryXNeutrons_editingFinished(); break;
        case 146: _t->on_lineEditClearEveryXNeutrons_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 147: _t->on_lineEdit_AutoUpdate_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 148: _t->on_checkBoxAutoRefreshRate_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 149: _t->on_checkBoxClearEveryDisplayRefresh_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 150: _t->on_checkBoxClearEveryDisplayRefresh_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 151: _t->on_checkBoxWarnMAterial_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 152: _t->on_radioButton_mapTrackEnergy_clicked(); break;
        case 153: _t->on_checkBoxTrackingData_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 154: _t->on_checkBoxHighResTrackingData_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 155: _t->on_radioButton_ySheet_clicked(); break;
        case 156: _t->on_radioButton_xSheet_clicked(); break;
        case 157: _t->on_lineEditDetLength_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 158: _t->on_lineEdit_zSize_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 159: _t->on_radioButton_TopToBottom_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 160: _t->on_radioButton_BottomToTop_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 161: _t->on_radioButton_LeftToRight_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 162: _t->on_radioButton_RightToLeft_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 163: _t->on_radioButton_Omni_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 164: _t->on_checkBox_DomainCutoff_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 165: _t->on_lineEditDomainFactor_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 166: _t->on_checkBox_DomainCutoffMeters_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 167: _t->on_lineEditDomainMeters_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 168: _t->on_checkBoxFileOutput2_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 169: _t->on_checkBoxExportAllTracks_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 170: _t->on_checkBox_ReflectiveBoundaries_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 171: _t->on_checkBox_PeriodicBoundaries_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 172: _t->on_lineEdit_DetectorFile_editingFinished(); break;
        case 173: _t->on_lineEdit_DetectorFile_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 174: _t->on_checkBoxFileOutput_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 175: _t->on_checkBoxFileOutput3_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 176: _t->on_pushButton_SaveConfig_clicked(); break;
        case 177: _t->on_pushButton_Enlarge2_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 178)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 178;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 178)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 178;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
