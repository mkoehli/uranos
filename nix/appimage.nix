{ self, nixpkgs, nix-appimage, src, ... }:
let
  pkgs = import nixpkgs {
    system = "x86_64-linux";
    overlays = [ (import ./minimize-deps.nix nixpkgs.lib) ];
  };
  uranos-minimized-deps = pkgs.libsForQt5.callPackage ./package.nix { inherit src; };
  closure = pkgs.writeReferencesToFile uranos-minimized-deps;
in nix-appimage.mkappimage.x86_64-linux {
  drv = uranos-minimized-deps;
  entrypoint = "${uranos-minimized-deps}/bin/uranos";
  name = "uranos";
}

